<?php

/* 
 * Set Relative URL
 */
// protocol config
$protocol = 'http';
//if( isset($_SERVER['HTTP_X_FORWARDED_PROTO']) ) {
//    $protocol = $protocol.'s';
//}
$host = $_SERVER['HTTP_HOST'];
//if( $host == 'pakarpro.com') {
//    $host = $host.'/ext/pcm/';
//}
define('RELATIVE_URL', $protocol.'://'.$host);

/* 
 * Set Global Configuration for Database
 */
define('DB_HOST', 'localhost');
define('DB_USER', 'dbpkp');
define('DB_PASS', 'dbaPKP2017');
define('DB_NAME', 'pkp');

/* 
 * Set Global Configuration for accessing directory
 */
define('ASSETS', 'assets/');
define('MAIN_PATH', ASSETS.'main/');
define('PANEL_PATH', ASSETS.'panel/');
define('PLG_PATH', ASSETS.'plugins/');
define('IMG_PATH', 'images/');

define('DATE', date('Y-m-d'));
define('TIME', date('H:i:s'));
define('DATETIME', date('Y-m-d H:i:s'));
define('DEFAULT_DATETIME', '1970-01-01 00:00:00');

$ip = $_SERVER['REMOTE_ADDR'];
if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
define('IP_ADDR', $ip);