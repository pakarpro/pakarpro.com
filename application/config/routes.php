<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete deletes:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'main';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* Main type */
$route['about-us'] = 'main/about_us';
$route['contact'] = 'main/contact';
$route['article'] = 'article/post';
$route['article/post/(:any)'] = 'article/post/$1';
$route['article/tag/(:any)'] = 'article/tag/$1';
$route['article/tag/([a-z\-]+)/(:num)'] = 'article/tag/$1/$2';
$route['article/category/(:any)'] = 'article/category/$1';
$route['article/category/([a-z\-]+)/(:num)'] = 'article/category/$1/$2';
$route['article/get-external-source/(:num)'] = 'article/get_external_source/$1';
//$route['article/s'] = 'article/search';
//$route['article/s/(:num)'] = 'article/search/$1';
$route['activity'] = 'activity/index';
$route['project'] = 'project/index';
$route['project/(:any)'] = 'project/index/$1';
$route['privacy-policy'] = 'main/privacy_policy';
//$route['terms-of-use'] = 'main/terms_of_use';



/* Panel type */
# slider
$route['panel/slider'] = 'slider/lists';
$route['panel/slider/changestatus'] = 'slider/changestatus';
$route['panel/slider/(:num)'] = 'slider/lists/$1';
$route['panel/slider/add'] = 'slider/add';
$route['panel/slider/detail/(:num)'] = 'slider/detail/$1';
$route['panel/slider/delete/(:num)'] = 'slider/delete/$1';

# socialmedia
$route['panel/socialmedia'] = 'socialmedia/lists';
$route['panel/socialmedia/changestatus'] = 'socialmedia/changestatus';
$route['panel/socialmedia/(:num)'] = 'socialmedia/lists/$1';
$route['panel/socialmedia/add'] = 'socialmedia/add';
$route['panel/socialmedia/detail/(:num)'] = 'socialmedia/detail/$1';
$route['panel/socialmedia/delete/(:num)'] = 'socialmedia/delete/$1';

# user
$route['panel/user'] = 'user/lists';
$route['panel/user/changestatus'] = 'user/changestatus';
$route['panel/user/changerole'] = 'user/changerole';
$route['panel/user/(:any)'] = 'user/lists/$1';
$route['panel/user/([a-z]+)/(:num)'] = 'user/lists/$1/$2';
$route['panel/user/all/add'] = 'user/add/all';
$route['panel/user/role/add'] = 'user/add/role';
$route['panel/user/all/detail/(:num)'] = 'user/detail/all/$1';
$route['panel/user/role/detail/(:num)'] = 'user/detail/role/$1';
$route['panel/user/all/delete/(:num)'] = 'user/delete/all/$1';
$route['panel/user/role/delete/(:num)'] = 'user/delete/role/$1';
$route['panel/changepassword'] = 'user/changepassword';        

$route['panel/user/delete/(:num)'] = 'user/delete/$1';

# mail
$route['panel/mail'] = 'mail/lists';
$route['panel/mail/(:num)'] = 'mail/lists/$1';
//$route['panel/mail/add'] = 'mail/add';
$route['panel/mail/view/(:num)'] = 'mail/view/$1';
$route['panel/mail/delete/(:num)'] = 'mail/delete/$1';

# article
$route['panel/article/add/all'] = 'article/add/all';
$route['panel/article/add/category'] = 'article/add/category';
$route['panel/article/add/tag'] = 'article/add/tag';
$route['panel/article/changestatus/all'] = 'article/changestatus/all';
$route['panel/article/changestatus/category'] = 'article/changestatus/category';
$route['panel/article/([a-z]+)'] = 'article/lists/$1';
$route['panel/article/([a-z]+)/(:any)'] = 'article/lists/$1/$2';
$route['panel/article/all/detail/(:num)'] = 'article/detail/all/$1';
$route['panel/article/category/detail/(:num)'] = 'article/detail/category/$1';
$route['panel/article/tag/detail/(:num)'] = 'article/detail/tag/$1';
$route['panel/article/all/delete/(:num)'] = 'article/delete/all/$1';
$route['panel/article/category/delete/(:num)'] = 'article/delete/category/$1';
$route['panel/article/tag/delete/(:num)'] = 'article/delete/tag/$1';

# project
$route['panel/project'] = 'project/lists';
$route['panel/project/changescale'] = 'project/changescale';
$route['panel/project/processImages'] = 'project/processImages';
$route['panel/project/deleteImage'] = 'project/deleteImage';
$route['panel/project/(:any)'] = 'project/lists/$1';
$route['panel/project/([a-z]+)/(:num)'] = 'project/lists/$1/$2';
$route['panel/project/all/add'] = 'project/add/all';
$route['panel/project/scale/add'] = 'project/add/scale';
$route['panel/project/status/add'] = 'project/add/status';
$route['panel/project/all/detail/(:num)'] = 'project/detail/all/$1';
$route['panel/project/scale/detail/(:num)'] = 'project/detail/scale/$1';
$route['panel/project/status/detail/(:num)'] = 'project/detail/status/$1';
$route['panel/project/all/delete/(:num)'] = 'project/delete/all/$1';
$route['panel/project/scale/delete/(:num)'] = 'project/delete/scale/$1';
$route['panel/project/status/delete/(:num)'] = 'project/delete/status/$1';

# activity
$route['panel/activity'] = 'activity/lists';
$route['panel/activity/changestatus'] = 'activity/changestatus';
$route['panel/activity/(:num)'] = 'activity/lists/$1';
$route['panel/activity/add'] = 'activity/add';
$route['panel/activity/detail/(:num)'] = 'activity/detail/$1';
$route['panel/activity/delete/(:num)'] = 'activity/delete/$1';

