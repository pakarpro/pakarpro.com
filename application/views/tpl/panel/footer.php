    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(PLG_PATH.'jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(PLG_PATH.'bootstrap/js/bootstrap.min.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url(PLG_PATH.'bootstrap-select/js/bootstrap-select.min.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url(PLG_PATH.'jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>

    <!-- Waves Effect Plugin Js --> 
    <script src="<?php echo base_url(PLG_PATH.'node-waves/waves.min.js'); ?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(PANEL_PATH.'js/admin.js'); ?>"></script>
    
    <!-- Real Datetime Js -->
    <script src="<?php echo base_url(PANEL_PATH.'js/real-datetime.js'); ?>"></script>
    <script type="text/javascript">
        window.onload = real_datetime('realDatetime');
    </script>
</body>
</html>