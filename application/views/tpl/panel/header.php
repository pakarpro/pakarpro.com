<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $page; ?> | Admin Panel</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(IMG_PATH.'ico/favicon.ico'); ?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(PLG_PATH.'bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(PLG_PATH.'node-waves/waves.min.css'); ?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url(PLG_PATH.'animate-css/animate.min.css'); ?>" rel="stylesheet">

    <!-- Preloader Css -->
    <link href="<?php echo base_url(PLG_PATH.'material-design-preloader/md-preloader.min.css'); ?>" rel="stylesheet">
    
    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(PLG_PATH.'bootstrap-select/css/bootstrap-select.css'); ?>" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?php echo base_url(PANEL_PATH.'css/style.min.css'); ?>" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url(PANEL_PATH.'css/all-themes.min.css'); ?>" rel="stylesheet">
</head>

<body class="theme-indigo">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="md-preloader pl-size-md">
                <svg viewbox="0 0 75 75">
                    <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4">
                </svg>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?php echo site_url('panel');?>">PCM - PANEL</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a id="realDatetime"></a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
