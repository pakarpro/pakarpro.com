<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Super Administrator</div>
                <div class="email">super.admin@pcm.com</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?php echo site_url('panel/changepassword'); ?>"><i class="material-icons">lock</i>Edit Password</a></li>
                        <li><a href="<?php echo site_url('panel/logout'); ?>"><i class="material-icons">input</i>Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li <?php if(!empty($menu) && $menu=='dashboard'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel');?>">
                        <i class="material-icons">dashboard</i>
                        <span>Dashboard</span>
                    </a>
                </li>
                
                <li <?php if(!empty($menu) && $menu=='activity'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel/activity');?>" class="waves-effect">
                        <i class="material-icons">update</i>
                        <span>Activity</span>
                    </a>
                </li>

                <li <?php if(!empty($menu) && $menu=='article'){ echo 'class="active"';}?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">description</i>
                        <span>Article</span>
                    </a>
                    <ul class="ml-menu">
                        <li <?php if(!empty($submenu) && $submenu=='all'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/article/all');?>">All post</a>
                        </li>
                        <li <?php if(!empty($submenu) && $submenu=='category'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/article/category');?>">Category</a>
                        </li>
                        <li <?php if(!empty($submenu) && $submenu=='tag'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/article/tag');?>">Tag</a>
                        </li>
                    </ul>
                </li>
                
                <li <?php if(!empty($menu) && $menu=='mail'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel/mail');?>" class="waves-effect">
                        <i class="material-icons">message</i>
                        <span>Mail</span>
                        <?php if( isset($unread_mail) ) {
                            echo '<span style="position:absolute;right:15px;">('.$unread_mail.')</span>';
                        } ?>
                    </a>
                </li>

                <li <?php if(!empty($menu) && $menu=='project'){ echo 'class="active"';}?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">description</i>
                        <span>Project</span>
                    </a>
                    <ul class="ml-menu">
                        <li <?php if(!empty($submenu) && $submenu=='all'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/project/all');?>">All project</a>
                        </li>
                        <li <?php if(!empty($submenu) && $submenu=='scale'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/project/scale');?>">Scale</a>
                        </li>
                        <li <?php if(!empty($submenu) && $submenu=='status'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/project/status');?>">Status</a>
                        </li>
                    </ul>
                </li>
                
                <li <?php if(!empty($menu) && $menu=='slider'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel/slider');?>" class="waves-effect waves-block">
                        <i class="material-icons">perm_media</i>
                        <span>Slider</span>
                    </a>                    
                </li>
                
                <li <?php if(!empty($menu) && $menu=='socialmedia'){ echo 'class="active"';}?>>
                    <a href="<?php echo site_url('panel/socialmedia');?>" class="waves-effect waves-block">
                        <i class="material-icons">public</i>
                        <span>Social media</span>
                    </a>
                </li>
                
                <li <?php if(!empty($menu) && $menu=='user'){ echo 'class="active"';}?>>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">group</i>
                        <span>Users</span>
                    </a>
                    <ul class="ml-menu">
                        <li <?php if(!empty($submenu) && $submenu=='all'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/user/all');?>">All user</a>
                        </li>
                        <li <?php if(!empty($submenu) && $submenu=='role'){ echo 'class="active"';}?>>
                            <a href="<?php echo site_url('panel/user/role');?>">Role</a>
                        </li>
                    </ul>
                </li>
                <?php /*
                <li>
                    <a href="<?php echo site_url('panel/logout'); ?>">
                        <i class="material-icons">input</i>
                        <span>Logout</span>
                    </a>
                </li>
                */ ?>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2017 PT. Parama Karya Propertindo <br>
                <a href="https://github.com/gurayyarar/AdminBSBMaterialDesign">Template by Material Design</a>.
            </div>
            <div class="version">
                <b>Version: </b> 2017.7.8.1
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
