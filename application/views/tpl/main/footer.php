    <section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Company</h3>
                        <ul>
                            <li><a href="#">About us</a></li>
                            <li><a href="#">We are hiring</a></li>
                            <li><a href="#">Meet the team</a></li>
                            <li><a href="#">Copyright</a></li>
                            <li><a href="#">Terms of use</a></li>
                            <li><a href="#">Privacy policy</a></li>
                            <li><a href="#">Contact us</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Support</h3>
                        <ul>
                            <li><a href="#">Faq</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Forum</a></li>
                            <li><a href="#">Documentation</a></li>
                            <li><a href="#">Refund policy</a></li>
                            <li><a href="#">Ticket system</a></li>
                            <li><a href="#">Billing system</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Developers</h3>
                        <ul>
                            <li><a href="#">Web Development</a></li>
                            <li><a href="#">SEO Marketing</a></li>
                            <li><a href="#">Theme</a></li>
                            <li><a href="#">Development</a></li>
                            <li><a href="#">Email Marketing</a></li>
                            <li><a href="#">Plugin Development</a></li>
                            <li><a href="#">Article Writing</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3>Our Partners</h3>
                        <ul>
                            <li><a href="#">Adipisicing Elit</a></li>
                            <li><a href="#">Eiusmod</a></li>
                            <li><a href="#">Tempor</a></li>
                            <li><a href="#">Veniam</a></li>
                            <li><a href="#">Exercitation</a></li>
                            <li><a href="#">Ullamco</a></li>
                            <li><a href="#">Laboris</a></li>
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->
            </div>
        </div>
    </section><!--/#bottom-->

    <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2017 All Rights Reserved |
                    Template by <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>
                </div>
                <?php if( !empty($sosmed_account) ) { ?>
                    <div class="col-sm-6">
                       <div class="social">
                            <ul class="social-share">
                                <?php foreach($sosmed_account as $k => $item) { ?>
                                    <li><a href="<?php echo $item['url']; ?>" target="_blank"><i class="fa fa-<?php echo $item['gid']; ?>"></i></a></li>
                                <?php } ?>
                            </ul>
                       </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <a href="" class="scrollup"><i class="fa fa-chevron-up"></i></a>
    </footer><!--/#footer-->

    <script src="<?php echo base_url(MAIN_PATH.'js/jquery.js'); ?>"></script>
    <script src="<?php echo base_url(MAIN_PATH.'js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url(MAIN_PATH.'js/jquery.prettyPhoto.js'); ?>"></script>
    <script src="<?php echo base_url(MAIN_PATH.'js/jquery.isotope.min.js'); ?>"></script>
    <script src="<?php echo base_url(MAIN_PATH.'js/main.js'); ?>"></script>
    <script src="<?php echo base_url(MAIN_PATH.'js/wow.min.js'); ?>"></script>
</body>
</html>
