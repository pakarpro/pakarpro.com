<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta name="description" content="">
    <meta name="author" content="PT. Parama Citra Makassar">
    <title>PT. Parama Karya Propertindo</title>

    <!-- core CSS -->
    <link href="<?php echo base_url(MAIN_PATH.'css/bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url(MAIN_PATH.'css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url(MAIN_PATH.'css/animate.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url(MAIN_PATH.'css/prettyPhoto.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url(MAIN_PATH.'css/main.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url(MAIN_PATH.'css/responsive.min.css'); ?>" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo base_url(IMG_PATH.'ico/favicon.ico'); ?>" type="image/x-icon">
</head><!--/head-->

<body class="homepage">
    <header id="header">
        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo site_url(); ?>">
                        <img class="img-responsive" src="<?php echo base_url('images/logo.png'); ?>" alt="PT. Parama Karya Propertindo">
                    </a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li <?php if($page=='home'){echo 'class="active"';} ?>><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li <?php if($page=='article'){echo 'class="active"';} ?>><a href="<?php echo site_url('article'); ?>">Article</a></li>
                        <li <?php if($page=='project'){echo 'class="active"';} ?>><a href="<?php echo site_url('project'); ?>">Our Project</a></li>
                        <li <?php if($page=='activity'){echo 'class="active"';} ?>><a href="<?php echo site_url('activity'); ?>">Activity</a></li>
                        <li <?php if($page=='about-us'){echo 'class="active"';} ?>><a href="<?php echo site_url('about-us'); ?>">About Us</a></li>
                        <li <?php if($page=='contact'){echo 'class="active"';} ?>><a href="<?php echo site_url('contact'); ?>">Contact</a></li>
                        <?php /*
                        <li><a href="services.html">Services</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="blog-item.html">Blog Single</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="404.html">404</a></li>
                                <li><a href="shortcodes.html">Shortcodes</a></li>
                            </ul>
                        </li>
                        <li><a href="blog.html">Blog</a></li>
                        */ ?>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
