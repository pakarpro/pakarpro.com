<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('call_header')){
    function call_header($type, $page='Not Found'){
        $CI = &get_instance();

        $data['page'] = $page;
            
        $view = $CI->load->view('tpl/'.$type.'/header', $data, true);
        return $view;
    }        
}

if(!function_exists('call_footer')){
    function call_footer($type){
        $CI = &get_instance();

        $data = array();
        if( $type == 'main' ) {
            // get active sosmed account
            $CI->load->helper('socialmedia/socialmedia');
            $sosmed_account = get_active_sosmed_account();
            if( !empty($sosmed_account) ) {
                $data['sosmed_account'] = $sosmed_account;
            }
        }

        $view = $CI->load->view('tpl/'.$type.'/footer', $data, true);
        return $view;
    }        
}

/*
 * Function name    :   call_sidebar
 * Usage            :   show menu only for panel page
 * Type             :   -
 * Value            :   -
 */
if(!function_exists('call_sidebar')){
    function call_sidebar($menu='', $submenu=''){
        $CI = &get_instance();
        
        $data['menu'] = $menu;
        if( !empty($submenu) ) {
            $data['submenu'] = $submenu;
        }
        
        // count unread mail
        $CI->load->helper('mail/mail');
        $count_unread = count_mail('unread');
        if( $count_unread > 0 ) {
            $data['unread_mail'] = $count_unread;
        }
        
        $view = $CI->load->view('tpl/panel/sidebar', $data, true);
        return $view;
    }
}

if( !function_exists('site_list') ) {
    function site_list() {
        $site = array(
            'pkp' => 'PT. Parama Karya Propertindo',
            'pcm' => 'PT. Parama Citra Makassar',
            'tambun' => 'Tambun',
            'klari' => 'Klari',
        );
        
        return $site;
    }
}

if(!function_exists('paging_config')){
    function paging_config($data, $set=true) {
        $full_tag_open = '<ul class="pagination pagination-lg">';
        if( $set == false ) {
            $full_tag_open = '<ul class="pagination">';
        }
        
        foreach($data as $field => $value) {
            $conf[$field] = $value;
        }
//        $conf['base_url'] = $data['base_url'];
//        $conf['total_rows'] = $data['total_rows'];
//        $conf['per_page'] = $data['per_page'];
//        $conf['query_string_segment'] = 'page';
        $conf['use_page_numbers'] = TRUE;
//        $conf['page_query_string'] = TRUE;
        $conf['full_tag_open'] = $full_tag_open;
        $conf['full_tag_close'] ="</ul>";
        $conf['num_tag_open'] = '<li>';
        $conf['num_tag_close'] = '</li>';
        $conf['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $conf['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $conf['next_tag_open'] = "<li>";
        $conf['next_tag_close'] = "</li>";
        $conf['prev_tag_open'] = "<li>";
        $conf['prev_tag_close'] = "</li>";
        $conf['first_tag_open'] = "<li>";
        $conf['first_tag_close'] = "</li>";
        $conf['last_tag_open'] = "<li>";
        $conf['last_tag_close'] = "</li>";
        $conf['prev_link'] = '<i class="fa fa-long-arrow-left"></i> Previous Page';
        $conf['next_link'] = 'Next Page <i class="fa fa-long-arrow-right"></i>';
        $conf['first_link'] = '<i class="fa fa-angle-double-left"></i> First';
        $conf['last_link'] = 'Last <i class="fa fa-angle-double-right"></i>';

        return $conf;
    }
}

/*
 * Function name    :   format_datetime
 * Usage            :   formatting date
 * Type             :   -
 * Value            :   -
 */
if(!function_exists('format_datetime')){
    function format_datetime($datetime, $type='dt', $abbr=FALSE){
        $exp = explode(' ', $datetime);
        $date = explode('-', $exp[0]);
        $time = explode(':', $exp[1]);

        // date
        $year = $date[0];
        $month = month($date[1], $abbr);
        $day = $date[2];

        // time start
        $hour = $time[0];
        $minute = $time[1];
        $second = $time[2];
        
        switch($type) {
            case 'dt':
                $newDate = $day.' '.$month.' '.$year.' '.$hour.'.'.$minute.' WIB';
                break;
            case 'd':
                $newDate = $day.' '.$month.' '.$year;
                break;
            case 't':
                $newDate = $hour.'.'.$minute.' WIB';
                break;
        }
        
        return $newDate;
    }        
}

/*
 * Function name    :   format_date
 * Usage            :   formatting date
 * Type             :   -
 * Value            :   -
 */
if(!function_exists('format_date')){
    function format_date($date_start, $date_end){
        // date start
        $year = substr($date_start,0,4);
        $month = month(substr($date_start,5,2));
        $day = substr($date_start,8,2);

        // date end
        $year2 = substr($date_end,0,4);
        $month2 = month(substr($date_end,5,2));
        $day2 = substr($date_end,8,2);

        if( $date_start == $date_end ) {
            $date = $day.' '.$month.' '.$year;
        } else {
            $date = $day.' '.$month.' '.$year.' - '.$day2.' '.$month2.' '.$year2;
        }
        
        return $date;
    }        
}

/*
 * Function name    :   format_time
 * Usage            :   formatting time
 * Type             :   -
 * Value            :   -
 */
if(!function_exists('format_time')){
    function format_time($time_start, $time_end){
        // time start
        $hour = substr($time_start,0,2);
        $minute = substr($time_start,3,2);

        // time end
        $hour2 = substr($time_end,0,2);
        $minute2 = substr($time_end,3,2);

        $time = $hour.':'.$minute.' - '.$hour2.':'.$minute2.' WIB';
        
        return $time;
    }        
}

/*
 * Function name    :   month
 * Usage            :   formatting month
 * Type             :   -
 * Value            :   -
 */
if(!function_exists('month')){
    function month($month, $abbr=false){

        switch( $month ) {
            case '01':
                $month = 'Januari';
                if( $abbr == true ) {
                    $month = 'Jan';
                }
                break;
            case '02':
                $month = 'Februari';
                if( $abbr == true ) {
                    $month = 'Feb';
                }
                break;
            case '03':
                $month = 'Maret';
                if( $abbr == true ) {
                    $month = 'Mar';
                }
                break;
            case '04':
                $month = 'April';
                if( $abbr == true ) {
                    $month = 'Apr';
                }
                break;
            case '05': $month = 'Mei'; break;
            case '06':
                $month = 'Juni';
                if( $abbr == true ) {
                    $month = 'Jun';
                }
                break;
            case '07':
                $month = 'Juli';
                if( $abbr == true ) {
                    $month = 'Jul';
                }
                break;
            case '08':
                $month = 'Agustus';
                if( $abbr == true ) {
                    $month = 'Agt';
                }
                break;
            case '09':
                $month = 'September';
                if( $abbr == true ) {
                    $month = 'Sept';
                }
                break;
            case '10':
                $month = 'Oktober';
                if( $abbr == true ) {
                    $month = 'Okt';
                }
                break;
            case '11':
                $month = 'November';
                if( $abbr == true ) {
                    $month = 'Nov';
                }
                break;
            case '12':
                $month = 'Desember';
                if( $abbr == true ) {
                    $month = 'Des';
                }
                break;
        }

        return $month;
    }        
}

/*
 * Function name    :   format_status
 * Usage            :   summarize long paragraph
 * Type             :   string
 * Value            :   
 */
if(!function_exists('format_status')) {
    function format_status($data) {
        $value = 0;
        if( isset($data['status']) && $data['status'] == 'on' ) {
            $value = 1;
        }
        
        return $value;
    }
}

/*
 * Function name    :   summarize_sentence
 * Usage            :   summarize long paragraph
 * Type             :   string
 * Value            :   
 */
if(!function_exists('summarize_sentence')) {
    function summarize_sentence($sentence='', $word_count=10) {
        if( !empty($sentence) ) {
            $count = str_word_count($sentence);
            if( $count > $word_count ) {
                $sentence = trim(implode(' ', array_slice(explode(' ', $sentence), 0, $word_count)));
            }
        }
        
        return $sentence;
    }
}

/*
 * Function name    :   error_msg_generator
 * Usage            :   create custom error message with delimiter
 * Type             :   string
 * Value            :   
 */
if(!function_exists('error_msg_generator')) {
    function error_msg_generator($error_msg) {
        $message = '';
        
        if( !empty($error_msg) ) {
            $message = '<p class="text-danger">';
            $message .= $error_msg;
            $message .= '</p>';
        }
        
        return $message;
    }
}

/*
// for compressing image
if( !function_exists('compress_image') ) {
    function compress_image($source, $destination, $quality) {
        $info = getimagesize($source);

        $image_create = str_replace('/','createfrom',$info['mime']);
        $image = str_replace('/','',$info['mime']);
        
        $image($image_create, $destination, $quality);

        chmod($destination, 0777);

        return $destination;
    }
}
*/
