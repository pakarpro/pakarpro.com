<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Controller {
    
    private $CI;
    private $slider;
    private $slider_status = 1;
    private $redirect = 'panel/login';
    private $img_path;
    private $temp_path;
    private $admin_data = array();
    private $validation_config = array(
        array(
            'field'   => 'title', 
            'label'   => 'Image title', 
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'caption', 
            'label'   => 'Image caption', 
            'rules'   => 'trim|required'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required'
    );

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('form');
        $this->load->model('Slider_model');
        $this->slider = new Slider_model();
        
        // image path
        $this->img_path = FCPATH.IMG_PATH.'slider/';
        $this->temp_path = FCPATH.IMG_PATH.'temp/slider/';

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
    
    // get last number of sorter
    private function _get_last_sorter() {
        $sorter = $this->slider->getLastSorter();
        $data = $sorter['sorter'];
        
        return $data;
    }
    
    // updating status, for ajax use
    public function changestatus() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $status = $data['status'];
            
            $update_param = array(
                'date_update' => DATETIME,
                'status' => $status
            );
            $update = $this->slider->updateSlider($update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    public function index() {
        $this->lists();
    }
    
    // list all sliders data
    public function lists($page=1) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        $count_slider = $this->slider->countSlider();
        if( $count_slider > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            if( $count_slider > $limit_per_page ) {
                // additional where param
                $where_param['limit'] = array($limit_per_page, $start_row);
                
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/slider'),
                    'total_rows'    =>  $count_slider,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            $where_param['order_by'] = array(
                'date_add' => 'DESC'
            );
            $data['data'] = $this->slider->getSlider($where_param);
        }
        
        $this->CI->load->view('list', $data);
    }
    
    // save new slider data
    public function add() {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;

        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();
            $this->slider_status = format_status($post_data);

            // validation
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $title = ucfirst($post_data['title']);
                $caption = ucfirst($post_data['caption']);
                $status = $this->slider_status;
                $sorter = $this->_get_last_sorter();
                
                // save parameter
                $save_param = array(
                    'date_add' => DATETIME,
                    'title' => $title,
                    'caption' => $caption,
                    'sorter' => $sorter,
                    'status' => $status,
                );

                // if image uploaded
                if( !empty($_FILES['image_name']['tmp_name']) ) {
                    $this->load->helper('uploads/uploads');
                    
                    $source = $_FILES;
                    $uploaded_data = upload_image_to_temp('slider', $source, $this->temp_path);
                }

                // if error occured
                if( isset($uploaded_data) ) {
                    foreach($uploaded_data as $k => $value) {
                        switch($k) {
                            case 'uploaded_data':
                                $save = $this->slider->setNewSlider($save_param);
                                if( isset($save['id']) ) {
                                    $id = $save['id'];
                                    
                                    // create directory
                                    $img_path = $this->img_path.$id.'/';
                                    if( !file_exists($img_path) ) {
                                        mkdir($img_path);
                                        chmod($img_path, 0777);
                                    }

                                    // crop image, store in directory, update image_name in database
                                    $image_name = cropping_image('slider', $value, $img_path, $title);
                                    
                                    // update image name in database
                                    $update_param = $image_name;
                                    $update = $this->slider->updateSlider($update_param, $id);
                                    if( $update == 'SUCCESSFUL' ) {
                                        // set notification
                                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    New slider has been added.
                                                  </div>';
                                        $this->session->set_flashdata('notif', $notif);

                                        // redirect to list general info page
                                        redirect('panel/slider/detail/'.$id);
                                    }
                                }
                                break;
                            case 'error_image':
                                $data[$k] = array_shift($value);
                                break;
                        }
                    }
                }
            }
        }

        // view page (add courses)
        $data['status'] = $this->slider_status;
        $this->load->view('add', $data);
    }
    
    // update slider data, with image or not
    public function detail($id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            // check ID in database
            $detail_slider = $this->slider->getSliderById($id);
            if( !empty($detail_slider) ) {
                $this->slider_status = $detail_slider['slider_status'];
                if(!empty($_POST)) {
                    $post_data = $this->input->post();
                    $this->slider_status = format_status($post_data);
                    $detail_slider = array_merge($detail_slider, $post_data);

                    $this->load->library('form_validation');
                    $this->form_validation->set_rules($this->validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }

                    if ($this->form_validation->run() == TRUE ) {
                        $title = ucwords($post_data['title']);
                        $caption = ucfirst($post_data['caption']);
                        $status = $this->slider_status;
                        $image_name = $post_data['image_name'];

                        // update parameter
                        $update_param = array(
                            'date_update' => DATETIME,
                            'title' => $title,
                            'caption' => $caption,
                            'status' => $status
                        );

                        if( !empty($_FILES['image_name']['tmp_name']) ) {
                            $this->load->helper('uploads/uploads');

                            $source = $_FILES;
                            $uploaded_data = upload_image_to_temp('slider', $source, $this->temp_path);
                        }

                        // if error occured
                        if( isset($uploaded_data) ) {
                            foreach($uploaded_data as $k => $value) {
                                switch($k) {
                                    case 'uploaded_data':
                                        // create directory
                                        $img_path = $this->img_path.$id.'/';
                                        if( !file_exists($img_path) ) {
                                            mkdir($img_path);
                                            chmod($img_path, 0777);
                                        }
                                        
                                        // unlink old photo
                                        $glob = glob($this->img_path.$id.'/*'.$image_name);
                                        if( !empty($glob) ) {
                                            foreach($glob as $item) {
                                                unlink($item);
                                            }
                                        }

                                        // crop image, store in directory, update image_name in database
                                        $image_name = cropping_image('slider', $value, $img_path, $title);
                                        $update_param = array_merge($update_param, $image_name);

                                        $update = $this->slider->updateSlider($update_param, $id);
                                        if( $update == 'SUCCESSFUL' ) {
                                            // set notification
                                            $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                        Slider has been updated with image.
                                                      </div>';
                                            $this->session->set_flashdata('notif', $notif);
                                        }

                                        // redirect to
                                        redirect('panel/slider/detail/'.$id);
                                    break;
                                    case 'error_image':
                                        $data[$k] = array_shift($value);
                                        break;
                                }
                            }
                        } else {
                            $update = $this->slider->updateSlider($update_param, $id);

                            if( $update == 'SUCCESSFUL' ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            Slider has been updated without image.
                                          </div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to list general info page
                                redirect('panel/slider/detail/'.$id);
                            }
                        }
                    } # end of validation
                } # end of post

                $data['data'] = $detail_slider;
            } # end of empty data
        }
        
        $data['slider_status'] = $this->slider_status;

        $this->load->view('detail', $data);
    }
    
    // delete slider data, include of image
    public function delete($id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting slider. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            // check if given ID is existing in database
            $slider = $this->slider->getSliderById($id);
            // if data is found
            if( !empty($slider) ) {
                // set image name
                $img_name = $slider['image_name'];
                // full path
                $path = $this->img_path.$id.'/';
                // set the full path of image
                $image = $path.$img_name;

                // delete the image
                unlink($image);
                // delete the directory
                rmdir($path);

                // run delete action
                $delete = $this->slider->deleteSlider($id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    // provide success message
                    $msg = 'Slider with title <b>'.$slider['title'].'</b> has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/slider');
    }
}