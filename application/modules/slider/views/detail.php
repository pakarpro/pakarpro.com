<?php echo call_header('panel', 'Detail Slider'); ?>

<?php echo call_sidebar('slider'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <div class="form-group">
                                    <a href="<?php echo site_url('panel/slider'); ?>" class="btn btn-default waves-effect">BACK</a>
                                </div>

                                <?php if( isset($data) ) { ?>
                                    <form action="<?php echo site_url('panel/slider/detail/'.$data['slider_id']); ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <div class="switch">
                                                <?php
                                                    $checked = '';
                                                    if( $slider_status == 1 ) {
                                                        $checked = 'checked';
                                                    }
                                                ?>
                                                <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Title</label>
                                            <?php echo form_error('title'); ?>
                                            <div class="form-line">
                                                <input name="title" type="text" class="form-control" value="<?php echo $data['title']; ?>" autofocus required="required">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Caption</label>
                                            <?php echo form_error('caption'); ?>
                                            <div class="form-line">
                                                <input name="caption" type="text" class="form-control" value="<?php echo $data['caption']; ?>" required="required">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Image</label>
                                            <?php if(isset($error_image)) echo $error_image; ?>
                                            <div class="form-line">
                                                <?php
                                                    $img_name = $data['image_name'];
                                                    $image = base_url(IMG_PATH.'slider/'.$data['slider_id'].'/'.$img_name);
                                                ?>
                                                <input type="file" name="image_name" id="imageInput" class="form-control">
                                                <input type="hidden" name="image_name" value="<?php echo $img_name; ?>">
                                            </div>
                                            <small class="rules">File size should be equal or less than 2 MB, with minimum size 1400 x 600 px</small>
                                            <br><br>
                                            <img class="img-responsive img-thumbnail" src="<?php echo $image; ?>" width="400">
                                        </div>

                                        <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 pull-right">UPDATE</button>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
