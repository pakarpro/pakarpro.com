<?php echo call_header('panel', 'Add New Slider'); ?>

<?php echo call_sidebar('slider'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-6 col-xs-12">
                                <form action="<?php echo site_url('slider/add'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <?php
                                            $checked = '';
                                            if( $status == 1 ) {
                                                $checked = 'checked';
                                            }
                                        ?>
                                        <div class="switch">
                                            <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Title</label>
                                        <?php echo form_error('title'); ?>
                                        <div class="form-line">
                                            <input name="title" type="text" class="form-control" value="<?php echo set_value('title'); ?>" autofocus required="required">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Caption</label>
                                        <?php echo form_error('caption'); ?>
                                        <div class="form-line">
                                            <input name="caption" type="text" class="form-control" value="<?php echo set_value('caption'); ?>" required="required">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Image</label>
                                        <?php if( isset($error_image) ) echo $error_image; ?>
                                        <div class="form-line">
                                            <input type="file" name="image_name" class="form-control">
                                        </div>
                                        <small class="rules">File size should be equal or less than 2 MB, with size 1400 x 600 px </small>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <a href="<?php echo site_url('panel/slider'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right" style="margin-right: 10px;" value="SAVE">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<script>
    $(document).on('click', '#toggleStatus', function() {
        if( $(this).is(':checked') ) {
            $('#status').val('1');
        } else {
            $('#status').val('0');
        }
    });
</script>