<?php

// to be displayed in homepage
if( !function_exists('get_active_slider') ){
    function get_active_slider(){
        $CI = &get_instance();

        $CI->load->model('slider/Slider_model');
        $slider = new Slider_model();
        
        $param = array(
            'fields' => array(
                'id AS slider_id',
                'title',
                'caption',
                'image_name'
            ),
            'where' => array(
                'status' => 1
            ),
            'order_by' => array(
                'date_add' => 'DESC'
            )
        );
        $data = $slider->getSlider($param);

        return $data;
    }
}

// get detail slider by id
if( !function_exists('get_last_sorter') ){
    function get_last_sorter(){
        $CI = &get_instance();

        $CI->load->model('slider/Slider_model');
        $slider = new Slider_model();
        $data = $slider->getLastSorter();
        $data['sorter'] += 1;
        
        return $data['sorter'];
    }
}

// count slider
if(!function_exists('count_slider')){
    function count_slider($type='all'){
        $CI = &get_instance();
        $CI->load->model('slider/Slider_model');
        
        $param = array();
        switch($type) {
            case 'active':
                $param['where'] = array('status' => 1);
                break;
            case 'inactive':
                $param['where'] = array('status' => 0);
                break;
        }
        
        $slider = new Slider_model();
        $data = $slider->countSlider($param);

        return $data;
    }
}
