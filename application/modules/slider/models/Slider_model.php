<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Slider_model extends CI_model {
    
    public $data;
    public $return;
    private $slider;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
        $this->return = array();
        $this->slider = array(
            'id AS slider_id',
            'site',
            'date_add',
            'date_update',
            'title',
            'caption',
            'image_name',
            'sorter',
            'status AS slider_status'
        );
    }
    
#=================================== CREATE ===================================#
    public function setNewSlider($data) {
        $params['table'] = SLIDER;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getSlider($params=array()) {
        // fields
        $fields = $this->slider;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        
        $params['fields'] = $fields;
        $params['table'] = SLIDER;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getSliderById($id) {
        $params['fields'] = $this->slider;
        $params['table'] = SLIDER;
        $params['where'] = array(
            'id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getLastSorter() {
        $params['select_max'] = 'sorter';
        $params['table'] = SLIDER;

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countSlider() {
        $params['table'] = SLIDER;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateSlider($data, $sliderId) {
        $params['table'] = SLIDER;
        $params['data'] = $data;
        $params['where'] = array(
            'id' => $sliderId
        );
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteSlider($sliderId) {
        $params['table'] = SLIDER;
        $params['where'] = array(
            'id' => $sliderId
        );
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}