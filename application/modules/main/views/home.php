<?php echo call_header('main', 'home'); ?>

<?php if( !empty($slider) ) { ?>
    <!-- #main-slider -->
    <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <?php
                    for($i=0; $i<$slider['controller']; $i++) {
                        $active = '';
                        if( $i==0 ) {
                            $active = 'class="active"';
                        }

                        echo '<li data-target="#main-slider" data-slide-to="'.$i.'" '.$active.'></li>';
                    }
                ?>
            </ol>
            <div class="carousel-inner">
                <?php
                    foreach($slider['slider'] as $k => $item) {
                        $active = '';
                        if( $k==0 ) {
                            $active = 'active';
                        }
                ?>
                    <div class="item <?php echo $active; ?>" style="background-image: url(<?php echo base_url(IMG_PATH.'slider/'.$item['slider_id'].'/'.$item['image_name']); ?>)">
                        <div class="container">
                            <div class="row slide-margin">
                                <div class="col-sm-12">
                                    <div class="carousel-content">
                                        <h1 class="animation animated-item-1"><?php echo $item['title']; ?></h1>
                                        <h2 class="animation animated-item-2"><?php echo $item['caption']; ?></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--/.item-->
                <?php } ?>
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section>
    <!--/#main-slider-->
<?php } ?>
    
<?php if( !empty($activity) ) { ?>
    <section id="feature" >
        <div class="container">
           <div class="center wow fadeInDown">
                <h2>Activities</h2>
            </div>

            <div class="row">
                <div class="features">
                    <?php
                        $no = 1;
                        foreach($activity['activity'] as $k => $item) {
                            $id = $item['activity_id'];
                            // image
                            $img_url = base_url(IMG_PATH.'activity/'.$id.'/small-'.$item['activity_image']);
                    ?>
                        <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                            <div class="feature-wrap">
                                <a class="_da" data-id="<?php echo $id; ?>" href="#" data-toggle="modal" data-target="#detailActivity">
                                    <img class="img-responsive img-thumbnail" src="<?php echo $img_url; ?>" width="100%">
                                </a>
                                <h3><?php echo $item['activity_caption']; ?></h3>
                            </div>
                        </div><!--/.col-md-4-->
                    <?php
                            if( $no % 3 == 0 ) {
                                echo '</div>';
                                echo '<div class="row">';
                            }

                            $no++;
                        }
                    ?>
                </div><!--/.features-->
            </div><!--/.row-->
            
            <?php if( $activity['total'] > 6 ) { ?>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <a class="btn btn-primary" href="<?php echo site_url('activity'); ?>">See More</a>
                    </div>
                </div>
            <?php } ?>
        </div><!--/.container-->
    </section><!--/#updates-->
    
    <!-- Modal for activities -->
    <div class="modal fade" id="detailActivity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Activity</h5>
                </div>
                <div class="modal-body activity-caption"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-close-modal btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
    
<?php if( !empty($project) ) { ?>
    <section id="project">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Our Projects</h2>
                <!--<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>-->
            </div>

            <div class="row">
                <?php
                    $no = 1;
                    foreach($project['project'] as $k => $item) {
                        // detail url
                        $detail_url = site_url('project/'.$item['gid_project']);
                        // image
                        $img_url = base_url(IMG_PATH.'project/'.$item['project_id'].'/'.$item['image_name']);
                ?>
                    <div class="col-xs-12 col-md-4 col-sm-6" style="margin-bottom:30px;">
                        <div class="recent-work-wrap">
                            <img class="img-responsive img-thumbnail" src="<?php echo $img_url; ?>" alt="<?php echo $item['title']; ?>">
                            <div class="overlay">
                                <div class="recent-work-inner">
                                    <h3 class="text-bold"><a href="<?php echo $detail_url; ?>"><?php echo $item['title']; ?></a></h3>
                                    <p><?php echo $item['annotation']; ?></p>
                                    <a class="preview" href="<?php echo $detail_url; ?>" style="margin-right:15px;">
                                        <i class="fa fa-eye"></i> Read More
                                    </a>
                                    <a class="preview" href="<?php echo $item['website']; ?>" target="_blank">
                                        <i class="fa fa-external-link"></i> Visit Link
                                    </a>
                                </div> 
                            </div>
                        </div>
                    </div>   
                <?php
                        if( $no % 3 == 0 ) {
                            echo '</div>';
                            echo '<div class="row">';
                        }
                        
                        $no++;
                    }
                ?>
            </div><!--/.row-->
            
            <?php if( $project['total'] > 3 ) { ?>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <a class="btn btn-primary" href="<?php echo site_url('project'); ?>">See More</a>
                    </div>
                </div>
            <?php } ?>
        </div><!--/.container-->
    </section><!--/#our-projetcs-->
<?php } ?>

<?php if( !empty($article) ) { ?>
    <section id="latest-news" style="background-color:#e2f1d6;">
        <div class="container">
            <div class="center wow fadeInDown">
                <h2>Latest News</h2>
            </div>

            <div class="row">
                <?php
                    $no = 1;
                    foreach($article['article'] as $k => $item) {
                        // detail url
                        $detail_url = site_url('article/post/'.$item['gid_article']);
                        // image
                        $date = explode(' ', $item['date_add']);
                        $date = str_replace('-','/',$date[0]).'/'.$item['article_id'].'/';
                        $img_url = base_url(IMG_PATH.'article/'.$date.'medium-'.$item['image_name']);
                ?>
                    <div class="col-md-4 col-sm-6 col-xs-12" style="margin-bottom:40px;">
                        <a href="<?php echo $detail_url; ?>">
                            <img src="<?php echo $img_url; ?>" class="img-responsive img-thumbnail">
                        </a>
                        <h3 class="text-bold">
                            <a href="<?php echo $detail_url; ?>"><?php echo $item['title']; ?></a>
                        </h3>
                        <p><?php echo $item['annotation']; ?></p>
                    </div>
                <?php
                        if( $no % 3 == 0 ) {
                            echo '</div>';
                            echo '<div class="row">';
                        }
                        
                        $no++;
                    }
                ?>
            </div><!--/.row-->
            
            <?php if( $article['total'] > 6 ) { ?>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <a class="btn btn-primary" href="<?php echo site_url('article'); ?>">See More</a>
                    </div>
                </div>
            <?php } ?>
        </div><!--/.container-->
    </section><!--/#latest-news-->
<?php }?>

<?php echo call_footer('main'); ?>
<script>
    $(document).on('click', '._da', function(){
        var image = $(this).children('img').attr('src');
        var caption = $(this).next('h3').text();

        $('#detailActivity').on('shown.bs.modal', function() {
            $('.activity-caption').html(
                '<img class="img-responsive img-thumbnail" src="'+image.replace('small-','')+'">'+
                '<h3 style="word-break: break-all;">'+caption+'</h3>'
            );
        })
        
        $(document).on('click', '.btn-close-modal', function(){
            $('.activity-caption').html('');
        });
    });
</script>