<?php echo call_header('main', 'contact'); ?>

<?php if( !empty($data) ) { ?>
    <!--/gmap_area -->
    <section id="contact-info">
        <div class="center">                
            <h2>How to Reach Us?</h2>
            <!--<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>-->
        </div>
        <div class="gmap-area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 text-center">
                        <div class="gmap">
                            <iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.773365854835!2d106.83796031535866!3d-6.161099995539333!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5c748ff5a8f%3A0x7ad5f92354b56811!2sJl.+Bungur+Besar+17+No.6%2C+Gn.+Sahari+Sel.%2C+Kemayoran%2C+Kota+Jakarta+Pusat%2C+DKI+Jakarta+10610!5e0!3m2!1sid!2sid!4v1493797514463" ></iframe>
                        </div>
                    </div>

                    <div class="col-sm-7 map-content">
                        <ul class="row">
                            <li class="col-sm-6">
                                <?php foreach($data['office'] as $k => $item) { ?>
                                    <address>
                                        <h5><?php echo $item['type']; ?></h5>
                                        <p>
                                            <?php echo $item['address']; ?><br>
                                            <?php echo $item['district']; ?>, <?php echo $item['city']; ?>
                                        </p>
                                        <p>
                                            Phone: <?php echo $item['phone']; ?><br>
                                            Email: <a href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a>
                                        </p>
                                    </address>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section> 
    <!--/gmap_area -->
<?php } ?>

    <section id="contact-page">
        <div class="container">
            <div class="center">        
                <h2>Drop Your Message</h2>
                <!--<p class="lead">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
            </div> 
            <div class="row contact-wrap"> 
                <div class="status alert alert-success" style="display: none;">
                    <!--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>-->
                    <!--Pesan berhasil dikirim-->
                </div>
                <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="<?php echo site_url('mail/send'); ?>">
                    <div class="col-sm-5 col-sm-offset-1">
                        <div class="form-group">
                            <label><b>Name *</b></label>
                            <input type="text" name="name" id="c_name" class="form-control" required>
                            <!--<p class="text-danger">Required</p>-->
                        </div>
                        <div class="form-group">
                            <label><b>Email *</b></label>
                            <input type="email" name="email" id="c_email" class="form-control" required>
                            <!--<p class="text-danger">Email is invalid</p>-->
                        </div>
                        <div class="form-group">
                            <label><b>Phone</b></label>
                            <input type="number" name="phone" id="c_phone" class="form-control">
                            <!--<p class="text-danger">Use number</p>-->
                        </div>
                        <div class="form-group">
                            <label><b>Company Name</b></label>
                            <input type="text" name="company_name" id="c_company_name" class="form-control">
                        </div>                        
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label><b>Subject *</b></label>
                            <input type="text" name="subject" id="c_subject" class="form-control" required>
                            <!--<p class="text-danger">At least 10 chars</p>-->
                        </div>
                        <div class="form-group">
                            <label><b>Message *</b></label>
                            <textarea name="message" id="c_message" required class="form-control" rows="8"></textarea>
                            <!--<p class="text-danger">At least 30 chars</p>-->
                        </div>                        
                        <div class="form-group">
                            <button type="submit" id="btn-contact" class="btn btn-primary btn-lg">Submit Message</button>
                            <p style="margin-top: 5px;"><i>*) required</i></p>
                        </div>
                    </div>
                </form> 
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->

<?php echo call_footer('main'); ?>

<script>
    
</script>