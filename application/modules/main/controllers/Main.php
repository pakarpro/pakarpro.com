<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
    
    private $CI;
    
    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('general');
    }
    
    // routing for homepage
    public function index() {
        $this->homepage();
    }
    
    // active office for contact
    private function _get_active_office() {
        // load office helper
        $this->load->helper('office/office');
        $office = get_active_office();
        
        $data = array();
        if( !empty($office) ) {
            // load area helper
            $this->load->helper('area/area');
            foreach($office as $k => $item) {
                // type
                $type = str_replace('_',' ',$item['type']);
                $item['type'] = ucwords($type);
                
                // province
                $province = get_area_by_id('province', $item['province']);
                $item['province'] = $province['province_name'];
                
                // city
                $city = get_area_by_id('city', $item['city']);
                $item['city'] = $city['city_name'];
                
                // distict
                $district = get_area_by_id('district', $item['district']);
                $item['district'] = $district['district_name'];
                
                $office[$k] = $item;
            }
            
            $data['office'] = $office;
        }

        return $data;
    }
    
    // active slider for homepage
    private function _get_active_slider() {
        // load slider helper
        $this->load->helper('slider/slider');
        $slider = get_active_slider();
        
        $data = array();
        if( !empty($slider) ) {
            $count = count($slider);
            
            foreach($slider as $k => $item) {
                $item['image_url'] = base_url(IMG_PATH.'slider/'.$item['slider_id'].'/'.$item['image_name']);
                
                $slider[$k] = $item;
            }

            $data = array(
                'slider' => $slider,
                'controller' => $count,
            );
        }
        
        return $data;
    }
    
    // list activity for homepage
    private function _get_activity() {
        $this->load->model('activity/Activity_model');
        $activity = new Activity_model();
        
        // count data first
        $count = $activity->countActivity();
        if( $count > 0 ) {
            $data['total'] = $count;

            // get data
            $where['limit'] = array(6,0);
            $where['order_by'] = array(
                'date_activity' => 'DESC'
            );
            $data['activity'] = $activity->getActivity($where);
        } else {
            $data = array();
        }
        
        return $data;
    }
    
    // latest article for homepage
    private function _get_latest_article() {
        $data = array();
        
        $this->load->model('article/Article_model');
        $article = new Article_model();
        
        $where['where'] = array(
            'status' => 1,
            "(NOW() >= date_start OR date_start = '1970-01-01 00:00:00')" => NULL,
            "(NOW() <= date_end OR date_end = '1970-01-01 00:00:00')" => NULL
        );
        // count data first
        $count = $article->countArticle(ARTICLE, $where);
        if( $count > 0 ) {
            $data['total'] = $count;
            
            // get article
            $where['limit'] = array(6,0);
            $where['order_by'] = array(
                'date_start' => 'DESC'
            );
            $data['article'] = $article->getArticle(ARTICLE, $where);
        }
        
        return $data;
    }
    
    // list project for homepage
    private function _get_project() {
        $this->load->model('project/Project_model');
        $project = new Project_model();
        
        // count data first
        $count = $project->countProject(PROJECT);
        if( $count > 0 ) {
            $data['total'] = $count;

            // get data
            $where['limit'] = array(3,0);
            $where['order_by'] = array(
                'date_add' => 'ASC'
            );
            $data['project'] = $project->getProject(PROJECT, $where);
        }
        
        return $data;
    }
    
    // showing homepage
    public function homepage() {
        // slider
        $data['slider'] = $this->_get_active_slider();
        
        // updates or activity
        $data['activity'] = $this->_get_activity();
        
        // latest article
        $data['article'] = $this->_get_latest_article();
        
        // our projects
        $data['project'] = $this->_get_project();
        
        $this->load->view('home', $data);
    }
    
    // showing about us
    public function about_us() {
        $this->load->view('about-us');
    }

    // showing contact
    public function contact() {
        $data['data'] = $this->_get_active_office();
        $this->load->view('contact-us', $data);
    }
    
    // showing privacy policy
    public function privacy_policy() {
        $this->load->view('privacy-policy');
    }
}
