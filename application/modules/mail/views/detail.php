<?php echo call_header('panel', 'Detail Message'); ?>

<?php echo call_sidebar('mail', 'detail'); ?>

<section class="content">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <?php if( isset($data) ) { ?>
                        <div class="header">
                            <?php
                                $back_url = site_url('panel/mail');
                                if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                    $back_url = $_SERVER['HTTP_REFERER'];
                                }
                            ?>
                            <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect m-b-15">BACK</a>
                            <h2 class="card-inside-title"><?php echo $data['subject']; ?></h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>From</label>
                                        <p><span class="m-r-5 text-bold"><?php echo $data['name']; ?></span> &lt;<?php echo $data['email']; ?>&gt;</p>
                                    </div>
                                    <div class="form-group">
                                        <label>Date</label>
                                        <p><?php echo format_datetime($data['date_sent']); ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <p><?php echo $data['phone']; ?></p>
                                    </div>
                                    <div class="form-group">
                                        <label>Message</label>
                                        <p><?php echo $data['message']; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        } else {
                            echo 'Data not found';
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
