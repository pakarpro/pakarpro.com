<?php echo call_header('panel', 'Lists of Contact'); ?>

<?php echo call_sidebar('mail'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php echo $this->session->flashdata('notif'); ?>
                    
                        <table class="table table-hover table-list">
                            <thead>
                                <tr>
                                    <th width="10">#</th>
                                    <th width="150">Date Sent</th>
                                    <th>Sender</th>
                                    <th>Subject - Message</th>
                                    <th width="30"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( !empty($data) ) {
                                        foreach($data as $k => $item) {
                                            $is_read = 'text-bold';
                                            if( $item['is_read'] == 1 ) {
                                                $is_read = '';
                                            }
                                ?>
                                    <tr data-target="<?php echo site_url('panel/mail/view/'.$item['mail_id']); ?>">
                                        <td class="_1"><?php echo $k+1; ?></td>
                                        <td class="_l <?php echo $is_read; ?>"><?php echo format_datetime($item['date_sent']); ?></td>
                                        <td class="_l <?php echo $is_read; ?>"><?php echo $item['name']; ?></td>
                                        <td class="_l">
                                            <span class="<?php echo $is_read; ?>"><?php echo $item['subject']; ?></span> <br>
                                            <span class="text-grey"><?php echo $item['message']; ?></span>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('panel/mail/delete/'.$item['mail_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this mail ?');">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="4">Data not found</td></tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <?php if( isset($pagination) ) { ?>
                        <!-- pagination -->
                        <div class="row clearfix">
                            <div class="col-xs-12 text-center">
                                <?php print_r($pagination); ?>
                            </div>
                        </div>
                        <!-- /pagination -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>