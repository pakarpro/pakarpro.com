<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel';
    private $admin_data = array();
    private $validation_config = array(
        array(
            'field'   => 'name',
            'label'   => 'Name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z\s\'\-\,\.]+$/]'
        ),
        array(
            'field'   => 'email',
            'label'   => 'Email',
            'rules'   => 'trim|required|valid_email'
        ),
        array(
            'field'   => 'phone',
            'label'   => 'Phone',
            'rules'   => 'trim|numeric|min_length[6]'
        ),
        array(
            'field'   => 'company_name',
            'label'   => 'Company name',
            'rules'   => 'trim|regex_match[/^[A-Za-z0-9\s\'\.]+$/]'
        ),
        array(
            'field'   => 'subject',
            'label'   => 'Subject',
            'rules'   => 'trim|required|min_length[10]'
        ),
        array(
            'field'   => 'message',
            'label'   => 'Message',
            'rules'   => 'trim|required|min_length[30]'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'regex_match' => '%s does not follow approriate format',
        'valid_email' => '%s is not valid',
        'numeric' => 'Use number only',
        'min_length' => '%s should be at least %d characters'
    );
    private $validation_delimiter = array(
        'open_tag' => '<p class="text-danger">',
        'close_tag' => '</p>'
    );
    private $mail;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->model('Mail_model');
        $this->mail = new Mail_model();

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
    }
    
    // check existing account based on sosmed type
    private function _check_existing_account($param) {
        $where['where'] = $param;
        $count = $this->mail->countMail(SOSMED_ACCOUNT, $where);
        echo $count; exit;
    }
    
    // format account rules
    private function _format_account_rules($rules) {
        $rules = unserialize($rules);
        
        // regex
        $regex = implode("", $rules);
        $data['regex_rules'] = 'regex_match[/^['.$regex.']+$/]';
        
        // info rules
        foreach($rules as $rule) {
            switch($rule) {
                case "a-z": $rule = 'letters'; break;
                case "0-9": $rule = 'numbers'; break;
                case "_": $rule = 'underscore'; break;
                case ".": $rule = 'dot'; break;
            }

            $formatted_rules[] = ' '.$rule;
        }
        $data['info_rules'] = 'Must contain at least'.implode(',', $formatted_rules);
        
        return $data;
    }

    // get client ip
    private function _get_client_ip() {
        $ip = $_SERVER['REMOTE_ADDR'];
        if( isset($_SERVER['HTTP_CLIENT_IP']) ) {   //check ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif( isset($_SERVER['HTTP_X_FORWARDED_FOR']) ) {   //to check ip is pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        
        return $ip;
    }
    
    // save new message
    private function _save($data) {
        // date send
        $data['date_sent'] = DATETIME;
        // client ip
        $data['client_ip'] = $this->_get_client_ip();
        
        // save to database
        $save = $this->mail->setNewMail($data);
        if( isset($save['id']) ) {
            return true;
        } else {
            return false;
        }
    }
    
    private function _set_read_status($id, $status='read') {
        $is_read = 1;
        if( $status == 'unread' ) {
            $is_read = 0;
        }
        $update_param = array(
            'date_read' => DATETIME,
            'is_read' => $is_read
        );
        $update = $this->mail->updateMail($update_param, $id);
    }

    // for ajax use, send mail through contact page
    public function send() {
        $post_data = $this->input->post();
        
        $data['status'] = 'failed';
        if( !empty($post_data) ) {
            // validation
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->validation_delimiter['open_tag'], $this->validation_delimiter['close_tag']);
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
//                $data['status'] = 'success';
                $save = $this->_save($post_data);
                if( $save == true ) {
                    $data['status'] = 'success';
                }
            } else {
                foreach($post_data as $k => $val) {
                    $error_msg[$k] = form_error($k);
                }
                $data['status'] = 'failed';
                $data['message'] = $error_msg;
            }
        }

        echo json_encode($data);
    }
    
    public function index() {
        $this->lists();
    }
    
    public function lists($page=1) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        $this->load->helper('mail');
        $count_mail = count_mail();
        if( $count_mail > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            if( $count_mail > $limit_per_page ) {
                // additional where param
                $where_param['limit'] = array($limit_per_page, $start_row);
                
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/mail'),
                    'total_rows'    =>  $count_mail,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            $where_param['order_by'] = array(
                'date_sent' => 'DESC'
            );
            $data['data'] = $this->mail->getMail($where_param);
        }

        $this->CI->load->view('list', $data);
    }
    
    // update socialmedia data, with image or not
    public function view($id) {
        // check session login
//        $this->_check_session();
        $data['admin_data'] = $this->admin_data;
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $mail = $this->mail->getMailById($id);
            if( !empty($mail) ) {
                $data['data'] = $mail;

                // set read status to 1 and update the date_read
                $this->_set_read_status($id);
            }
        }

        $this->load->view('detail', $data);
    }
    
    // delete socialmedia data, include of image
    public function delete($id) {
        // check session login
//        $this->_check_session();
        $data['admin_data'] = $this->admin_data;
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting message. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && $id != 0 || is_numeric($id) ) {
            // check if given ID is existing in database
            $mail = $this->mail->getMailById($id);
            // if data is found
            if( !empty($mail) ) {
                // run delete action
                $delete = $this->mail->deleteMail($id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    // provide success message
                    $msg = 'Message from <b>'.$mail['name'].'</b> has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/mail');
    }
}