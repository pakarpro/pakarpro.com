<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('list_mail')){
    function list_mail($data=''){
        $CI = &get_instance();
        $CI->load->model('mail/Mail_model');
        
        if( !empty($data) ) {
            $mail_list = $CI->Mail_model->list_mail($data);
        } else {
            $mail_list = $CI->Mail_model->list_mail();
        }
        
        return $mail_list;
    }
}

if(!function_exists('count_mail')){
    function count_mail($type='all'){
        $CI = &get_instance();
        $CI->load->model('mail/Mail_model');
        
        $param = array();
        switch($type) {
            case 'read':
                $param['where'] = array('is_read' => 1);
                break;
            case 'unread':
                $param['where'] = array('is_read' => 0);
                break;
        }
        
        $mail = new Mail_model();
        $data = $mail->countMail($param);

        return $data;
    }
}
