<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends CI_Controller {
    
    private $CI;
    private $redirect = 'admin/dashboard';
    private $user_data = array();
    private $validation_config;
    private $validation_msg;
    private $validation_delimiter;
    private $img_path;
    private $temp_path;
    
    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->load->helper('general');
        $this->load->helper('classes/Classes');
        $this->load->model('Classes_model');
        
        $this->validation_config = array(
            array(
                'field'   => 'id_course',
                'label'   => 'Course',
                'rules'   => 'trim|required'
            ),
            array(
                'field'   => 'title', 
                'label'   => 'Class title', 
                'rules'   => 'trim|required|max_length[255]'
            ),
            array(
                'field'   => 'annotation', 
                'label'   => 'Class annotation', 
                'rules'   => 'trim|required|max_length[255]'
            ),
            array(
                'field'   => 'size', 
                'label'   => 'Class size', 
                'rules'   => 'trim|required|numeric'
            ),
            array(
                'field'   => 'location', 
                'label'   => 'Class location', 
                'rules'   => 'trim|required|max_length[255]'
            ),
            array(
                'field'   => 'class_status', 
                'label'   => 'Class status', 
                'rules'   => 'trim|required|max_length[255]'
            ),
            array(
                'field'   => 'content',
                'label'   => 'Classes Info',
                'rules'   => 'trim|required'
            ),
             array(
                'field'   => 'date_start', 
                'label'   => 'Class start date', 
                'rules'   => 'trim|required|regex_match[/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/]'
            ),
            array(
                'field'   => 'start_time', 
                'label'   => 'Class start time', 
                'rules'   => 'trim|required|regex_match[/^[0-2][0-9]\:[0-6][0-9]\:*[0-9]*$/]'
            ),
            array(
                'field'   => 'end_time', 
                'label'   => 'Class end time', 
                'rules'   => 'trim|required|regex_match[/^[0-2][0-9]\:[0-6][0-9]\:*[0-9]*$/]'
            )
        );
        
        $this->validation_msg = array(
            'required' => '%s is required',
            'max_length' => '%s cannot exceed 255 characters',
            'regex_match' => '%s does not follow approriate format'
        );
        
        $this->validation_delimiter = array(
            'open_tag' => '<p class="text-danger">',
            'close_tag' => '</p>'
        );
        
        $this->img_path = FCPATH.'uploads/classes/';
        
        $this->temp_path = FCPATH.'uploads/temp/classes/';
         
        if( $this->session->userdata('logged_in') ) {
            $this->user_data = $this->session->userdata('logged_in');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->user_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('admin'.$this->redirect);
        }
    }
    
    // save new data
    public function add_classes() {
        // check session login
        $this->_check_login_session();
        $data['user_data'] = $this->user_data;                
        
        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();
            $data['post_data'] = $post_data;

            // validation
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->validation_delimiter['open_tag'], $this->validation_delimiter['close_tag']);
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $id_course = $post_data['id_course'];
                $title = ucwords($post_data['title']);
                $annotation = ucfirst($post_data['annotation']);
                $content = ucfirst($post_data['content']);
                $date_start = $post_data['date_start'];
                $time_start = $post_data['start_time'];
                $end_time = $post_data['end_time'];
                $class_size = $post_data['size'];
                $location = ucwords($post_data['location']);
                $class_status = $post_data['class_status'];
                $status = 1;
                
                // save parameter
                $save_param['parameters'] = array(
                    'id_course' => $id_course,
                    'title' => $title,
                    'annotation' => $annotation,
                    'content' => $content,
                    'date_start' => $date_start,
                    'start_time' => $time_start,
                    'end_time' => $end_time,
                    'size' => $class_size,
                    'location' => $location,
                    'class_status' => $class_status,
                    'status' => $status
                );
                
                // if image uploaded
                if( !empty($_FILES['logo']['tmp_name']) || !empty($_FILES['timeline_logo']['tmp_name']) ) {
                    $this->load->helper('uploads/uploads');
                    
                    $source = $_FILES;
                    $uploaded_data = upload_image_to_temp('class', $source, $this->temp_path);
                }
                
                // if error occured
                if( isset($uploaded_data) ) {
                    foreach($uploaded_data as $k => $value) {
                        switch($k) {
                            case 'uploaded_data':
                                $save = $this->Classes_model->add_classes($save_param);
                                if( isset($save['id']) ) {
                                    $id = $save['id'];
                                    
                                    // create directory
                                    $img_path = $this->img_path.$id.'/';
                                    if( !file_exists($img_path) ) {
                                        mkdir($img_path);
                                        chmod($img_path, 0777);
                                    }

                                    // crop image, store in directory, update logo in database
                                    $logo = cropping_image('class', $value, $img_path, $title);
                                    
                                    // update image name in database
                                    $update_logo = array(
                                        'parameters' => $logo,
                                        'where' => array('id' =>  $id)
                                    );
                                    $this->Classes_model->update_classes($update_logo);

                                    // set notification and redirect to list general info
                                    $rdr['notif_classes']['success'] = $title.' has been added';
                                    $this->session->set_flashdata($rdr);

                                    // redirect to list general info page
                                    redirect('admin/list_classes');
                                }
                                break;
                            case 'error_image':
                                $data[$k] = $value;
                                break;
                        }
                    }
                } else {
                    // save without image
                    $this->Classes_model->add_classes($save_param);

                    // set notification and redirect to list general info
                    $rdr['notif_classes']['success'] = $title.' has been added without image';
                    $this->session->set_flashdata($rdr);

                    // redirect to list general info page
                    redirect('admin/list_classes');
                }
            }
        }

        //load courses data
        $this->load->helper('courses/Courses');
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_classes'] = list_classes($data);
        //if there is no course registered, redirect user to fill the course data first
        if(empty($data['list_classes'])) {
            redirect(site_url('admin/add_courses'));
        }

        // view page (add courses)
        $this->load->view('admin/classes/add_classes', $data);
    }
    
    public function update_classes($id) {
        // check session login
        $this->_check_login_session();
        $data['user_data'] = $this->user_data;

        if(!empty($_POST)) {
            $post_data = $this->input->post();
            $data['post_data'] = $post_data;
            
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->validation_delimiter['open_tag'], $this->validation_delimiter['close_tag']);
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }
            
            if ($this->form_validation->run() == TRUE ) {
                $id_course = $post_data['id_course'];
                $title = ucwords($post_data['title']);
                $annotation = ucfirst($post_data['annotation']);
                $content = ucfirst($post_data['content']);
                $date_start = $post_data['date_start'];
                $time_start = $post_data['start_time'];
                $end_time = $post_data['end_time'];
                $class_size = $post_data['size'];
                $location = ucwords($post_data['location']);
                $class_status = $post_data['class_status'];
                $logo = $post_data['logo'];
                $timeline_logo = $post_data['timeline_logo'];
                $status = 1;
                
                // save parameter
                $update_param['parameters'] = array(
                    'id_course' => $id_course,
                    'title' => $title,
                    'annotation' => $annotation,
                    'content' => $content,
                    'date_start' => $date_start,
                    'start_time' => $time_start,
                    'end_time' => $end_time,
                    'size' => $class_size,
                    'location' => $location,
                    'class_status' => $class_status,
                    'logo' => $logo,
                    'timeline_logo' => $timeline_logo,
                    'status' => $status
                );
                $update_param['where'] = array(
                    'id' => $id
                );
                
                if( !empty($_FILES['logo']['tmp_name']) || !empty($_FILES['timeline_logo']['tmp_name']) ) {
                    $this->load->helper('uploads/uploads');
                    
                    $source = $_FILES;
                    $uploaded_data = upload_image_to_temp('class', $source, $this->temp_path);
                }

                // if error occured
                if( isset($uploaded_data) ) {
                    foreach($uploaded_data as $k => $value) {
                        switch($k) {
                            case 'uploaded_data':
                                // create directory
                                $img_path = $this->img_path.$id.'/';
                                if( !file_exists($img_path) ) {
                                    mkdir($img_path);
                                    chmod($img_path, 0777);
                                }
                                
                                foreach($value as $a => $b) {
                                    // unlink old photo
                                    unlink($img_path.$$a);
                                }

                                // crop image, store in directory, update logo in database
                                $logo = cropping_image('class', $value, $img_path, $title);
                                $update_param['parameters'] = array_merge($update_param['parameters'], $logo);
                                $this->Classes_model->update_classes($update_param);

                                // set notification and redirect to list general info
                                $rdr['notif_classes']['success'] = $title.' has been updated';
                                $this->session->set_flashdata($rdr);

                                // redirect to list general info page
                                redirect('admin/list_classes');
                            break;
                            case 'error_image':
                                $data[$k] = $value;
                                break;
                        }
                    }
                } else {
                    $this->Classes_model->update_classes($update_param);
                
                    // set notification and redirect to list general info
                    $rdr['notif_classes']['success'] = $title.' has been updated without image';
                    $this->session->set_flashdata($rdr);

                    // redirect to list general info page
                    redirect('admin/list_classes');
                }
            }
        }

        //get class detail
        $this->load->helper('classes/Classes');
        $data['classes_param'] = array(
            'where' => array(
                'id' => $id
            )
        );
        $data['classes'] = get_classes($data['classes_param']);

        //get the list of courses data
        $this->load->helper('courses/Courses');
        //assign course data to the class
        foreach($data['classes'] as $k=>$v)
        {            
            $data['courses_param'] = array(
                'where' => array(
                    'id' => $v->id_course
                )
            );
            $course_data = get_courses($data['courses_param']);
            if(!empty($course_data))
            {
                $data['classes'][$k]->course_title = $course_data[0]->title;
            }
        }

        //get courses data list
        $this->load->helper('courses/Courses');
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_courses'] = list_courses($data);

        //get class assignment data
        $data['classes_assignment_param'] = array(
            'fields' => array(
                'tp_classes_assignment.id',
                'tp_classes_assignment.id_staff',
                'tp_classes_assignment.id_classes',
                'tp_staff.id AS staff_id',
                'tp_staff.fname',
                'tp_staff.lname',
                'tp_staff.logo',
                'tp_staff_position.name AS staff_position'
            ),
            'join'  => array(
                'tp_staff' => 'tp_staff.id = tp_classes_assignment.id_staff',
                'tp_staff_position' => 'tp_staff_position.id = tp_staff.id_staff_position'
            ),
            'where' => array(
                'tp_classes_assignment.id_classes' => $id
            )
        );
        $data['classes_assignment_list'] = list_classes_assignment($data['classes_assignment_param']);

        $this->load->view('admin/classes/classes_detail', $data);
    }
    
    public function delete_classes($id) {
        // check session login
        $this->_check_login_session();
        $data['user_data'] = $this->user_data;

        // get logo
        $this->load->helper('classes/Classes');
        $params['where'] = array(
            'id' => $id
        );
        $get_data = get_classes($params);
        if( !empty($get_data) ) {
            $get_data = array_shift($get_data);
            $logo = $get_data->logo;
            $timeline_logo = $get_data->timeline_logo;
            $path = FCPATH.'uploads/classes/'.$id.'/';

            // remove photo
            if( !empty($get_data->logo) ) {
                unlink($path.$logo);
            }
            if( !empty($get_data->timeline_logo) ) {
                unlink($path.$timeline_logo);
            }
            
            // remove directory
            rmdir($path);

            // set notification
            $rdr['notif_classes']['success'] = $get_data->title.' has been deleted';
        } else {
            // set notification
            $rdr['notif_classes']['failed'] = 'No data to be deleted.';
        }

        $this->session->set_flashdata($rdr);

        // redirect to list general info page
        redirect('admin/list_classes');
    }

    public function add_classes_assignment() {
        // check session login
        $this->_check_login_session();
        $data['user_data'] = $this->user_data;
        if(!empty($_POST))
        {
            $this->load->library('form_validation');
            $config = array(
                array(
                    'field'   => 'id_staff', 
                    'label'   => 'Staff', 
                    'rules'   => 'required'
                ),
                array(
                    'field'   => 'id_classes', 
                    'label'   => 'Class', 
                    'rules'   => 'required'
                )
            );
            $this->form_validation->set_rules($config);                
            $this->form_validation->set_message('required', '<font color="red"><br/>%s is required!</font>'); 
            $this->form_validation->set_message('max_length', '<font color="red"><br/>%s cannot exceed 255 characters</font>'); 
            if ($this->form_validation->run() == TRUE)
            {                    
                $data['parameters'] = array(
                    'id_staff'          => $_POST['id_staff'],
                    'id_classes'        => $_POST['id_classes']
                );
                //check first if the user is already assigned, to said class, if not, assign the data to db
                $params_check['where'] = array(
                    'id_staff'          => $_POST['id_staff'],
                    'id_classes'        => $_POST['id_classes']
                );
                $classes_assignment = $this->Classes_model->get_classes_assignment($params_check);
                if(empty($classes_assignment))
                {
                    $query = $this->Classes_model->add_classes_assignment($data);
                }
                redirect(site_url('admin/classes_detail/'.$_POST['id_classes']));
            }
            else 
            {
                //load staff data
                $this->load->helper('staff/Staff');
                $page = 1;
                $data['staff_params']['limit']['per_page'] = 200;
                $data['staff_params']['limit']['start'] = ($page-1)*$data['staff_params']['limit']['per_page'];
                $data['list_staff'] = list_staff($data['staff_params']);    
                //assign position name for the staff
                $this->load->helper('staff_position/Staff_position');
                foreach($data['list_staff'] as $k=>$v)
                {                        
                    $data['staff_position_params']['where'] = array(
                        'id'    =>  $v->id_staff_position
                    );
                    $staff_position = get_staff_position($data['staff_position_params']);
                    if(!empty($staff_position))
                    {
                        $data['list_staff'][$k]->staff_position = $staff_position[0]->name;
                    }
                }
                //if there is no staff position, redirect user to fill the staff position
                if(empty($data['list_staff']))
                {
                    redirect(site_url('admin/add_staff'));
                }

                //load class data
                $this->load->helper('classes/Classes');
                $page = 1;
                $data['classes_params']['limit']['per_page'] = 200;
                $data['classes_params']['limit']['start'] = ($page-1)*$data['classes_params']['limit']['per_page'];
                $data['list_classes'] = list_classes($data['classes_params']);    
                //if there is no staff position, redirect user to fill the staff position
                if(empty($data['list_classes']))
                {
                    redirect(site_url('admin/add_classes'));
                }
        
                $this->load->view('admin/classes/add_classes_assignment', $data);
            }
        }
    }
    
    public function delete_classes_assignment($id) {
        // check session login
        $this->_check_login_session();
        $data['user_data'] = $this->user_data;

        $params = array(           
            'where' => array(
                'id' => $id
            )
        );
        $this->Classes_model->delete_classes_assignment($params);
        
        redirect(site_url('admin/list_classes'));
    }
    
    // additional function to tell form that input may contain only alphacharacters and white space
    function alpha_space($str) {
        return ( ! preg_match("/^([a-z_ ])+$/i", $str)) ? FALSE : TRUE;
    }
}
