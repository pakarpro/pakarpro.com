<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('list_classes')){
    function list_classes($data){
        $CI = &get_instance();
        $CI->load->model('classes/Classes_model');
        
        $classes_list = $CI->Classes_model->list_classes($data);
        return $classes_list;
    }
}
if(!function_exists('count_list_classes')){
    function count_list_classes(){
        $CI = &get_instance();
        $CI->load->model('classes/Classes_model');
        
        $count_classes_list = $CI->Classes_model->count_classes();
        return $count_classes_list;
    }
}
if(!function_exists('get_classes')){
    function get_classes($data){
        $CI = &get_instance();
        $CI->load->model('classes/Classes_model');
        $event = $CI->Classes_model->get_classes($data);
        return $event;
    }
}

if(!function_exists('list_classes_assignment')){
    function list_classes_assignment($data){
        $CI = &get_instance();
        $CI->load->model('classes/Classes_model');
        $event = $CI->Classes_model->list_classes_assignment($data);
        return $event;
    }
}

