<?php


if (!defined('BASEPATH')) exit('No direct script access allowed');


class Classes_model extends CI_model
{
    
    public function add_classes($data)
    {
        $this->db->insert('tp_classes', $data['parameters']);
        $insert_id['id'] = $this->db->insert_id();

        return $insert_id;
    }
    
    public function add_classes_assignment($data)
    {
        $this->db->insert('tp_classes_assignment', $data['parameters']);
        $insert_id['id'] = $this->db->insert_id();

        return $insert_id;
    }
    
    public function count_classes()
    {
        $this->db->select('*');
        $this->db->from('tp_classes');

        $query = $this->db->get();

        if($query->num_rows()!=0)
        {
          return $query->num_rows();
        }
        else
        {
          return 0;
        }	 
    }
    
    public function get_classes($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_classes');
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k, $v);
            }
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $fields => $value) {
                $this->db->join($fields, $value);
            }
        }
        
        if(isset($data['like']))
        {
            foreach($data['like'] AS $k=>$v)
            {
                $this->db->like($k, $v);
            }
        }
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function get_classes_assignment($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_classes_assignment');
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k, $v);
            }
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $fields => $value) {
                $this->db->join($fields, $value);
            }
        }
        
        if(isset($data['like']))
        {
            foreach($data['like'] AS $k=>$v)
            {
                $this->db->like($k, $v);
            }
        }
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function list_classes($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_classes');
        
        // limit
        if(isset($data['limit']))
        {
            $this->db->limit($data['limit']['per_page'], $data['limit']['start']);
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $k => $val) {
                $this->db->join($k, $val);
            }
        }
        
        // where
        if(isset($data['where'])) {
            foreach($data['where'] as $k => $val) {
                $this->db->where($k, $val);
            }
        }
        
        // order_by
        if(isset($data['order_by'])) {
            foreach($data['order_by'] as $k => $val) {
                $this->db->order_by($k, $val);
            }
        }
        
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function list_classes_assignment($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_classes_assignment');
        
        // limit
        if(isset($data['limit']))
        {
            $this->db->limit($data['limit']['per_page'], $data['limit']['start']);
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $k => $val) {
                $this->db->join($k, $val);
            }
        }
        
        // where
        if(isset($data['where'])) {
            foreach($data['where'] as $k => $val) {
                $this->db->where($k, $val);
            }
        }
        
        // order_by
        if(isset($data['order_by'])) {
            foreach($data['order_by'] as $k => $val) {
                $this->db->order_by($k, $val);
            }
        }
        
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function update_classes($data)
    {
        if(isset($data['parameters']))
        {
            foreach($data['parameters'] AS $k=>$v)
            {
                $this->db->set($k,$v);
            }
        }
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        $this->db->update('tp_classes');
    }

    public function delete_classes($data)
    {
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        $this->db->delete('tp_classes');
    }
    
    public function delete_classes_assignment($data)
    {
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        $this->db->delete('tp_classes_assignment');
    }
}