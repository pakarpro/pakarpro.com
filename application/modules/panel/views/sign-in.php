﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | PT. Parama Karya Propertindo</title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url(IMG_PATH.'ico/favicon.ico'); ?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(PLG_PATH.'bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(PLG_PATH.'node-waves/waves.min.css'); ?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url(PLG_PATH.'animate-css/animate.min.css'); ?>" rel="stylesheet">

    <!-- Custom Css -->
    <link href="<?php echo base_url(PANEL_PATH.'css/style.min.css'); ?>" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">Admin <b>Panel</b></a>
        </div>
        <div class="card">
            <div class="body">
                <form action="<?php echo $rdr; ?>" name="form-login" method="post">
                    <?php if( isset($error_login) ) {
                        echo '<div class="msg">'.$error_login.'</div>';
                    } ?>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo set_value('username'); ?>" required autofocus>
                        </div>
                        <?php echo form_error('username'); ?>
                    </div>
                    
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo set_value('password'); ?>" required>
                        </div>
                        <?php echo form_error('password'); ?>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-pink waves-effect" type="submit">SIGN IN</button>
                        </div>
                    </div>
                    <?php /*
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="forgot-password.html">Forgot Password?</a>
                        </div>
                    </div>
                     */ ?>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url(PLG_PATH.'jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url(PLG_PATH.'bootstrap/js/bootstrap.min.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url(PLG_PATH.'node-waves/waves.min.js'); ?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url(PLG_PATH.'jquery-validation/jquery.validate.js'); ?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url(PANEL_PATH.'js/admin.js'); ?>"></script>
    <script src="<?php echo base_url(PANEL_PATH.'js/sign-in.js'); ?>"></script>
</body>

</html>