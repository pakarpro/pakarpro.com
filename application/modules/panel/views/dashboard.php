<?php echo call_header('panel', 'Dashboard'); ?>

<?php echo call_sidebar('dashboard'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>DASHBOARD</h2>
        </div>

        <!-- Widgets -->
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect" onclick="goTo('slider')">
                    <div class="icon">
                        <i class="material-icons">perm_media</i>
                    </div>
                    <div class="content">
                        <div class="text">ACTIVE SLIDER</div>
                        <div class="number count-to" data-from="0" data-to="<?php echo $active_slider; ?>" data-speed="250" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect" onclick="goTo('mail')">
                    <div class="icon">
                        <i class="material-icons">message</i>
                    </div>
                    <div class="content">
                        <div class="text">UNREAD MAIL</div>
                        <div class="number count-to" data-from="0" data-to="<?php echo $unread_msg; ?>" data-speed="250" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect" onclick="goTo('article/all')">
                    <div class="icon">
                        <i class="material-icons">description</i>
                    </div>
                    <div class="content">
                        <div class="text">ACTIVE ARTICLE</div>
                        <div class="number count-to" data-from="0" data-to="<?php echo $active_article; ?>" data-speed="250" data-fresh-interval="20"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Widgets -->
        
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'jquery-countto/jquery.countTo.js'); ?>"></script>
<script>
    //Widgets count
    $('.count-to').countTo();
    
    $('.info-box').css('cursor', 'pointer');
    
    function goTo(target) {
        window.location = target;
    }
    
</script>