<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {
    
    private $CI;
    private $redirect = 'panel/dashboard';
    private $admin_data = array();
    private $validation_msg = array(
        'required' => '%s is required',
        'min_length' => '%s at least %d characters',
        'max_length' => '%s maximum length is %d characters'
    );
    private $validation_config = array(
        array(
            'field'   => 'username',
            'label'   => 'Username',
            'rules'   => 'trim|required|min_length[5]|max_length[30]'
        ),
        array(
            'field'   => 'password',
            'label'   => 'Password',
            'rules'   => 'trim|required|min_length[8]|max_length[30]'
        )
    );
    private $validation_delimiter = array(
        'open_tag' => '<p class="text-danger">',
        'close_tag' => '</p>'
    );
    private $slider;

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('form');

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }

    // as login page
    public function index() {
        if( !empty($this->admin_data) ) {
            $this->dashboard();
        } else {
            $this->login();
        }
    }
    
    // check session login
    private function _check_session_login() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('login'.$this->redirect);
        }
    }
    
    // validate login
    private function _validate_login($username, $password) {
        $this->load->model('user/User_model');
        $user = new User_model();
        
        $check_param['where'] = array(
            'username' => $username,
            'password' => $password
        );
        $check = $user->getUser(USER, $check_param);
        
        return $check;
    }
    
    public function login() {
        // redirect
        if( isset($_GET['rdr']) ) {
            $this->redirect = $_GET['rdr'];
        }
        $data['rdr'] = '?rdr='.urlencode($this->redirect);
        
        if( !empty($_POST) ) {
            // get post data
            $post_data = $this->input->post();

            // validation
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters($this->validation_delimiter['open_tag'], $this->validation_delimiter['close_tag']);
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $this->load->helper('user/user');
                
                $username = $post_data['username'];
                $password = enc_password($post_data['password']);
                
                $validate = $this->_validate_login($username, $password);
                if( !empty($validate) ) {
                    $admin_data = array_shift($validate);

                    if( $admin_data['user_status'] == 0 ) {
                        $error_login = 'Account is not active.';
                        $data['error_login'] = error_msg_generator($error_login);
                    } else {
                        // set session
                        $this->session->set_userdata('is_login', $admin_data['user_id']);

                        redirect($this->redirect);
                    }
                } else {
                    $error_login = 'Invalid username or password.';
                    $data['error_login'] = error_msg_generator($error_login);
                }
            }
        }
        
        $this->load->view('sign-in', $data);
    }
    
    public function logout() {
        $this->session->unset_userdata('is_login');
        
        redirect('panel');
    }

    public function dashboard() {
        // check session login
        $this->_check_session_login();
        $data['user_data'] = $this->admin_data;

        // get active slider
        $this->load->helper('slider/slider');
        $data['active_slider'] = count_slider('active');
        
        // get unread mail
        $this->load->helper('mail/mail');
        $data['unread_msg'] = count_mail('unread');
        
        // get total active article
        $this->load->helper('article/article');
        $data['active_article'] = count_article('active');
        
        $this->load->view('dashboard', $data);
    }
   
    #USER SECTION
    public function add_user() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->view('user/register', $data);
    }

    public function list_user($page=1)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $data['limit']['per_page'] = 10;
        $this->load->helper('user/user');
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_user'),
                'total_rows'    =>  count_list_user(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $params = array(
            'fields' => array(
                'tp_accounts.id AS user_id',
                'tp_accounts.username',
                'tp_accounts.password'
            )
        );
        $data['user_list'] = list_user($params);
        $this->load->view('user/list_user', $data);    
    }
    #END OF USER SECTION
    
    
    #STAFF POSITION SECTION#
    public function add_staff_position()
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->view('staff_position/add_staff_position');
    }
    
    public function list_staff_position($page=1)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('staff_position/Staff_position');
        $data['limit']['per_page'] = 10;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_staff_position'),
                'total_rows'    =>  count_list_staff_position(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['staff_position_list'] = list_staff_position($data);
        $this->load->view('staff_position/list_staff_position', $data);    
    }
    
    public function update_staff_position($id)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('staff_position/Staff_position');
        $data['where'] = array(
            'id'    =>  $id
        );
        $data['staff_position'] = get_staff_position($data);
        $this->load->view('staff_position/update_staff_position', $data);
    }
    #END OF STAFF POSITION SECTION#
    
   
    #STAFF SECTION #
    public function add_staff() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('staff_position/Staff_position');
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_staff_position'] = list_staff_position($data);
        
        //if there is no staff position, redirect user to fill the staff position
        if(empty($data['list_staff_position']))
        {
            redirect(site_url('panel/add_staff_position'));
        }

        $this->load->view('staff/add_staff', $data);
    }
    
    public function list_staff($page=1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('staff/Staff');
        $data['limit']['per_page'] = 10;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_staff'),
                'total_rows'    =>  count_list_staff(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        // get event
        $data['fields'] = array(
            'tp_staff.id',
            'tp_staff_position.name AS position',
            'tp_staff.fname',
            'tp_staff.lname',
            'tp_staff.logo',
            'tp_staff.about_staff',
            'tp_staff.status',
        );
        $data['join'] = array(
            'tp_staff_position' => 'tp_staff.id_staff_position = tp_staff_position.id'
        );
        $data['staff_list'] = list_staff($data);
        $this->load->view('staff/list_staff', $data);    
    }
    
    public function staff_detail($id){
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        //get staff detail
        $this->load->helper('staff/Staff');
        $data['staff_param'] = array(
            'fields' => array(
                'tp_staff.*',
                'tp_staff_position.name AS position'
            ),
            'join' => array(
                'tp_staff_position' => 'tp_staff.id_staff_position = tp_staff_position.id'
            ),
            'where' => array(
                'tp_staff.id' => $id
            )
        );
        $get_staff = get_staff($data['staff_param']);
        if( !empty($get_staff) ) {
            $data['staff'] = array_shift($get_staff);
        }

        //get list of staff position
        $this->load->helper('staff_position/Staff_position');
        $page = 1;
        $data['staff_position']['limit']['per_page'] = 200;
        $data['staff_position']['limit']['start'] = ($page-1)*$data['staff_position']['limit']['per_page'];
        $data['list_staff_position'] = list_staff_position($data['staff_position']);

        $this->load->view('staff/staff_detail', $data);
    }
    #END OF STAFF SECTION #
    
    
    #CAREER SECTION#
    public function add_career()
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        //load staff position data
        $this->load->helper('staff_position/Staff_position');
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_staff_position'] = list_staff_position($data);    
        //if there is no staff position, redirect user to fill the staff position
        if(empty($data['list_staff_position']))
        {
            redirect(site_url('panel/add_staff_position'));
        }

        $this->load->view('career/add_career', $data);
    }
    
    public function list_career($page=1)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('career/Career');
        $data['limit']['per_page'] = 10;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_career'),
                'total_rows'    =>  count_list_career(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['career_list'] = list_career($data);
        
        //add position name to each position in job vacancy
        $this->load->helper('staff_position/Staff_position');
        if(!empty($data['career_list']))
        {
            foreach($data['career_list'] AS $k=>$v)
            {
                $data['position_params']['where'] = array(
                    'id'    => $v->id_staff_position
                );
                $position_name = get_staff_position($data['position_params']);
                $position_name = $position_name[0]->name;
                if(!empty($position_name))
                {
                    $data['career_list'][$k]->staff_position = $position_name;
                }
                else
                {
                    $data['career_list'][$k]->staff_position = $v->id_staff_position;
                }
            }
        }
        
        $this->load->view('career/list_career', $data);    
    }
    
    public function update_career($id)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('career/Career');
        $data['where'] = array(
            'id'    =>  $id
        );
        $data['career'] = get_career($data);
        
        //load staff position data
        $this->load->helper('staff_position/Staff_position');
        $page = 1;
        $data['staff_position_params']['limit']['per_page'] = 200;
        $data['staff_position_params']['limit']['start'] = ($page-1)*$data['staff_position_params']['limit']['per_page'];
        $data['list_staff_position'] = list_staff_position($data['staff_position_params']);    
        //if there is no staff position, redirect user to fill the staff position
        if(empty($data['list_staff_position']))
        {
            redirect(site_url('panel/add_staff_position'));
        }
                
        $this->load->view('career/update_career', $data);
    }
    #END OF CAREER SECTION#
    
    
    #COURSE SECTION#
    public function add_courses()
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;       

        $this->load->view('courses/add_courses', $data);
    }
    
    public function list_courses($page=1)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('courses/Courses');
        $data['limit']['per_page'] = 10;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_career'),
                'total_rows'    =>  count_list_courses(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['courses_list'] = list_courses($data);                
        
        $this->load->view('courses/list_courses', $data);    
    }
    
    public function update_courses($id)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('courses/Courses');
        $data['where'] = array(
            'id'    =>  $id
        );
        $data['courses'] = get_courses($data);               
                
        $this->load->view('courses/update_courses', $data);
    }
    #END OF COURSE SECTION#
    
    
    #CLASS SECTION #
    public function add_classes() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        //get courses data list
        $this->load->helper('courses/Courses');
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_courses'] = list_courses($data);
        //if there is no course registered, redirect user to fill the course data first
        if(empty($data['list_courses']))
        {
            redirect(site_url('panel/add_courses'));
        }

        $this->load->view('classes/add_classes', $data);
    }
    
    public function list_classes($page=1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('classes/Classes');
        $data['limit']['per_page'] = 10;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_classes'),
                'total_rows'    =>  count_list_classes(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
                
        $data['classes_list'] = list_classes($data);
        $this->load->view('classes/list_classes', $data);    
    }
    
    public function classes_detail($id){
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        //get class detail
        $this->load->helper('classes/Classes');
        $data['classes_param'] = array(
            'where' => array(
                'id' => $id
            )
        );
        $data['classes'] = get_classes($data['classes_param']);
        
        //get the list of courses data
        $this->load->helper('courses/Courses');
        //assign course data to the class
        foreach($data['classes'] as $k=>$v)
        {            
            $data['courses_param'] = array(
                'where' => array(
                    'id' => $v->id_course
                )
            );
            $course_data = get_courses($data['courses_param']);
            if(!empty($course_data))
            {
                $data['classes'][$k]->course_title = $course_data[0]->title;
            }
        }        
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_courses'] = list_courses($data);
        
        //get class assignment data
        $data['classes_assignment_param'] = array(
            'fields' => array(
                'tp_classes_assignment.id',
                'tp_classes_assignment.id_staff',
                'tp_classes_assignment.id_classes',
                'tp_staff.id AS staff_id',
                'tp_staff.fname',
                'tp_staff.lname',
                'tp_staff.logo',
                'tp_staff_position.name AS staff_position'
            ),
            'join'  => array(
                'tp_staff' => 'tp_staff.id = tp_classes_assignment.id_staff',
                'tp_staff_position' => 'tp_staff_position.id = tp_staff.id_staff_position'
            ),
            'where' => array(
                'tp_classes_assignment.id_classes' => $id
            )
        );
        $data['classes_assignment_list'] = list_classes_assignment($data['classes_assignment_param']);
        
        #print_r($data['classes_assignment_list']);exit;
        $this->load->view('classes/classes_detail', $data);
    }
    
    public function add_classes_assignment()
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        //load staff data
        $this->load->helper('staff/Staff');
        $page = 1;
        $data['staff_params']['limit']['per_page'] = 200;
        $data['staff_params']['limit']['start'] = ($page-1)*$data['staff_params']['limit']['per_page'];
        $data['list_staff'] = list_staff($data['staff_params']);    
        //assign position name for the staff
        $this->load->helper('staff_position/Staff_position');
        foreach($data['list_staff'] as $k=>$v)
        {                        
            $data['staff_position_params']['where'] = array(
                'id'    =>  $v->id_staff_position
            );
            $staff_position = get_staff_position($data['staff_position_params']);
            if(!empty($staff_position))
            {
                $data['list_staff'][$k]->staff_position = $staff_position[0]->name;
            }
        }
        //if there is no staff position, redirect user to fill the staff position
        if(empty($data['list_staff']))
        {
            redirect(site_url('panel/add_staff'));
        }

        //load class data
        $this->load->helper('classes/Classes');
        $page = 1;
        $data['classes_params']['limit']['per_page'] = 200;
        $data['classes_params']['limit']['start'] = ($page-1)*$data['classes_params']['limit']['per_page'];
        $data['list_classes'] = list_classes($data['classes_params']);    
        //if there is no staff position, redirect user to fill the staff position
        if(empty($data['list_classes']))
        {
            redirect(site_url('panel/add_classes'));
        }
        
        #print_r($data);exit;
        $this->load->view('classes/add_classes_assignment', $data);
    }
    # END OF CLASS SECTION #
    
    
    #NEWS TYPE SECTION#
    public function add_news_type()
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->view('news/add_news_type');
    }
    
    public function list_news_type($page=1)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('news/News');
        $data['limit']['per_page'] = 10;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_news_type'),
                'total_rows'    =>  count_list_news_type(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['news_type_list'] = list_news_type($data);
        $this->load->view('news/list_news_type', $data);    
    }
    
    public function update_news_type($id)
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('news/News');
        $data['where'] = array(
            'id'    =>  $id
        );
        $data['news_type'] = get_news_type($data);
        $this->load->view('news/update_news_type', $data);
    }
    #END OF NEWS TYPE SECTION#
    
    
    #NEWS SECTION #
    public function add_news() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('news/News');
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_news_type'] = list_news_type($data);
        
        //if there is no staff position, redirect user to fill the staff position
        if(empty($data['list_news_type']))
        {
            redirect(site_url('panel/list_news_type'));
        }
        $this->load->view('news/add_news', $data);
    }
    
    public function list_news($page=1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('news/News');
        $data['limit']['per_page'] = 10;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_news'),
                'total_rows'    =>  count_list_news(),
                'per_page'      =>  $data['limit']['per_page']
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        
        $data['fields'] = array(
            'tp_news.id',
            'tp_news.id_news_type',
            'tp_news.title',
            'tp_news.annotation',
            'tp_news.content',
            'tp_news.date_add',
            'tp_news.date_updated',
            'tp_news.logo',
            'tp_news.scheduling',
            'tp_news.status',
            'tp_news_type.name AS news_type'
        );
        $data['order_by'] = array(
            'tp_news.id'  => 'DESC'
        );
        $data['join'] = array(
            'tp_news_type' => 'tp_news.id_news_type = tp_news_type.id'
        );
        $data['news_list'] = list_news($data);
        $this->load->view('news/list_news', $data);    
    }
    
    
    public function news_detail($id){
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        //get news detail
        $this->load->helper('news/News');
        $data['news_param'] = array(
            'fields' => array(
                'tp_news.id',
                'tp_news.id_news_type',
                'tp_news.title',
                'tp_news.annotation',
                'tp_news.content',
                'tp_news.date_add',
                'tp_news.date_updated',
                'tp_news.logo',
                'tp_news.scheduling',
                'tp_news.status',
                'tp_news_type.name AS news_type'
            ),
            'join' => array(
                'tp_news_type' => 'tp_news.id_news_type = tp_news_type.id'
            ),
            'where' => array(
                'tp_news.id' => $id
            )
        );
        $data['news'] = get_news($data['news_param']);       
        
        //get news type
        $page = 1;
        $data['limit']['per_page'] = 200;
        $data['limit']['start'] = ($page-1)*$data['limit']['per_page'];
        $data['list_news_type'] = list_news_type($data);
        
        //get news tags
        $data['news_tags'] = array(
            'where' => array(
                'id_news'   => $id
            )
        );
        $data['list_news_tags'] = list_news_tags($data['news_tags']);
        
        $this->load->view('news/news_detail', $data);
    }
    #END OF NEWS SECTION #
    
    
    #GENERAL INFO SECTION #
    public function add_general_info() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;       

        $this->load->library('pagination');
        $this->load->helper('general_info/General_info');                      
        $data['general_info_type_list'] = list_general_info_type($data);
        
        if(empty($data['general_info_type_list']))
        {
            redirect(site_url('panel/add_general_info_type'));
        }
        
        $this->load->view('general_info/add_general_info', $data);
    }
    
    public function list_general_info($page=1) {
        // check session login
//        if( empty($this->admin_data) ) {
//            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
//            redirect('panel'.$this->redirect);
//        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('general_info/General_info');
        $per_page = 10;
        $limit_start = ($page-1)*$per_page;
        $data['general_info_list_params'] = array(
            'limit' => array(
                'per_page'  => $per_page,
                'start'     => $limit_start
            ),
            'fields' => array(
                'tp_general_info.id',
                'title',
                'annotation',
                'content',
                'type',
                'logo',
                'tp_general_info_type.name AS type_name'
            ),
            'join' => array(
                'tp_general_info_type' => 'tp_general_info.type = tp_general_info_type.id'
            ),
            'order_by'  => array(
                'tp_general_info.id' => 'DESC'
            )
        );
        $data['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_general_info'),
                'total_rows'    =>  count_list_general_info(),
                'per_page'      =>  $per_page
        );     
        $config = paging_config($data['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
               
        $data['general_info_list'] = list_general_info($data['general_info_list_params']);
        $data['general_info_type_list'] = list_general_info_type(NULL);
        
        $this->load->view('general_info/list_general_info', $data);    
    }
        
    
    public function general_info_detail($id){
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        //get general info detail
        $this->load->helper('general_info/General_info');
        $data['general_info_param'] = array(
            'fields' => array(
                'tp_general_info.id',
                'title',
                'annotation',
                'content',
                'type',
                'logo',
                'tp_general_info_type.name AS type_name'
            ),
            'join' => array(
                'tp_general_info_type' => 'tp_general_info.type = tp_general_info_type.id'
            ),
            'where' => array(
                'tp_general_info.id' => $id
            )
        );
        $data['general_info'] = get_general_info($data['general_info_param']);
        
        
        $data['general_info_type_list'] = list_general_info_type($data);        
        if(empty($data['general_info_type_list']))
        {
            redirect(site_url('panel/add_general_info_type'));
        }
        
        $this->load->view('general_info/general_info_detail', $data);
    }
    #END OF GENERAL INFO SECTION #

    # CONTACT US SECTION #
    public function list_contact_us($page=1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->library('pagination');
        $this->load->helper('contact_us/contact_us');
        $params['limit']['per_page'] = 10;
        $params['limit']['start'] = ($page-1)*$params['limit']['per_page'];
        $params['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_contact_us'),
                'total_rows'    =>  count_list_contact_us(),
                'per_page'      =>  $params['limit']['per_page']
        );                
        $config = paging_config($params['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['contact_us_list'] = list_contact_us($params);
        $this->load->view('contact_us/list_contact_us', $data);
    }

    public function contact_us_detail($id){
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('contact_us/contact_us');        
        
        $data['contact_us_param'] = array(
            'where' => array(
                'id' => $id
            ),
        );
        $data['contact_us'] = get_contact_us($data['contact_us_param']);
        
        $this->load->view('contact_us/contact_us_detail', $data);
    }
    # END OF CONTACT US SECTION #
    
    
    #START OF UI SECTION#
    public function list_ui()
    {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        $this->load->helper('ui/Ui');
                
        $data['lang_list'] = list_lang(NULL);
        #print_r($data['lang_list']);exit;
        $this->load->view('ui/list_ui', $data);    
    }    
    #END OF UI SECTION#
    
    #================================== FUND RAISING ================================#
    public function add_file() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        $data['error_file'] = '';

        $this->load->view('fund_raising/add_file', $data);
    }
    
    public function list_donation($page=1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $this->load->model('fund_raising/Fund_raising_model', 'fund_raising');
        $this->load->library('pagination');
        $params['limit']['per_page'] = 10;
        $params['limit']['start'] = ($page-1)*$params['limit']['per_page'];
        $params['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_donation'),
                'total_rows'    =>  $this->fund_raising->countTotalDonation(),
                'per_page'      =>  $params['limit']['per_page']
        );
        $config = paging_config($params['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        // get donation list
        $data['donationList'] = $this->fund_raising->getDonationList();
        
        // laod view
        $this->load->view('fund_raising/list_donation', $data);
    }

    public function donation_detail($donationId) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        // check status of donation, if user has uploaded bill or not
        $this->load->model('fund_raising/Fund_raising_model');
        $checkDonationStatus = $this->Fund_raising_model->getCurrentDonationStatusById($donationId);
        
        if( $checkDonationStatus['status'] != 0 ) {
            $transaction = $this->Fund_raising_model->getDetailDonationById($donationId);
            $confirmation = $this->Fund_raising_model->getFundConfirmationByDonationId($transaction['id']);

            $this->load->helper('bank/bank');
            $bank = get_bank_by_id($confirmation['bank_id']);

            $this->load->model('member/Member_model', 'member');
            $member = $this->member->getMemberDataById($transaction['member_id']);
            $data['donationDetail'] = array_merge($transaction, $confirmation, $bank, $member);
        }
        
        $this->load->view('fund_raising/donation_detail', $data);
    }
    
    public function list_donator($page=1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;

        
        $this->load->model('member/Member_model', 'member');
        $this->load->library('pagination');
        $params['limit']['per_page'] = 10;
        $params['limit']['start'] = ($page-1)*$params['limit']['per_page'];
        $params['paging_config'] = array(
                'base_url'      =>  site_url('panel/list_donation'),
                'total_rows'    =>  $this->member->countTotalMember(),
                'per_page'      =>  $params['limit']['per_page']
        );
        $config = paging_config($params['paging_config']);
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();

        // get donation list
        $data['donatorList'] = $this->member->getDonatorList();
        
        // laod view
        $this->load->view('fund_raising/list_donator', $data);
    }

    public function donator_detail($donatorId) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['post_data'] = array();
        $data['error_logo'] = '';

        $this->load->model('member/Member_model', 'member');
        $data['donatorDetail'] = $this->member->getMemberDataById($donatorId);
        
        $this->load->view('fund_raising/donator_detail', $data);
    }
    #================================== FUND RAISING ================================#

    #========================== BANK MODULES > OWNER ACCOUNTS =======================#
    public function list_owner_accounts($page = 1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $this->load->helper('bank/bank');
        $data['ownerAccountList'] = get_owner_account(true);
        
        $this->load->view('bank/list-owner-account', $data);
    }
    
    public function add_owner_account() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $this->load->helper('bank/bank');
        $data['bankList'] = get_bank_list();
        
        $this->load->view('bank/add-owner-account', $data);
    }
    
    public function owner_account_detail($ownerAccountId) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['ownerAccountDetail'] = array();
        if( !empty($ownerAccountId) && $ownerAccountId != 0 ) {
            $this->load->helper('bank/bank');
            $data['ownerAccountDetail'] = get_owner_account_by_id($ownerAccountId);
        }
        
        $data['post_data'] = array();
        
        $this->load->view('bank/detail-owner-account', $data);
    }
    #========================== BANK MODULES > OWNER ACCOUNTS =======================#
    
    #========================== BANK MODULES > BANK MASTER =========================#
    public function list_banks($page = 1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $this->load->helper('bank/bank');
        $data['bankList'] = get_bank_list(true);
        
        $this->load->view('bank/list-bank', $data);
    }
    
    public function add_bank() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $this->load->helper('bank/bank');
        $data['bankList'] = get_bank_list();
        
        $this->load->view('bank/add-bank', $data);
    }
    
    public function bank_detail($bankId) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['post_data'] = array();
        
        $data['bankDetail'] = array();
        if( !empty($bankId) && $bankId != 0 ) {
            $this->load->helper('bank/bank');
            $data['bankDetail'] = get_bank_by_id($bankId);
        }
        
        $this->load->view('bank/detail-bank', $data);
    }
    #========================== BANK MODULES > BANK MASTER =========================#
    
    #================================ SLIDER MODULES ===============================#
//    public function slider($action='list', $param='') {
//        $data['user_data'] = $this->admin_data;
//        
////        $this->load->helper('slider/slider');
////        $data['sliderList'] = get_list_slider();
//        
//        $this->CI->load->view('slider/'.$action.'-slider');
//    }
    
    public function add_slider() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['error_image'] = '';
        
        $this->load->view('slider/add-slider', $data);
    }
    
    public function slider_detail($sliderId='') {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['post_data'] = array();
        
        $data['sliderDetail'] = array();
        if( !empty($sliderId) && $sliderId != 0 ) {
            $this->load->helper('slider/slider');
            $data['sliderDetail'] = get_slider_by_id($sliderId);
        }
        
        $this->load->view('slider/detail-slider', $data);
    }
    #================================ SLIDER MODULES ===============================#
    
    #============================= SOCIAL MEDIA MODULE =============================#
    public function add_social_media() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['error_image'] = '';
        
        $this->load->view('social_media/add-social-media', $data);
    }
    
    public function list_social_media($page = 1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        // default variable
        $limit_per_page = 10;
        $start_row = ($page-1) * $limit_per_page;
        
        $this->load->helper('social_media/social_media');
        // count total rows
        $count_sosmed = count_social_media();
        
        // parameters
        $whereParams = array(
            'limit' => array($start_row,$limit_per_page)
        );
        $data['sosmedList'] = get_social_media($whereParams);
        
        // set pagination if returned data more than 10
        if( $count_sosmed > $limit_per_page ) {
            $this->load->library('pagination');
            $paging_config = array(
                'base_url'      =>  site_url('panel/list_social_media'),
                'total_rows'    =>  $count_sosmed,
                'per_page'      =>  $limit_per_page
            );     
            $config = paging_config($paging_config);
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();
        }
        
        $this->load->view('social_media/list-social-media', $data);
    }
    
    public function social_media_detail($gid='') {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['post_data'] = array();
        
        $data['detail'] = array();
        if( !empty($gid) ) {
            $this->load->helper('social_media/social_media');
            $data['detail'] = get_social_media($gid);
        }
        
        $this->load->view('social_media/social-media-detail', $data);
    }
    
    public function add_sosmed_account() {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        // get list social media
        $this->load->helper('social_media/social_media');
        $data['sosmedList'] = get_social_media();
        
        $this->load->view('social_media/add-social-media-account', $data);
    }
    
    public function list_sosmed_account($page=1) {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        // default variable
        $limit_per_page = 10;
        $start_row = ($page-1) * $limit_per_page;
        
        $this->load->helper('social_media/social_media');
        // count total rows
        $count_sosmed = count_social_media();
        
        // parameters
        $whereParams = array(
            'limit' => array($start_row,$limit_per_page)
        );
        $account_list = get_social_media_account($whereParams);
        foreach($account_list as $k => $row) {
            // get social media name
            $sosmed = get_social_media($row['id_sosmed']);
            $row['sosmed_name'] = $sosmed['name'];
            $row['url'] = $sosmed['url'];
            $account_list[$k] = $row;
        }
        
        $data['accountList'] = $account_list;
        
        // set pagination if returned data more than 10
        if( $count_sosmed > $limit_per_page ) {
            $this->load->library('pagination');
            $paging_config = array(
                'base_url'      =>  site_url('panel/social_media_account'),
                'total_rows'    =>  $count_sosmed,
                'per_page'      =>  $limit_per_page
            );     
            $config = paging_config($paging_config);
            $this->pagination->initialize($config);
            $data['pagination'] = $this->pagination->create_links();
        }
//        print_r($data); exit;
        $this->load->view('social_media/list-social-media-account', $data);
    }
    
    public function sosmed_account_detail($id='') {
        // check session login
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel'.$this->redirect);
        }
        $data['user_data'] = $this->admin_data;
        
        $data['post_data'] = array();
        
        $data['detail'] = array();
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $this->load->helper('social_media/social_media');
            
            $detail = get_social_media_account($id);
            $sosmed = get_social_media($detail['id_sosmed']);
            $detail['sosmed_name'] = $sosmed['name'];

            $data['detail'] = $detail;
        }
        
        $this->load->view('social_media/social-media-account-detail', $data);
    }
    #================================ SOCIAL MEDIA MODULES ===============================#
}
