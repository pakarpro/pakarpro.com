<?php

if(!function_exists('read_key')){
    function read_key($lenght, $pos=0)
    {
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/key.txt";
        $return = '';
        $fp = fopen($file, 'rb');
        if(!$fp) {
            // show something
        }
        $fseek = fseek($fp, $pos, SEEK_SET);
        if($fseek == -1) {
            // show something
        }
        $data = fread($fp, $lenght);
        $data = unpack("h*", $data);
        $arr = str_split(current($data), 2);
        foreach($arr as $val) {
            $return .= chr(hexdec($val));
        }
        fclose($fp);
        return $return;    
    }
}

if(!function_exists('write_key')){
    function write_key($key) {
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/key.txt";
        $fp = fopen($file, 'wb');
        $len = strlen($key);
        for ($i = 0; $i < $len; ++$i) {
            $hx = dechex(ord($key[$i]));
            $result = fwrite($fp, pack("h*", $hx));
            if(!$result) {
                // show something
            }
        }
        fclose($fp);
    }
}
if(!function_exists('write_iv')){
    function write_iv() {
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/iv.txt";
        $fp = fopen($file, 'wb');
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        fwrite($fp, $iv);
        /*
        for ($i = 0; $i < $iv_size; ++$i) {
            $hx = dechex(ord($iv[$i]));
            $result = fwrite($fp, pack("h*", $hx));
            if(!$result) {
                // show something
            }
        }
         * 
         */
        fclose($fp);
    }
}

if(!function_exists('read_iv')){
    function read_iv($pos=0)
    {
        $lenght = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/iv.txt";
        $return = '';
        $fp = fopen($file, 'rb');
        if(!$fp) {
            // show something
        }
        $fseek = fseek($fp, $pos, SEEK_SET);
        if($fseek == -1) {
            // show something
        }
        $data = fread($fp, $lenght);
        #$data = unpack("h*", $data);
        /*
        $arr = str_split(current($data), 2);
        foreach($arr as $val) {
            $return .= chr(hexdec($val));
        }
         * 
         */
        fclose($fp);
        return $data;    
    }
}
if(!function_exists('encrypt')){
    function encrypt($str, $key, $iv)
    {
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key,
                                     $str, MCRYPT_MODE_CBC, $iv);
        $ciphertext = $iv . $ciphertext;

        $ciphertext_base64 = base64_encode($ciphertext);
        return $ciphertext_base64;
    }
}
if(!function_exists('decrypt')){
    function decrypt($lenght, $pos=0)
    {
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/key.txt";
        $return = '';
        $fp = fopen($file, 'rb');
        if(!$fp) {
            // show something
        }
        $fseek = fseek($fp, $pos, SEEK_SET);
        if($fseek == -1) {
            // show something
        }
        $data = fread($fp, $lenght);
        $data = unpack("h*", $data);
        $arr = str_split(current($data), 2);
        foreach($arr as $val) {
            $return .= chr(hexdec($val));
        }
        fclose($fp);
        return $return;    
    }
}
