<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {
    
    private $CI;
    private $article;
    private $article_status = 1;
    private $category_status = 1;
    private $page = 1;
    private $redirect = 'panel/login';
    private $all = 'article';
    private $category = 'category';
    private $tag = 'tag';
    private $img_path;
    private $temp_path;
    private $admin_data = array();
    private $article_validation_config = array(
        array(
            'field'   => 'category',
            'label'   => 'Category',
            'rules'   => 'trim'
        ),
        array(
            'field'   => 'tag',
            'label'   => 'Tag',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'title',
            'label'   => 'Title',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'content',
            'label'   => 'Content',
            'rules'   => 'trim|required'
        ),     
        array(
            'field'   => 'date_start',
            'label'   => 'Date schedule start',
            'rules'   => 'trim|required|callback_checkDateFormat'
        ),     
        array(
            'field'   => 'date_end',
            'label'   => 'Date schedule end',
            'rules'   => 'trim|required|callback_checkDateFormat'
        ),     
    );
    private $category_validation_config = array(
        array(
            'field'   => 'category_name',
            'label'   => 'Category name',
            'rules'   => 'trim|required'
        )
    );
    private $tag_validation_config = array(
        array(
            'field'   => 'tag_name',
            'label'   => 'Tag name',
            'rules'   => 'trim|required'
        )
    );
    private $gid_validation = array(
        'field'   => 'gid_custom',
        'label'   => 'Keyword',
        'rules'   => 'trim|required'
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'is_unique' => '%s is already taken',
        'edit_unique' => '%s is already taken',
        'checkDateFormat' => 'Invalid date and time format',
    );

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();

        $this->load->helper('form');
        $this->load->model('Article_model');
        $this->article = new Article_model();
        
        // image path
        $this->img_path = FCPATH.IMG_PATH.'article/';
        $this->temp_path = FCPATH.IMG_PATH.'temp/article/';
         
        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // check session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
    
    // generate annotation
    private function _annotation($content) {
        $content = strip_tags($content);
        $content = preg_replace("/&#?[a-z0-9]+;/i","",$content);
        $annotation = summarize_sentence($content, 30);
        
        return $annotation;
    }
    
    // count article activity, both like and view
    private function _count_article_activity($type='view', $table, $article_id) {
        $param['where'] = array(       // set param
            'client_ip' => IP_ADDR,
            'article_id' => $article_id
        );
        $param['like'] = array(
            'date_'.$type => DATE
        );
        $count = $this->article->countArticle($table, $param);
        
        return $count;
    }
    
    // create directory recursively
    private function _create_directory($date, $id) {
        $dir = $date.'-'.$id;
        $dir = explode('-', $dir);
        foreach($dir as $key) {
            $this->img_path .= $key.'/';
            if( !file_exists($this->img_path) ) {
                mkdir($this->img_path);                               // create directory if not exist
                chmod($this->img_path, 0777);                         // set permission of directory
            }
        }
        
        return $this->img_path;
    }
    
    // formatting input data
    private function _format_input_data($data, $id='') {
        // tag
        if( isset($data['tag']) ) {
            $tags = explode(',', $data['tag']);
            foreach($tags as $item) {
                $tag[] = ucwords($item);
            }
            $data['tag'] = implode(',', $tag);
        }
        
        // title
        if( isset($data['tag']) ) {
            $data['title'] = ucwords($data['title']);
        }
        
        // category_name
        if( isset($data['category_name']) ) {
            $data['category_name'] = ucwords($data['category_name']);
        }
        
        // tag_name
        if( isset($data['tag_name']) ) {
            $data['tag_name'] = strtolower($data['tag_name']);
        }
        
        // gid
        if( isset($data['gid_custom']) ) {
            $data['gid_custom'] = strtolower($data['gid_custom']);
        }
        $gid_article = $data['gid_default'];
        if( isset($data['use_custom_gid']) ) {
            $gid_article = $data['gid_custom'];
        }
        $data['gid'] = $gid_article;    // for validation use

        return $data;
    }
    
    // get all article category
    private function _get_article_category($status=false) {
        if( $status == true ) {
            $category_param['where'] = array(
                'status' => 1
            );
        }
        $category_param['order_by'] = array(
            'gid' => 'ASC'
        );
        $category = $this->article->getArticle(ARTICLE_CATEGORY, $category_param);

        return $category;
    }
    
    // get related tag
    private function _get_article_tag($tag_param=array()) {
        $tag_param['order_by'] = array(
            ARTICLE_TAG.'.gid' => 'ASC'
        );
        $tag = $this->article->getArticle(ARTICLE_TAG, $tag_param);
        
        return $tag;
    }
    
    // show all categories as widget, for main pages
    private function _get_category_for_widget() {
        // get all categories first
        $category = $this->_get_article_category(true);
        if( !empty($category) ) {
            foreach($category as $k => $item) {
                $item['total'] = $this->_get_total_article_per_category($item['category_id']);
                
                $category[$k] = $item;
                if($item['total'] == 0) {
                    unset($category[$k]);
                }
            }
        }

        return $category;
    }
    
    // get most viewed article
    private function _get_most_viewed_article() {
        $most_viewed_param = array(
            'fields' => array(
                'gid AS gid_article',
                'title',
                'count_likes',
                'count_views'
            ),
            'where' => array(
                'count_views != ' => 0
            )
        );
        $count_most_viewed = $this->article->countArticle(ARTICLE, $most_viewed_param);
        if( $count_most_viewed > 0 ) {
            // additional where param
            $most_viewed_param['order_by'] = array('count_views' => 'DESC');
            $most_viewed_param['limit'] = 3;
            $most_viewed = $this->article->getArticle(ARTICLE, $most_viewed_param);
            
            return $most_viewed;
        } else {
            return array();
        }
    }
    
    // show all related categories
    private function _get_related_category($article_id) {
        $category_param['where'] = array(
            'article_id' => $article_id
        );
        $category = $this->article->getArticle(RELATED_CATEGORY, $category_param);
        
        return $category;
    }
    
    // show all related tags as widget, for main pages
    private function _get_related_tag($article_id) {
        $tag_param['fields'] = array(
            ARTICLE_TAG.'.id AS tag_id',
            ARTICLE_TAG.'.gid AS gid_tag',
            ARTICLE_TAG.'.name AS tag_name'
        );
        $tag_param['where'][RELATED_TAG.'.article_id'] = $article_id;
        $tag_param['join'] = array(
            RELATED_TAG => ARTICLE_TAG.'.id = '.RELATED_TAG.'.tag_id'
        );
        $tag = $this->_get_article_tag($tag_param);
        
        return $tag;
    }
    
    // get total article for each category
    private function _get_total_article_per_category($category_id) {
        // get count of each category
        $count_param['where'] = array(
            RELATED_CATEGORY.'.category_id' => $category_id,
            ARTICLE.'.status' => 1
        );
        $count_param['join'] = array(
            ARTICLE => ARTICLE.'.id = '.RELATED_CATEGORY.'.article_id'
        );

        $total = $this->article->countArticle(RELATED_CATEGORY, $count_param);
        
        return $total;
    }
    
    // get total likes for each article
    private function _get_total_like($article_id) {
        $like_param['where'] = array(
            'article_id' => $article_id
        );
        $count = $this->article->countArticle(ARTICLE_LIKE, $like_param);
        
        return $count;
    }
    
    // get total views for each article
    private function _get_total_view($article_id) {
        $view_param['where'] = array(
            'article_id' => $article_id
        );
        $count = $this->article->countArticle(ARTICLE_VIEW, $view_param);
        
        return $count;
    }
    
    // get prev and next article
    private function _set_article_pager($type='next', $article_id) {
        $order_value = 'ASC';
        $where_field = 'id >';
        if( $type == 'prev' ) {
            $where_field = 'id <';
            $order_value = 'DESC';
        }
        
        $param['fields'] = array(
            'gid AS gid_article',
            'title',
            'date_start',
        );
        $param['where'] = array(
            'status' => 1,
            "(NOW() >= date_start OR date_start = '1970-01-01 00:00:00')" => NULL,
            "(NOW() <= date_end OR date_end = '1970-01-01 00:00:00')" => NULL,
            $where_field => $article_id
        );
        $param['order_by'] = array(
            'id' => $order_value
        );
        $param['limit'] = 1;
        $article = $this->article->getArticle(ARTICLE, $param);
        if( !empty($article) ) {
            $article = array_shift($article);
        }
        
        return $article;
    }
    
    // disabling article status being 0 when date schedule end is over
    public function disable_expired_article() {
        $where_param = array(
            'date_end < ' => DATETIME,
            'status' => 1,
        );
        $count_param['where'] = $where_param;
        $count = $this->article->countArticle(ARTICLE, $count_param);
        if( $count > 0 ) {
            $update_param = array(
                'date_update' => DATETIME,
                'status' => 0
            );
            $update = $this->article->updateArticle(ARTICLE, $update_param, $where_param);
            if( $update == 'SUCCESSFUL' ) {
                echo $count.' artikel berhasil dinonaktifkan';
            }
        } else {
            echo 'all done, clean';
        }
    }
    
    // for ajax use, change status
    public function changestatus($type) {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $status = $data['status'];
            
            $update_param['status'] = $status;
            
            switch($type) {
                case 'all':
                    $update_param['date_update'] = DATETIME;
                    $table = ARTICLE;
                    break;
                case 'category':
                    $table = ARTICLE.'_'.$type;
                    break;
                default:
                    die();
            }

            $update = $this->article->updateArticle($table, $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    // counter for like and view
    public function set_article_count($type='view', $article_id='') {
        $post = $this->input->post('article_id');
        if( !empty($post) && $post != 0 ) {
            $article_id = $post;
        }
        
        $table = ARTICLE.'_by_'.$type;      // set table
        $count = $this->_count_article_activity($type, $table, $article_id);
        if( $count == 0 ) {                                                // if client has not seen this article today
            // soon will using API                                              // save to article_by_view_table
            $view_counter_param = array(
                'date_'.$type => DATETIME,
                'client_ip' => IP_ADDR,
                'article_id' => $article_id,
            );
            $this->article->setNewArticle($table, $view_counter_param);   // save to article_view

            // get total views or likes
            $func_name = '_get_total_'.$type;
            $total = $this->$func_name($article_id);
            $total_param = array(
                'count_'.$type.'s' => $total
            );
            $this->article->updateArticle(ARTICLE, $total_param, $article_id);    // update total views in table article
            if( !empty($post) && $post != 0 ) {
                echo 'success';
            }
        }
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for main page
     * -------------------------------------------------------------------------
     */
    public function post($param='') {
        if( !empty($param) && !is_numeric($param) && !is_array($param) ) {
            $file = 'detail';
            $gid = $param;
            
            $article = $this->article->getArticleByGid(ARTICLE, $gid);
            if( !empty($article) ) {
                $article_id = $article['article_id'];
                
                // get related tag
                $tag = $this->_get_related_tag($article_id);
                $data['tag'] = $tag;
                
                // get already liked article
                $article['already_like'] = $this->_count_article_activity('like', ARTICLE_LIKE, $article_id);
                $article['already_view'] = $this->_count_article_activity('view', ARTICLE_VIEW, $article_id);
                
                // get next and previous article
                $prev_article = $this->_set_article_pager('prev', $article_id);
                if( !empty($prev_article) ) {
                    $data['pager']['previous'] = $prev_article;
                }
                $next_article = $this->_set_article_pager('next', $article_id);
                if( !empty($next_article) ) {
                    $data['pager']['next'] = $next_article;
                }
                
                // count view for checking if user has been visited this article or no
                $this->set_article_count('view', $article_id);

                $data['article'] = $article;
            }
        } else {
            $file = 'list';
            if( !empty($param) ) {
                $this->page = $param;
            }

            $article_param['where'] = array(
                'status' => 1,
                "(NOW() >= date_start OR date_start = '1970-01-01 00:00:00')" => NULL,
                "(NOW() <= date_end OR date_end = '1970-01-01 00:00:00')" => NULL
            );
            $count_article = $this->article->countArticle(ARTICLE, $article_param);
            if( $count_article > 0 ) {
                // set pagination
                $limit_per_page = 5;
                $start_row = ($this->page-1) * $limit_per_page;
                if( $count_article > $limit_per_page ) {
                    // additional where param
                    $article_param['limit'] = array($limit_per_page, $start_row);
                    $paging_config = array(
                        'base_url'      =>  site_url('article/post'),
                        'total_rows'    =>  $count_article,
                        'per_page'      =>  $limit_per_page
                    );
                    $config = paging_config($paging_config);
                    $this->load->library('pagination');
                    $this->pagination->initialize($config);
                    $data['pagination'] = $this->pagination->create_links();
                }
                
                // get article
                $article_param['order_by'] = array(
                    'date_start' => 'DESC'
                );
                $article = $this->article->getArticle(ARTICLE, $article_param);
                foreach($article as $k => $item) {
                    $article_id = $item['article_id'];
                    // get related tag
                    $related_tag = $this->_get_related_tag($article_id);
                    if( !empty($related_tag) ) {
                        foreach($related_tag as $i => $j) {
                            $data['tag'][$j['gid_tag']] = array(
                                'gid_tag' => $j['gid_tag'],
                                'tag_name' => $j['tag_name']
                            );
                        }
                    }
                    
                    // get already liked article
                    $item['already_like'] = $this->_count_article_activity('like', ARTICLE_LIKE, $article_id);
                    // get already viewed article
                    $item['already_view'] = $this->_count_article_activity('view', ARTICLE_VIEW, $article_id);
                    
                    $article[$k] = $item;
                }

                $data['article'] = $article;
            }
        }

        // get all category
        $data['category'] = $this->_get_category_for_widget();
        
        // get most viewed article
        $data['most_viewed'] = $this->_get_most_viewed_article();
        
        $this->load->view('main/'.$file, $data);
    }
    
    public function tag($gid, $page=1) {
        if( !empty($gid) && !is_array($gid) && !is_numeric($gid) ) {
            $this->page = $page;
            // get detail tag first
            $tag_param['where'] = array(
                'gid' => $gid
            );
            $tag = $this->article->getArticle(ARTICLE_TAG, $tag_param);
            if( !empty($tag) ) {
                $tag = array_shift($tag);
                
                // count article
                $article_param = array(
                    'where' => array(
                        RELATED_TAG.'.tag_id' => $tag['tag_id'],
                        ARTICLE.'.status' => 1,
                        "(NOW() >= ".ARTICLE.".date_start OR ".ARTICLE.".date_start = '1970-01-01 00:00:00')" => NULL,
                        "(NOW() <= ".ARTICLE.".date_end OR ".ARTICLE.".date_end = '1970-01-01 00:00:00')" => NULL
                    ),
                    'join' => array(
                        RELATED_TAG => RELATED_TAG.'.article_id = '.ARTICLE.'.id',
                    )
                );
                $count_article = $this->article->countArticle(ARTICLE, $article_param);
                if( $count_article > 0 ) {
                    $limit_per_page = 5;
                    $start_row = ($this->page-1) * $limit_per_page;
                    if( $count_article > $limit_per_page ) {
                        // additional where param
                        $article_param['limit'] = array($limit_per_page, $start_row);
                        $paging_config = array(
                            'base_url'      =>  site_url('article/tag/'.$gid),
                            'total_rows'    =>  $count_article,
                            'per_page'      =>  $limit_per_page
                        );
                        $config = paging_config($paging_config);
                        $this->load->library('pagination');
                        $this->pagination->initialize($config);
                        $data['pagination'] = $this->pagination->create_links();
                    }

                    // get article with additional param
                    $article_param['fields'] = array(
                        ARTICLE.'.id AS article_id',
                        ARTICLE.'.gid AS gid_article',
                        ARTICLE.'.date_add',
                        ARTICLE.'.date_start',
                        ARTICLE.'.title',
                        ARTICLE.'.annotation',
                        ARTICLE.'.content',
                        ARTICLE.'.image_name',
                        ARTICLE.'.count_likes',
                        ARTICLE.'.count_views'
                    );
                    $article_param['order_by'] = array(
                        ARTICLE.'.date_start' => 'DESC'
                    );
                    $article = $this->article->getArticle(ARTICLE, $article_param);
                    foreach($article as $k => $item) {
                        $article_id = $item['article_id'];
                        
                        // get related tag
                        $related_tag = $this->_get_related_tag($article_id);
                        if( !empty($related_tag) ) {
                            foreach($related_tag as $i => $j) {
                                $data['tag'][$j['gid_tag']] = array(
                                    'gid_tag' => $j['gid_tag'],
                                    'tag_name' => $j['tag_name']
                                );
                            }
                        }
                    
                        // get already liked article
                        $item['already_like'] = $this->_count_article_activity('like', ARTICLE_LIKE, $article_id);
                        // get already viewed article
                        $item['already_view'] = $this->_count_article_activity('view', ARTICLE_VIEW, $article_id);

                        $article[$k] = $item;
                    }
                    
                    $data['article'] = $article;
                }
            }
        }

        // get all category
        $data['category'] = $this->_get_category_for_widget();

        // get most viewed article
        $data['most_viewed'] = $this->_get_most_viewed_article();

        $this->load->view('main/list', $data);
    }
    
    public function category($gid, $page=1) {
        if( !empty($gid) && !is_array($gid) && !is_numeric($gid) ) {
            $this->page = $page;
            
            // get detail category first
            $category_param['where'] = array(
                'gid' => $gid
            );
            $category = $this->article->getArticle(ARTICLE_CATEGORY, $category_param);
            if( !empty($category) ) {
                $category = array_shift($category);
                
                // count article
                $article_param = array(
                    'where' => array(
                        RELATED_CATEGORY.'.category_id' => $category['category_id'],
                        ARTICLE.'.status' => 1,
                        "(NOW() >= ".ARTICLE.".date_start OR ".ARTICLE.".date_start = '1970-01-01 00:00:00')" => NULL,
                        "(NOW() <= ".ARTICLE.".date_end OR ".ARTICLE.".date_end = '1970-01-01 00:00:00')" => NULL
                    ),
                    'join' => array(
                        RELATED_CATEGORY => RELATED_CATEGORY.'.article_id = '.ARTICLE.'.id',
                    )
                );
                $count_article = $this->article->countArticle(ARTICLE, $article_param);
                if( $count_article > 0 ) {
                    $limit_per_page = 5;
                    $start_row = ($this->page-1) * $limit_per_page;
                    if( $count_article > $limit_per_page ) {
                        // additional where param
                        $article_param['limit'] = array($limit_per_page, $start_row);
                        $paging_config = array(
                            'base_url'      =>  site_url('article/category/'.$gid),
                            'total_rows'    =>  $count_article,
                            'per_page'      =>  $limit_per_page
                        );
                        $config = paging_config($paging_config);
                        $this->load->library('pagination');
                        $this->pagination->initialize($config);
                        $data['pagination'] = $this->pagination->create_links();
                    }

                    // get article with additional param
                    $article_param['fields'] = array(
                        ARTICLE.'.id AS article_id',
                        ARTICLE.'.gid AS gid_article',
                        ARTICLE.'.date_add',
                        ARTICLE.'.date_start',
                        ARTICLE.'.title',
                        ARTICLE.'.annotation',
                        ARTICLE.'.content',
                        ARTICLE.'.image_name',
                        ARTICLE.'.count_likes',
                        ARTICLE.'.count_views'
                    );
                    $article_param['order_by'] = array(
                        ARTICLE.'.date_start' => 'DESC'
                    );
                    $article = $this->article->getArticle(ARTICLE, $article_param);
                    foreach($article as $k => $item) {
                        $article_id = $item['article_id'];
                        
                        // get related tag
                        $related_tag = $this->_get_related_tag($article_id);
                        if( !empty($related_tag) ) {
                            foreach($related_tag as $i => $j) {
                                $data['tag'][$j['gid_tag']] = array(
                                    'gid_tag' => $j['gid_tag'],
                                    'tag_name' => $j['tag_name']
                                );
                            }
                        }

                        // get already liked article
                        $item['already_like'] = $this->_count_article_activity('like', ARTICLE_LIKE, $article_id);
                        // get already viewed article
                        $item['already_view'] = $this->_count_article_activity('view', ARTICLE_VIEW, $article_id);

                        $article[$k] = $item;
                    }
                    
                    $data['article'] = $article;
                }
            }
        } # end of empty gid

        // get all category
        $data['category'] = $this->_get_category_for_widget();

        // get most viewed article
        $data['most_viewed'] = $this->_get_most_viewed_article();

        $this->load->view('main/list', $data);
    }
    
    public function search($page=1) {
        $where = array(
            'status' => 1,
            "(NOW() >= date_start OR date_start = '1970-01-01 00:00:00')" => NULL,
            "(NOW() <= date_end OR date_end = '1970-01-01 00:00:00')" => NULL
        );
        
        $get_data = $this->input->get();
//        print_r($get_data); exit;
        foreach($get_data as $k => $val) {
            switch($k) {
                case 'q':
                    // for URI string purpose
                    $q = str_replace(' ', '+', $val);
                    $q = '?q='.$q;
                    
                    // search parameter
                    if(str_word_count($val) > 1) {
                        unset($or_like);
                        $val = explode(' ', $val);
                        foreach($val as $index) {
                            $like[] = "(gid LIKE '%".$index."%' OR title LIKE '%".$index."%' OR content LIKE '%".$index."%')";
                        }
                        $like = implode(' OR ', $like);
                        $where[$like] = NULL;
                    } else {
                        $where["(gid LIKE '%".$val."%' OR title LIKE '%".$val."%' OR content LIKE '%".$val."%')"] = NULL;
                    }
                    
                    // as variable in view file
                    $data['query'] = $val;
                    break;
                case 'page':
                    $this->page = $val;
                    break;
            }
        }

        $article_param['where'] = $where;
        $count_article = $this->article->countArticle(ARTICLE, $article_param);
        if( $count_article > 0 ) {
            // set pagination
            $limit_per_page = 5;
            $start_row = ($this->page-1) * $limit_per_page;
            if( $count_article > $limit_per_page ) {
                // additional where param
                $article_param['limit'] = array($limit_per_page, $start_row);
                $paging_config = array(
                    'base_url' =>  site_url('article/search'.$q),
                    'total_rows' =>  $count_article,
                    'per_page' =>  $limit_per_page,
                    'query_string_segment' => 'page',
                    'page_query_string' => TRUE,
                );
                $config = paging_config($paging_config);
                $this->load->library('pagination');
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }

            // additional param
            $article_param['fields'] = array(
                'id AS article_id',
                'gid AS gid_article',
                'title',
                'annotation',
                'date_start',
                'count_likes',
                'count_views'
            );
            $article_param['order_by'] = array(
                'date_start' => 'DESC'
            );
            $article = $this->article->getArticle(ARTICLE, $article_param);

            $data['article'] = $article;
        }
        
        // get all category
        $data['category'] = $this->_get_category_for_widget();

        // get most viewed article
        $data['most_viewed'] = $this->_get_most_viewed_article();

        $this->load->view('main/search-result', $data);
    }
    
    // service run by using cron
    public function get_external_source($limit=10) {
//        $url = 'http://rumahmurah.com/news/get_news?p='.$limit;
        $url = 'http://rumahmurah.com/news/get_news';
        $curl = curl_init();

//        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/json'
//        ));
        curl_setopt( $curl, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36");
        curl_setopt( $curl, CURLOPT_URL, $url );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, 1 );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );    # required for https urls
        curl_setopt( $curl, CURLOPT_MAXREDIRS, 10 );
        
        $response = json_decode(curl_exec($curl), true);
        
//        print_r($response); exit;
        if( !empty($response) ) {
            foreach($response as $k => $item) {
                $temp_id = $item['id'];
                $gid = strtolower($item['gid']);
                $date_start = $item['date_start'];
                $content = str_replace('<img src="', '<img class="img-responsive img-thumbnail" src="http://rumahmurah.com/', $item['content']);
//                $image_name = str_replace('_','-',strtolower($item['image_name']));
                $title = ucfirst($item['title']);
                $image_name = $item['image_name'];
                $ext_img_url = $item['img_url'];
                
                // check gid first
                $check_param['where'] = array(
                    'gid' => $gid
                );
                $check_gid = $this->article->countArticle(ARTICLE, $check_param);
                if( $check_gid == 0 ) {
//                    echo $content; exit;
//                    $img = str_replace('_','-',strtolower($image_name));
                    $save_article_param = array(
                        'gid' => $gid,
                        'site' => 'pkp',
                        'date_add' => DATETIME,
                        'date_start' => $date_start,
                        'title' => $title,
//                        'annotation' => $annotation,
//                        'content' => $content,
//                        'image_name' => $img,
                        'status' => 1,
                        'source' => 2,
                    );
                    $save_article = $this->article->setNewArticle(ARTICLE, $save_article_param);
                    if( isset($save_article['id']) ) {
//                    if( isset($save_article_param) ) {
                        $article_id = $save_article['id'];

                        // stored in tmp path first
                        $tmp_img = $this->temp_path.$image_name;    // set tmp path and its image
                        copy($ext_img_url.$image_name, $tmp_img);   // copy image from external source to temp
                        chmod($tmp_img, 0777);                      // set image permission
                        
                        // create new name for image
                        $img_prop = getimagesize($tmp_img);
                        $ext = str_replace('image/', '', $img_prop['mime']);
                        if($ext == 'jpeg') {
                            $ext = 'jpg';
                        }
                        $new_img_name = $gid.'.'.$ext;
                        
                        $path_img = $this->_create_directory(DATE, $article_id);
                        $this->load->helper('uploads/uploads');
                        crop_image(750, 400, $tmp_img, $path_img, $new_img_name);           // original photo
                        crop_image(400, 215, $tmp_img, $path_img, 'medium-'.$new_img_name);  // medium photo
//                        crop_image(200, 150, $tmp_img, $path_img, $new_img_name);         // small photo
                        
                        unlink($tmp_img);                               // remove old photo
                        
                        $to_be_removed = array(
                            '<p></p>',
                            '<p> </p>',
                            '<p>&nbsp;</p>',
                            '<p><strong></strong></p>'
                        );
                        $content = str_replace($to_be_removed,'',$content);
                        $content = trim(strip_tags($content, '<p><strong><i><img><ul><li>'));
                        $content = str_replace('&nbsp;',' ',$content);
                        // formatting annotation
                        $annotation = $this->_annotation($content);
                        $annotation = str_replace('RumahMurah.com','<b>RumahMurah.com</b>',$annotation);

                        // get image from content
                        $dom = new DOMDocument();
                        $dom->loadHTML($content);
                        $img_tags = $dom->getElementsByTagName('img');
                        $length = $img_tags->length;
                        if( $length > 0 ) {
                            foreach($img_tags as $img_tag) {
                                echo '- original - <br>';
                                echo $img_tag->getAttribute('src');
                                echo '<br>';
                                $old_src = $img_tag->getAttribute('src');
                                $old_src2 = 'http://rumahmurah.com'.str_replace('//','/',$old_src);
                                echo $old_src2 = str_replace('http://rumahmurah.comhttp:/rumahmurah.com', 'http://rumahmurah.com', $old_src);
                                echo '<br>';
                                $old_image_name = explode('/', $old_src2);
                                $old_image_name = end($old_image_name);
//                                if(file_exists($old_src)) {
                                $img_prop = getimagesize($old_src2);                     // get image properties
                                $ext = str_replace('image/', '', $img_prop['mime']);    // set extension of image
                                $quality = 8;
                                if($ext == 'jpeg') {
                                    $ext = 'jpg';
                                    $quality = 70;
                                }
//                                    $new_img_src_name = $gid.$no.'.'.$ext;                // set new name of image
                                $dest = $path_img.$old_image_name;                      // set destination path of image
                                copy($old_src2, $dest);                                  // copy image from external source to article path
                                chmod($dest, 0777);                                     // set permission of image

                                $imagecreatefrom = str_replace('/','createfrom',$img_prop['mime']);
                                $imagetype = str_replace('/','',$img_prop['mime']);
                                $i = $imagecreatefrom($dest);
                                $thumb = thumbnail_box($i, 750, 400);
                                imagedestroy($i);

                                if(is_null($thumb)){
                                    // image creation or copying failed
                                    header('HTTP/1.1 500 Internal Server Error');
                                    die('Something is gone wrong');
                                    exit;
                                }

                                $imagetype($thumb, $dest, $quality);

                                // replace old src with new src
//                                    echo $old_src = str_replace('http://rumahmurah.com', '', $old_src);
//                                    echo '<br>';
                                echo $new_src = '/'.str_replace(FCPATH, '', $dest);
                                echo '<br>';
                                $content = str_replace($old_src, $new_src, $content);
                            }
                        }
                        
                        echo $update_param['content'] = $content;
                        echo '<hr>';
                        $update_param['annotation'] = trim($annotation);

                        // update image name in db
                        $update_param['image_name'] = $new_img_name;
                        $update_article = $this->article->updateArticle(ARTICLE, $update_param, $article_id);
                        if( $update_article == 'SUCCESSFUL' ) {
                            // save related tag
                            $tags = $item['tag'];
                            foreach($tags as $i => $j) {
                                $tag_gid = str_replace("_","-",strtolower($j['name']));
                                $tag_name = str_replace("_"," ",strtolower($j['name']));
                                // check existing tag in database
                                $check_tag_param['where'] = array(
                                    'name' => $tag_name
                                );
                                $check_tag = $this->article->countArticle(ARTICLE_TAG, $check_tag_param);
                                if( $check_tag == 0 ) {             // if not exist, save new tag
                                    $save_tag_param = array(
                                        'gid' => $tag_gid,
                                        'name' => $tag_name
                                    );
                                    $save_tag = $this->article->setNewArticle(ARTICLE_TAG, $save_tag_param);
                                    if( isset($save_tag['id']) ) {
                                        $tag_id = $save_tag['id'];
                                    }
                                } else {                            // get the tag id
                                    $get_tag = $this->article->getArticle(ARTICLE_TAG, $check_tag_param);
                                    $get_tag = array_shift($get_tag);
                                    $tag_id = $get_tag['tag_id'];
                                }

                                $save_rel_tag = array(
                                    'article_id' => $article_id,
                                    'tag_id' => $tag_id
                                );
                                $this->article->setNewArticle(RELATED_TAG, $save_rel_tag);
                            }

                            // save related category
                            $save_rel_category = array(
                                'article_id' => $article_id,
                                'category_id' => 1
                            );
                            $this->article->setNewArticle(RELATED_CATEGORY, $save_rel_category);
                            exit;
                        } # end of updating image
                    } # end of adding new article
                } # end of check
            }
        } # end of responsse
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for panel page
     * -------------------------------------------------------------------------
     */
    
    // list all article data
    public function lists($type='all', $param='') {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // default
        $this->page = 1;
        $table = ARTICLE;
        $order_by = array('date_start' => 'DESC');

        if( $type != 'all' ) {
            $table = $table.'_'.$this->$type;
            $order_by = array('gid' => 'ASC');
        }
        
        if( !empty($param) && !is_array($param) && !is_numeric($param) ) {  // string parameter, mean detail of data
            $file = 'detail-'.$this->$type;
            $gid = $param;
            
            $data[$this->$type] = $this->_detail($this->$type, $gid);
        } else {
            $file = 'list-'.$this->$type;
            if( !empty($param) ) {
                $this->page = $param;
            }
            
            $count = $this->article->countArticle($table);
            if( $count > 0 ) {
                $limit_per_page = 10;
                $start_row = ($this->page-1) * $limit_per_page;

                if( $count > $limit_per_page ) {
                    // additional where param
                    $article_param['limit'] = array($limit_per_page, $start_row);

                    $this->load->library('pagination');
                    $paging_config = array(
                        'base_url'      =>  site_url('panel/article/'.$type),
                        'total_rows'    =>  $count,
                        'per_page'      =>  $limit_per_page
                    );
                    $config = paging_config($paging_config, false);
                    $this->pagination->initialize($config);
                    $data['pagination'] = $this->pagination->create_links();
                }
                $article_param['order_by'] = $order_by;
                $article = $this->article->getArticle($table, $article_param);
//                print_r($article); exit;
                foreach($article as $k => $item) {
                    if( $type == 'article' ) {
                        // get total likes
                        $item['likes'] = $this->_get_total_like($item[$this->$type.'_id']);
                        $article[$k] = $item;
                    }
                }

                if( $type == 'category' ) {
                    function build_sorter($key) {
                        return function ($a, $b) use ($key) {
                            return strnatcmp($a[$key], $b[$key]);
                        };
                    }
                    usort($article, build_sorter('category_id'));
                }

                $data[$this->$type] = $article;
            }
        }
//print_r($data); exit;/
        $this->load->view('panel/'.$file, $data);
    }
    
    // save new article data
    public function add($type='all') {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        $type = $this->$type;
        $table = ARTICLE;
        if( $type != 'article' ) {
            $table = ARTICLE.'_'.$type;
        }

        if(!empty($_POST)) {
            $post_data = $this->input->post();                          // input data
            $post_data = $this->_format_input_data($post_data);         // formatting input data
            if( $type != 'tag' ) {
                $this->article_status = format_status($post_data);          // status
            }
            $detail = $post_data;

            // validation
#---------------------------------------------------------------------------------------------------------------------#
            $this->load->library('form_validation');
            if( $type == 'article' ) {
                if( !isset($post_data['category']) ) {                          // if category is empty
                    foreach($this->article_validation_config as $key => $val) {
                        if( $val['field'] == 'category' ) {
                            $val['rules'] = $val['rules'].'|required';      // add rules "required"
                        }
                        $this->article_validation_config[$key] = $val;
                    }
                }
                
                $validation_config = $this->article_validation_config;
            } elseif( $type == 'category' ) {
                $validation_config = $this->category_validation_config;
            } elseif( $type == 'tag' ) {
                $validation_config = $this->tag_validation_config;
            }
            if( isset($post_data['use_custom_gid']) ) {                 // if custom gid is chosen
                foreach($this->gid_validation as $key => $val) {        // add new rules to check existing gid
                    if( $key == 'rules' ) {                             // in database, must be unique
                        $val = $val.'|is_unique['.$table.'.gid]';
                    }
                    $this->gid_validation[$key] = $val;
                }

                $validation_config[] = $this->gid_validation;
            }

            $this->form_validation->set_rules($validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }
#---------------------------------------------------------------------------------------------------------------------#

            if ($this->form_validation->run() == TRUE ) {
                switch($type) {
                    case 'article':
                        $categories = $post_data['category'];
                        $tags = $post_data['tag'];
                        $title = $post_data['title'];
                        $gid = $post_data['gid'];
                        $content = $post_data['content'];
                        $annotation = $this->_annotation($content);
                        $date_start = $post_data['date_start'];
                        $date_end = $post_data['date_end'];

                        // save parameter
                        $save_param = array(
                            'gid' => $gid,
                            'date_add' => DATETIME,
                            'date_start' => $date_start,
                            'date_end' => $date_end,
                            'title' => $title,
                            'annotation' => $annotation,
                            'content' => $content,
                            'status' => $this->article_status,
                        );

                        if( !empty($_FILES['image_name']['tmp_name']) ) {
                            $this->load->helper('uploads/uploads');

                            $source = $_FILES;
                            $uploaded_data = upload_image_to_temp('article', $source, $this->temp_path);
                        }

                        // if error occured
                        if( isset($uploaded_data) ) {
                            foreach($uploaded_data as $k => $value) {
                                switch($k) {
                                    case 'uploaded_data':
                                        $save = $this->article->setNewArticle(ARTICLE, $save_param);
                                        if( isset($save['id']) ) {
                                            $id = $save['id'];

                                            // create directory
                                            $img_path = $this->_create_directory(DATE, $id);
//                                            $date_directory = str_replace('-', '/', DATE).'/';
//                                            $img_path = $this->img_path.$date_directory.$id.'/';
//                                            if( !file_exists($img_path) ) {
//                                                mkdir($img_path);
//                                                chmod($img_path, 0777);
//                                            }
                                            // crop image, store in directory, update image_name in database
                                            $image_name = cropping_image('article', $value, $img_path, $title);
                                            $update_param = $image_name;

                                            $update = $this->article->updateArticle(ARTICLE, $update_param, $id);
                                            if( $update == 'SUCCESSFUL' ) {
                                                // save related category
                                                $rel_category_param['article_id'] = $id;
                                                foreach($categories as $index) {
                                                    $rel_category_param['category_id'] = $index;

                                                    $this->article->setNewArticle(RELATED_CATEGORY, $rel_category_param);
                                                }

                                                // save related tag
                                                $tags = explode(',', $tags);
                                                foreach($tags as $index) {
                                                    // check existing tag in database
                                                    $check_tag_param['where'] = array(
                                                        'name' => $index
                                                    );
                                                    $check_tag = $this->article->countArticle(ARTICLE_TAG, $check_tag_param);
                                                    if( $check_tag == 0 ) {             // if not exist, save new tag
                                                        $save_tag_param = array(
                                                            'gid' => preg_replace("/\s\'\"/","-",strtolower($index)),
                                                            'name' => $index
                                                        );
                                                        $save_tag = $this->article->setNewArticle(ARTICLE_TAG, $save_tag_param);
                                                        if( isset($save_tag['id']) ) {
                                                            $tag_id = $save_tag['id'];
                                                        }
                                                    } else {                            // get the tag id
                                                        $get_tag = $this->article->getArticle(ARTICLE_TAG, $check_tag_param);
                                                        $get_tag = array_shift($get_tag);
                                                        $tag_id = $get_tag['tag_id'];
                                                    }

                                                    $input_tags[] = $tag_id;
                                                }
        //                                        print_r($input_tags); exit;
                                                $rel_tag_param['article_id'] = $id;
                                                foreach($input_tags as $index) {
                                                    $rel_tag_param['tag_id'] = $index;

                                                    $this->article->setNewArticle(RELATED_TAG, $rel_tag_param);
                                                }

                                                // set notification
                                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                            New article has been added with image.'.
                                                          '</div>';
                                                $this->session->set_flashdata('notif', $notif);

                                                // redirect to list general info page
                                                redirect('panel/article/all/'.$gid);
                                            }
                                        }
                                    break;
                                    case 'error_image':
                                        $data[$k] = array_shift($value);
                                        break;
                                }
                            }
                        } else {
                            $save = $this->article->setNewArticle(ARTICLE, $save_param);

                            if( isset($save['id']) ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            New article has been added without image.
                                          </div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to list general info page
                                redirect('panel/article/all/'.$gid);
                            }
                        } # end of checking uploaded data
                        break;
                    case 'category':
                    case 'tag':
                        $gid = $post_data['gid'];
                        $name = $post_data[$type.'_name'];
                        if( $type == 'tag' ) {
                            $name = strtolower($name);
                        }

                        // save parameter
                        $save_param = array(
                            'gid' => $gid,
                            'name' => $name
                        );
                        if( $type == 'category' ) {
                            $save_param['status'] = $this->article_status;
                        }
                        $save = $this->article->setNewArticle($table, $save_param);
                        if( isset($save['id']) ) {
                            // set notification
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        New '.$type.' has been added.
                                      </div>';
                            $this->session->set_flashdata('notif', $notif);

                            // redirect to list general info page
                            redirect('panel/article/'.$type.'/'.$gid);
                        }
                        break;
                }
            } # end of validation
        } # end of post

        // get article category for type 'all' only
        if( $type == 'article' ) {
            // get all category with active status for adding new article
            $data['category'] = $this->_get_article_category(true);
        }
        
        // view page (add article)
        $data['status'] = $this->article_status;
        
//        print_r($data); exit;
        $this->load->view('panel/add-'.$type, $data);
    }
    
    // update article data, operated by admin
    private function _detail($type, $gid) {
        $data = array();
        $table = ARTICLE;
        if( $type != 'article' ) {
            $table = ARTICLE.'_'.$type;
        }
        $detail = $this->article->getArticleByGid($table, $gid);
//        print_r($detail); exit;
        if( !empty($detail) ) {
            $id = $detail[$type.'_id'];
            
            if( $type == 'article' ) {
                // formatting date end
                if($detail['date_end'] == '1970-01-01 00:00:00') {
                    $detail['date_end'] = '';
                }
                
                // get related tag
                $related_tag = $this->_get_related_tag($id);
                foreach($related_tag as $k => $item) {
                    $tag[] = $item['tag_name'];
                    $current_tag_id[] = $item['tag_id'];
                }
                $detail['tag'] = implode(',', $tag);

                // get related category
                $related_category = $this->_get_related_category($id);
                foreach($related_category as $k => $item) {
                    $current_category_id[] = $item['category_id'];
                    $category[] = $item['category_id'];
                }
                $detail['category'] = $category;
                
                // get all category with active status for adding new article
                $data['category'] = $this->_get_article_category(true);
            }

            if( $type != 'tag' ) {
                $this->article_status = $detail[$type.'_status'];
            }

            if(!empty($_POST)) {
                $post_data = $this->input->post();                          // input data
                $post_data[$type.'_id'] = $id;                              // id
                $post_data = $this->_format_input_data($post_data, $id);    // formatting input data
                $this->article_status = format_status($post_data);          // status
                $detail = array_merge($detail, $post_data);

                // validation
#---------------------------------------------------------------------------------------------------------------------#
                $this->load->library('form_validation');
                if( $type == 'article' ) {
                    if( !isset($detail['category']) ) {                          // if category is empty
                        foreach($this->article_validation_config as $key => $val) {
                            if( $val['field'] == 'category' ) {
                                $val['rules'] = $val['rules'].'|required';      // add rules "required"
                            }
                            $this->article_validation_config[$key] = $val;
                        }
                    }
                    $validation_config = $this->article_validation_config;
                } elseif( $type == 'category' ) {
                    $validation_config = $this->category_validation_config;
                } else {
                    $validation_config = $this->tag_validation_config;
                }

                if( isset($detail['use_custom_gid']) ) {                     // if custom gid is chosen
                    foreach($this->gid_validation as $key => $val) {        // add new rules to check existing gid
                        if( $key == 'rules' ) {                             // in database, must be unique
                            $val = $val.'|edit_unique['.$table.'.gid.'.$detail['gid'].']|is_unique['.$table.'.gid]';
                        }
                        $this->gid_validation[$key] = $val;
                    }
                    
                    $validation_config[] = $this->gid_validation;
                }

                $this->form_validation->set_rules($validation_config);
                foreach($this->validation_msg as $key => $msg) {
                    $this->form_validation->set_message($key, $msg);
                }
#---------------------------------------------------------------------------------------------------------------------#

                if ($this->form_validation->run() == TRUE ) {
                    switch($type) {
                        case 'article':
                            $categories = $detail['category'];
                            $tags = $detail['tag'];
                            $title = $detail['title'];
                            $gid = $detail['gid'];
                            $content = $detail['content'];
                            $annotation = $this->_annotation($content);
                            $date_start = $detail['date_start'];
                            $date_end = $detail['date_end'];
                            $image_name = $detail['image_name'];

                            // update parameter
                            $update_param = array(
                                'gid' => $gid,
                                'date_update' => DATETIME,
                                'date_start' => $date_start,
                                'date_end' => $date_end,
                                'title' => $title,
                                'annotation' => $annotation,
                                'content' => $content,
                                'status' => $this->article_status,
                            );

                            if( !empty($_FILES['image_name']['tmp_name']) ) {
                                $this->load->helper('uploads/uploads');

                                $source = $_FILES;
                                $uploaded_data = upload_image_to_temp('slider', $source, $this->temp_path);
                            }

                            // if error occured
                            if( isset($uploaded_data) ) {
                                foreach($uploaded_data as $k => $value) {
                                    switch($k) {
                                        case 'uploaded_data':
                                            // create directory
                                            $exp_date = explode(' ', $detail['date_add']);
                                            $date_directory = $exp_date[0];
                                            $img_path = $this->_create_directory($date_directory, $id);
//                                            $img_path = $this->img_path.$date_directory.$id.'/';
//                                            if( !file_exists($img_path) ) {
//                                                mkdir($img_path);
//                                                chmod($img_path, 0777);
//                                            }

                                            // unlink old photo
                                            $glob = glob($img_path.'*'.$image_name);
                                            if( !empty($glob) ) {
                                                foreach($glob as $item) {
                                                    unlink($item);
                                                }
                                            }

                                            // crop image, store in directory, update image_name in database
                                            $image_name = cropping_image('article', $value, $img_path, $title);
                                            $update_param = array_merge($update_param, $image_name);

                                            $update = $this->article->updateArticle($table, $update_param, $id);
                                            if( $update == 'SUCCESSFUL' ) {
                                                // update related category
                                                $merge_category = array_merge($categories, $current_category_id);
                                                $merge_category = array_unique($merge_category);
                                                $rel_category_param['article_id'] = $id;
                                                foreach($merge_category as $index) {
                                                    $rel_category_param['category_id'] = $index;
                                                    if( in_array($index, $categories) && !in_array($index, $current_category_id) ) {   //
                                                        $this->article->setNewArticle(RELATED_CATEGORY, $rel_category_param);
                                                    } elseif( !in_array($index, $categories) && in_array($index, $current_category_id) ) {
                                                        $this->article->deleteArticle(RELATED_CATEGORY, $rel_category_param);
                                                    }
                                                }

                                                // update related tag
                                                $tags = explode(',', $tags);
                                                foreach($tags as $index) {
                                                    // check existing tag in database
                                                    $check_tag_param['where'] = array(
                                                        'name' => $index
                                                    );
                                                    $check_tag = $this->article->countArticle(ARTICLE_TAG, $check_tag_param);
                                                    if( $check_tag == 0 ) {             // if not exist, save new tag
                                                        $save_tag_param = array(
                                                            'gid' => preg_replace("/\s\'\"/","-",strtolower($index)),
                                                            'name' => $index
                                                        );
                                                        $save_tag = $this->article->setNewArticle(ARTICLE_TAG, $save_tag_param);
                                                        if( isset($save_tag['id']) ) {
                                                            $tag_id = $save_tag['id'];
                                                        }
                                                    } else {                            // get the tag id
                                                        $get_tag = $this->article->getArticle(ARTICLE_TAG, $check_tag_param);
                                                        $get_tag = array_shift($get_tag);
                                                        $tag_id = $get_tag['tag_id'];
                                                    }

                                                    $input_tags[] = $tag_id;
                                                }

                                                $merge_tag = array_merge($input_tags, $current_tag_id);
                                                $merge_tag = array_unique($merge_tag);
                                                $rel_tag_param['article_id'] = $id;
                                                foreach($merge_tag as $index) {
                                                    $rel_tag_param['tag_id'] = $index;
                                                    if( in_array($index, $input_tags) && !in_array($index, $current_tag_id) ) {
                                                        $this->article->setNewArticle(RELATED_TAG, $rel_tag_param);
                                                    } elseif( !in_array($index, $input_tags) && in_array($index, $current_tag_id) ) {
                                                        $this->article->deleteArticle(RELATED_TAG, $rel_tag_param);
                                                    }
                                                }

                                                // optional for image
                                                $msg = '';
                                                if( isset($image_name) ) {
                                                    $msg = 'with image';
                                                }
                                                // set notification
                                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                            Article has been updated '.$msg.
                                                          '</div>';
                                                $this->session->set_flashdata('notif', $notif);

                                                // redirect to list general info page
                                                redirect('panel/article/all/'.$gid);
                                            }
                                        break;
                                        case 'error_image':
                                            $data[$k] = array_shift($value);
                                            break;
                                    }
                                }
                            } else {
                                $update = $this->article->updateArticle($table, $update_param, $id);
                                if( $update == 'SUCCESSFUL' ) {
                                    // update related category
                                    $merge_category = array_merge($categories, $current_category_id);
                                    $merge_category = array_unique($merge_category);
                                    $rel_category_param['article_id'] = $id;
                                    foreach($merge_category as $index) {
                                        $rel_category_param['category_id'] = $index;
                                        if( in_array($index, $categories) && !in_array($index, $current_category_id) ) {   //
                                            $this->article->setNewArticle(RELATED_CATEGORY, $rel_category_param);
                                        } elseif( !in_array($index, $categories) && in_array($index, $current_category_id) ) {
                                            $this->article->deleteArticle(RELATED_CATEGORY, $rel_category_param);
                                        }
                                    }

                                    // update related tag
                                    $tags = explode(',', $tags);
                                    foreach($tags as $index) {
                                        // check existing tag in database
                                        $check_tag_param['where'] = array(
                                            'name' => $index
                                        );
                                        $check_tag = $this->article->countArticle(ARTICLE_TAG, $check_tag_param);
                                        if( $check_tag == 0 ) {             // if not exist, save new tag
                                            $save_tag_param = array(
                                                'gid' => preg_replace("/\s\'\"/","-",strtolower($index)),
                                                'name' => $index
                                            );
                                            $save_tag = $this->article->setNewArticle(ARTICLE_TAG, $save_tag_param);
                                            if( isset($save_tag['id']) ) {
                                                $tag_id = $save_tag['id'];
                                            }
                                        } else {                            // get the tag id
                                            $get_tag = $this->article->getArticle(ARTICLE_TAG, $check_tag_param);
                                            $get_tag = array_shift($get_tag);
                                            $tag_id = $get_tag['tag_id'];
                                        }

                                        $input_tags[] = $tag_id;
                                    }

                                    $merge_tag = array_merge($input_tags, $current_tag_id);
                                    $merge_tag = array_unique($merge_tag);
                                    $rel_tag_param['article_id'] = $id;
                                    foreach($merge_tag as $index) {
                                        $rel_tag_param['tag_id'] = $index;
                                        if( in_array($index, $input_tags) && !in_array($index, $current_tag_id) ) {
                                            $this->article->setNewArticle(RELATED_TAG, $rel_tag_param);
                                        } elseif( !in_array($index, $input_tags) && in_array($index, $current_tag_id) ) {
                                            $this->article->deleteArticle(RELATED_TAG, $rel_tag_param);
                                        }
                                    }

                                    // optional for image
                                    $msg = '';
                                    if( isset($image_name) ) {
                                        $msg = 'with image';
                                    }
                                    // set notification
                                    $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                Article has been updated '.$msg.
                                              '</div>';
                                    $this->session->set_flashdata('notif', $notif);

                                    // redirect to list general info page
                                    redirect('panel/article/all/'.$gid);
                                }
                            }
                            break;
                        case 'category':
                        case 'tag':
                            $gid = $detail['gid'];
                            $name = $detail[$type.'_name'];
                            if( $type == 'tag' ) {
                                $name = strtolower($name);
                            }

                            // update parameter
                            $update_param = array(
                                'gid' => $gid,
                                'name' => $name
                            );
                            if($type == 'category') {
                                $update_param['status'] = $this->article_status;
                            }
                            $update = $this->article->updateArticle($table, $update_param, $id);
                            if( $update == 'SUCCESSFUL' ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            '.ucfirst($type).' with name <b>'.$detail['gid_'.$type].'</b> has been updated.'.
                                          '</div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to list general info page
                                redirect('panel/article/'.$type.'/'.$gid);
                            }
                            break;
                    }
                } # end of validation
            } # end of post

            $data['data'] = $detail;
            
            if( $type != 'tag' ) {
                // status
                $data['status'] = $this->article_status;
            }
        }

        return $data;
    }

    // delete article data, include of image
    public function delete($type='all', $id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        $table = ARTICLE;
        if( $type != 'all' ) {
            $table = ARTICLE.'_'.$type;
        }

        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting '.$type.'. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && $id != 0 || is_numeric($id) ) {
            // check if given ID is existing in database
            $detail = $this->article->getArticleById($table, $id);
            // if data is found
            if( !empty($detail) ) {
                switch($type) {
                    case 'all':
                        $content = $detail['content'];
                        // delete master photos
                        $exp_date = explode(' ', $detail['date_add']);
                        $date_directory = $exp_date[0];
                        $img_path = $this->_create_directory($date_directory, $id);

                        $glob = glob($img_path.'*');
                        if( !empty($glob) ) {
                            foreach($glob as $k => $item) {
                                unlink($item);          // remove images
                            }
                            rmdir($img_path);          // remove directory
                        }
                        $name = 'title <b>'.$detail['title'].'</b>';
                        break;
                    case 'category':
                    case 'tag':
                        $table_relationship = ARTICLE.'_by_'.$type;     // delete from relationship table first
                        $delete_param = array(
                            $type.'_id' => $id
                        );
                        $count = $this->article->countArticle($table_relationship, array('where'=>$delete_param));
                        if( $count > 0 ) {
                            $this->article->deleteArticle($table_relationship, $delete_param);
                        }
                        $name = 'name <b>'.$detail[$type.'_name'].'</b>';
                        break;
                }

                // delete master table
                $delete = $this->article->deleteArticle($table, $id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    // provide success message
                    $msg = ucfirst($type).' with '.$name.' has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/article/'.$type);
    }
    
    // check datetime format
    public function checkDateFormat($datetime) {
        $data = explode(' ', $datetime);
//        $data = explode('%20', $datetime);
        $match_date = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data[0]);
        $match_time = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])/", $data[1]);
        if( $match_date && $match_time ) {
            return true;
//            echo 'true';
        } else {
            return false;
//            echo 'false';
        }
    }
}
