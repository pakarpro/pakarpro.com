<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Article_model extends CI_model {
    
    public $data;
    public $return;
    private $article;
    private $article_category;
    private $article_like;
    private $article_tag;
    private $article_view;
    private $related_category;
    private $related_tag;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
        $this->return = array();
        $this->article = array(
            'id AS article_id',
            'gid AS gid_article',
            'site',
            'date_add',
            'date_update',
            'date_start',
            'date_end',
            'title',
            'annotation',
            'content',
            'image_name',
            'status AS article_status',
            'count_likes',
            'count_views'
        );
        $this->article_category = array(
            'id AS category_id',
            'gid AS gid_category',
            'name AS category_name',
            'status AS category_status',
        );
        $this->article_like = array(
            'id AS like_id',
            'date_like',
            'article_id',
            'client_ip',
            'region_code',
            'region_name',
            'country_code',
            'country_name'
        );
        $this->article_tag = array(
            'id AS tag_id',
            'gid AS gid_tag',
            'name AS tag_name'
        );
        $this->article_view = array(
            'id AS view_id',
            'date_view',
            'article_id',
            'client_ip',
            'region_code',
            'region_name',
            'country_code',
            'country_name'
        );
        $this->related_category = array(
            'id AS rel_category_id',
            'article_id',
            'category_id'
        );
        $this->related_tag = array(
            'id AS rel_tag_id',
            'article_id',
            'tag_id'
        );
    }
    
#=================================== CREATE ===================================#
    public function setNewArticle($table, $data) {
        $params['table'] = $table;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getArticle($table, $params=array()) {
        // fields
        switch($table) {
            case ARTICLE: $fields = $this->article; break;
            case ARTICLE_TAG: $fields = $this->article_tag; break;
            case ARTICLE_CATEGORY: $fields = $this->article_category; break;
            case RELATED_TAG: $fields = $this->related_tag; break;
            case RELATED_CATEGORY: $fields = $this->related_category; break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getArticleById($table, $id) {
        // fields
        switch($table) {
            case ARTICLE: $fields = $this->article; break;
            case ARTICLE_TAG: $fields = $this->article_tag; break;
            case ARTICLE_CATEGORY: $fields = $this->article_category; break;
            case RELATED_TAG: $fields = $this->related_tag; break;
            case RELATED_CATEGORY: $fields = $this->related_category; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getArticleByGid($table, $gid) {
        // fields
        switch($table) {
            case ARTICLE: $fields = $this->article; break;
            case ARTICLE_CATEGORY: $fields = $this->article_category; break;
            case ARTICLE_TAG: $fields = $this->article_tag; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'gid' => $gid
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countArticle($table, $params=array()) {
        $params['table'] = $table;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateArticle($table, $data, $param) {
        $where = array('id' => $param);
        if(is_array($param)) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteArticle($table, $param) {
        $where = array('id' => $param);
        if(is_array($param)) {
            $where = $param;
        }
        $params['table'] = $table;
//        $params['where'] = array(
//            'id' => $id
//        );
        $params['where'] = $where;
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}