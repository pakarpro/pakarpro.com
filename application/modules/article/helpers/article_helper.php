<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('count_article')){
    function count_article($type='all'){
        $CI = &get_instance();
        $CI->load->model('article/Article_model');
        
        $param = array();
        switch($type) {
            case 'active':
                $param['where'] = array(
                    'status' => 1,
                    "(NOW() >= date_start OR date_start = '1970-01-01 00:00:00')" => NULL,
                    "(NOW() <= date_end OR date_end = '1970-01-01 00:00:00')" => NULL
                );
                break;
        }
        
        $article = new Article_model();
        $data = $article->countArticle(ARTICLE, $param);

        return $data;
    }
}
