<?php echo call_header('panel', 'Add New Article'); ?>

<?php echo call_sidebar('article', 'all'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-12 col-xs-12">
                                <form action="<?php echo site_url('article/add/all'); ?>" method="post" enctype="multipart/form-data" novalidate>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <div class="switch">
                                            <?php
                                                $checked = '';
                                                if( set_value('status') == 1 ) {
                                                    $checked = 'checked';
                                                }
                                            ?>
                                            <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Category</label>
                                        <?php echo form_error('category'); ?>
                                        <div class="checkbox">
                                            <?php
                                                foreach($category as $k => $item) {
                                                    $check = '';
                                                    if( !empty(set_value('category')) && in_array($item['category_id'], set_value('category')) ) {
//                                                        if(in_array($item['category_id'], set_value('category'))) {
                                                        $check = 'checked';
                                                    }
                                            ?>
                                                <input type="checkbox" class="filled-in chk-col-red category" name="category[]" id="<?php echo $item['category_id']; ?>" value="<?php echo $item['category_id']; ?>" <?php echo $check; ?>>
                                                <label for="<?php echo $item['category_id']; ?>"><?php echo $item['category_name']; ?></label>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Tags</label>
                                        <?php echo form_error('tag'); ?>
                                        <div class="form-line">
                                            <input name="tag" type="text" class="form-control" data-role="tagsinput" value="<?php echo set_value('tag'); ?>" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Title</label>
                                        <?php echo form_error('title'); ?>
                                        <div class="form-line">
                                            <input name="title" id="title" type="text" class="form-control" value="<?php echo set_value('title'); ?>" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Keyword (for SEO)</label>
                                        <div class="form-line">
                                            <?php
                                                $checked_gid = '';
                                                $required_gid = '';
                                                $disabled_gid = 'disabled';
                                                $gid = '';
                                                $gid_custom = '';
                                                $gid_default = set_value('gid_article');
                                                $gid_custom_error = '';
                                                if(!empty(set_value('use_custom_gid'))) {
                                                    $checked_gid = 'checked';
                                                    $required_gid = 'required';
                                                    $disabled_gid = '';
                                                    $gid = $data['gid'];
                                                    $gid_custom = set_value('gid_custom');
                                                    $gid_default = set_value('gid_default');
                                                    $gid_custom_error = form_error('gid_custom');
                                                }
                                            ?>
                                            <input name="gid" type="hidden" class="form-control" value="<?php echo $gid; ?>">
                                            <input name="gid_article" type="hidden" class="form-control" value="<?php echo set_value('gid_article'); ?>">
                                            <input name="gid_default" id="gid_default" type="hidden" class="gid form-control" value="<?php echo $gid_default; ?>">
                                            <p class="gid"><?php echo $gid_default; ?></p>
                                            <div class="clearfix">
                                                <input name="use_custom_gid" type="checkbox" id="use_custom_gid" class="filled-in chk-col-red" <?php echo $checked_gid; ?>>
                                                <label for="use_custom_gid">USE CUSTOM</label>
                                                <?php echo $gid_custom_error; ?>
                                                <input name="gid_custom" id="gid_custom" type="text" class="form-control" value="<?php echo $gid_custom; ?>" <?php echo $disabled_gid.' '.$required_gid; ?>>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Content</label>
                                        <?php echo form_error('content'); ?>
                                        <div class="form-line">
                                            <textarea id="content" name="content" class="form-control" required="required"><?php echo set_value('content'); ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <label>Date Start</label>
                                            <?php echo form_error('date_start'); ?>
                                            <div class="form-line">
                                                <input type="text" id="date_start" name="date_start" class="form-control" placeholder="Date start for scheduling" value="<?php echo set_value('date_start'); ?>" required="required">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>Date End</label>
                                            <?php echo form_error('date_end'); ?>
                                            <div class="form-line">
                                                <input type="text" id="date_end" name="date_end" class="form-control" placeholder="Date end of scheduling" value="<?php echo set_value('date_end'); ?>" required="required">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Image</label>
                                        <?php if(isset($error_image)) echo $error_image; ?>
                                        <div class="form-line">
                                            <input type="file" name="image_name" id="imageInput" class="form-control">
                                        </div>
                                        <small class="rules">File size should be equal or less than 2 MB, with minimum size 750 x 400 px</small>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <a href="<?php echo site_url('panel/article/all'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right" style="margin-right: 10px;" value="SAVE">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<!-- Bootstrap Tagsinput Css -->
<link href="<?php echo base_url(PLG_PATH.'bootstrap-tagsinput/bootstrap-tagsinput.css'); ?>" rel="stylesheet">
<!-- Bootstrap Tags Input Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'bootstrap-tagsinput/bootstrap-tagsinput.js'); ?>"></script>
<!-- Bootstrap Material Datetime Picker Css -->
<link href="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet">
<!-- Autosize Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'autosize/autosize.js'); ?>"></script>
<!-- Moment Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'momentjs/moment.js'); ?>"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js'); ?>"></script>
<!-- TinyMCE -->
<script src="<?php echo base_url(PLG_PATH.'tinymce/tinymce.js'); ?>"></script>
<script>
    // for editor, use tinymce for better
    tinymce.init({
        selector: "textarea#content",
        theme: "modern",
        height: 480,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu jbimages'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinymce.relative_urls = false;
    
    // gid
    $(document).on('change', '#title', function() {
        generate_gid('title');
    }).on('keyup', '#title', function() {
        generate_gid('title');
    }).on('blur', '#title', function() {
        generate_gid('title');
    });
    
    $(document).on('click', '#use_custom_gid', function() {
        if($(this).is(':checked')) {
            var default_gid = $('#gid_default').val();
            var gid = $('#gid_custom').val();
            if( gid == '' ) {
                gid = default_gid;
            }
            $('#gid_custom').removeAttr('disabled').focus().val(gid);
        } else {
            $('#gid_custom').attr('disabled', 'disabled');
        }
    });
    
    function generate_gid(element) {
        var title = $('#'+element).val();
        
        title = title.replace(/\s/g, "-").toLowerCase();
        $('.gid').val(title).text(title);
    }
    
    //Datetimepicker plugin
    $('#date_end').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD HH:mm:59',
        clearButton: true,
        weekStart: 0
    });
    $('#date_start').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD HH:mm:00',
        minDate: new Date(),
        clearButton: true,
        weekStart: 0
    }).on('change', function(e, date) {
        $('#date_end').bootstrapMaterialDatePicker('setMinDate', date);
    });
</script>
