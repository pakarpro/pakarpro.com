<?php echo call_header('panel', 'Tag of Article'); ?>

<?php echo call_sidebar('article', 'tag'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php echo $this->session->flashdata('notif'); ?>
                        
                        <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/article/add/tag'); ?>">ADD NEW TAG</a>
                        <table class="table table-hover table-list">
                            <thead>
                                <tr>
                                    <th width="30">#</th>
                                    <th width="200">Keyword</th>
                                    <th>Item</th>
                                    <th width="30"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( !empty($tag) ) {
                                        foreach($tag as $k => $item) {
                                ?>
                                    <tr data-target="<?php echo site_url('panel/article/tag/'.$item['gid_tag']); ?>" data-id="<?php echo $item['gid_tag']; ?>">
                                        <td class="_l"><?php echo $k+1; ?></td>
                                        <td class="_l"><?php echo $item['gid_tag']; ?></td>
                                        <td class="_l"><?php echo $item['tag_name']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('panel/article/tag/delete/'.$item['tag_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this article ?');">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="4">Data not found</td></tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <?php if( isset($pagination) ) { ?>
                        <!-- pagination -->
                        <div class="row clearfix">
                            <div class="col-xs-12 text-center">
                                <?php print_r($pagination); ?>
                            </div>
                        </div>
                        <!-- /pagination -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>

<script>
    // updating status of selected data
    $(document).on('click', '._cs', function() {
        var status;
        var id = $(this).attr('data-id');

        if( $(this).is(':checked') ) {
            status = 1;
        } else {
            status = 0;
        }

        var data = {
            "id": id,
            "status": status
        }
        
        $.post('changestatus/tag', data, function(response) {
            console.log(response);
            // do something here, for custom
        });
    });
</script>