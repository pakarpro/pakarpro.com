<?php echo call_header('panel', 'Add New Tag'); ?>

<?php echo call_sidebar('article', 'tag'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-6 col-xs-12">
                                <form action="<?php echo site_url('article/add/tag'); ?>" method="post" novalidate>
                                    <div class="form-group">
                                        <label>Tag name</label>
                                        <?php echo form_error('tag_name'); ?>
                                        <div class="form-line">
                                            <input name="tag_name" id="tag_name" type="text" class="form-control" value="<?php echo set_value('tag_name'); ?>" autofocus required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <div class="form-line">
                                            <?php
                                                $checked_gid = '';
                                                $required_gid = '';
                                                $disabled_gid = 'disabled';
                                                $gid = '';
                                                $gid_custom = '';
                                                $gid_default = set_value('gid_article');
                                                $gid_custom_error = '';
                                                if(!empty(set_value('use_custom_gid'))) {
                                                    $checked_gid = 'checked';
                                                    $required_gid = 'required';
                                                    $disabled_gid = '';
                                                    $gid = $data['gid'];
                                                    $gid_custom = set_value('gid_custom');
                                                    $gid_default = set_value('gid_default');
                                                    $gid_custom_error = form_error('gid_custom');
                                                }
                                            ?>
                                            <input name="gid" type="hidden" class="form-control" value="<?php echo $gid; ?>">
                                            <input name="gid_article" type="hidden" class="form-control" value="<?php echo set_value('gid_article'); ?>">
                                            <input name="gid_default" id="gid_default" type="hidden" class="gid form-control" value="<?php echo $gid_default; ?>">
                                            <p class="gid"><?php echo $gid_default; ?></p>
                                            <div class="clearfix">
                                                <input name="use_custom_gid" type="checkbox" id="use_custom_gid" class="filled-in chk-col-red" <?php echo $checked_gid; ?>>
                                                <label for="use_custom_gid">USE CUSTOM</label>
                                                <?php echo $gid_custom_error; ?>
                                                <input name="gid_custom" id="gid_custom" type="text" class="form-control" value="<?php echo $gid_custom; ?>" <?php echo $disabled_gid.' '.$required_gid; ?>>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">                                        
                                        <a href="<?php echo site_url('panel/article/tag'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right" style="margin-right: 10px;" value="SAVE">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<script>
    // gid
    $(document).on('change', '#tag_name', function() {
        generate_gid('tag_name');
    }).on('keyup', '#tag_name', function() {
        generate_gid('tag_name');
    }).on('blur', '#tag_name', function() {
        generate_gid('tag_name');
    });
    
    $(document).on('click', '#use_custom_gid', function() {
        if($(this).is(':checked')) {
            var default_gid = $('#gid_default').val();
            var gid = $('#gid_custom').val();
            if( gid == '' ) {
                gid = default_gid;
            }
            $('#gid_custom').removeAttr('disabled').focus().val(gid);
        } else {
            $('#gid_custom').attr('disabled', 'disabled');
        }
    });
    
    function generate_gid(element) {
        var tag_name = $('#'+element).val();
        
        tag_name = tag_name.replace(/\s/g, "-").toLowerCase();
        $('.gid').val(tag_name).text(tag_name);
    }
</script>
