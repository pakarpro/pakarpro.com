<?php echo call_header('panel', 'Detail Category'); ?>

<?php echo call_sidebar('article', 'category'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <div class="form-group">
                                    <?php
                                        $back_url = site_url('panel/article/article');
                                        if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                            $back_url = $_SERVER['HTTP_REFERER'];
                                        }
                                    ?>
                                    <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect">BACK</a>
                                </div>

                                <?php
                                    if( isset($category['data']) ) {
                                        $data = $category['data'];
                                ?>
                                    <form action="<?php echo site_url('panel/article/category/'.$data['gid_category']); ?>" method="post">
                                        <?php if( $data['category_id'] != 1 ) { ?>
                                            <div class="form-group">
                                                <label>Status</label>
                                                <div class="switch">
                                                    <?php
                                                        $checked = '';
                                                        if( $category['status'] == 1 ) {
                                                            $checked = 'checked';
                                                        }
                                                    ?>
                                                    <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                                </div>
                                            </div>
                                        <?php } ?>

                                        <div class="form-group">
                                            <label>Category Name</label>
                                            <?php echo form_error('category_name'); ?>
                                            <div class="form-line">
                                                <input name="category_name" id="category_name" type="text" class="form-control" value="<?php echo $data['category_name']; ?>" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Keyword</label>
                                            <div class="form-line">
                                                <?php
                                                    $checked_gid = '';
                                                    $required_gid = '';
                                                    $disabled_gid = 'disabled';
                                                    $gid = $data['gid_category'];
                                                    $gid_custom = '';
                                                    $gid_default = $data['gid_category'];
                                                    $gid_custom_error = '';
                                                    if(isset($data['use_custom_gid'])) {
                                                        $checked_gid = 'checked';
                                                        $required_gid = 'required';
                                                        $disabled_gid = '';
                                                        $gid = $data['gid'];
                                                        $gid_custom = $data['gid_custom'];
                                                        $gid_default = $data['gid_default'];
                                                        $gid_custom_error = form_error('gid_custom');
                                                    }
                                                ?>
                                                <input name="gid" type="hidden" class="form-control" value="<?php echo $gid; ?>">
                                                <input name="gid_category" type="hidden" class="form-control" value="<?php echo $data['gid_category']; ?>">
                                                <input name="gid_default" id="gid_default" type="hidden" class="gid form-control" value="<?php echo $gid_default; ?>">
                                                <p class="gid"><?php echo $gid_default; ?></p>
                                                <div class="clearfix">
                                                    <input name="use_custom_gid" type="checkbox" id="use_custom_gid" class="filled-in chk-col-red" <?php echo $checked_gid; ?>>
                                                    <label for="use_custom_gid">USE CUSTOM</label>
                                                    <?php echo $gid_custom_error; ?>
                                                    <input name="gid_custom" id="gid_custom" type="text" class="form-control" value="<?php echo $gid_custom; ?>" <?php echo $disabled_gid.' '.$required_gid; ?>>
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 pull-right">UPDATE</button>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<script>
    // gid
    $(document).on('change', '#category_name', function() {
        generate_gid('category_name');
    }).on('keyup', '#category_name', function() {
        generate_gid('category_name');
    }).on('blur', '#category_name', function() {
        generate_gid('category_name');
    });
    
    $(document).on('click', '#use_custom_gid', function() {
        if($(this).is(':checked')) {
            var default_gid = $('#gid_default').val();
            var gid = $('#gid_custom').val();
            if( gid == '' ) {
                gid = default_gid;
            }
            $('#gid_custom').removeAttr('disabled').focus().val(gid);
        } else {
            $('#gid_custom').attr('disabled', 'disabled');
        }
    });
    
    function generate_gid(element) {
        var category_name = $('#'+element).val();
        
        category_name = category_name.replace(/\s/g, "-").toLowerCase();
        $('.gid').val(category_name).text(category_name);
    }
</script>
