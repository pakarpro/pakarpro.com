<?php echo call_header('main', 'article'); ?>

    <section id="blog" class="container">
        <div class="center">
            <h2>Artikel</h2>
        </div>

        <div class="blog">
            <div class="row">
                <div class="col-md-8">
                    <?php
                        if( isset($article) && !empty($article) ) {
                            foreach($article as $k => $item) {
                                // detail url
                                $detail_url = site_url('article/post/'.$item['gid_article']);
                                // image
                                $date = explode(' ', $item['date_add']);
                                $date = str_replace('-','/',$date[0]).'/'.$item['article_id'].'/';
                                $img_url = base_url(IMG_PATH.'article/'.$date.'medium-'.$item['image_name']);
                        ?>
                            <div class="blog-item">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-2 text-center">
                                        <div class="entry-meta">
                                            <span id="publish_date"><?php echo format_datetime($item['date_start'], 'd', true); ?></span>
                                            <?php
                                                $liked = '';
                                                if( $item['already_like'] > 0 ) {
                                                    $liked = 'liked';
                                                }
                                                $viewed = '';
                                                if( $item['already_view'] > 0 ) {
                                                    $viewed = 'viewed';
                                                }
                                            ?>
                                            <span><i class="fa fa-eye<?php echo ' '.$viewed; ?>"></i> <a><?php echo $item['count_views']; ?> Views</a></span>
                                            <span class="_lk" data-id="<?php echo $item['article_id']; ?>">
                                                <i class="fa fa-heart<?php echo ' '.$liked; ?>"></i>
                                                <a><span id="total_<?php echo $item['article_id']; ?>"><?php echo $item['count_likes']; ?></span> Likes </a>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-10 blog-content">
                                        <a href="<?php echo $detail_url; ?>">
                                            <img class="img-responsive img-blog img-thumbnail" src="<?php echo $img_url; ?>" width="100%" alt="<?php echo $item['title']; ?>">
                                        </a>
                                        <h2><a href="<?php echo $detail_url; ?>"><?php echo $item['title']; ?></a></h2>
                                        <h3><?php echo $item['annotation']; ?>...</h3>
                                        <a class="btn btn-primary readmore" href="<?php echo $detail_url; ?>">
                                            Read More <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </div>    
                            </div><!--/.blog-item-->
                    <?php } } else { ?>
                        <h2>
                            Belum ada artikel dikategori ini.
                            Kembali ke <a href="<?php echo site_url(); ?>">halaman utama</a>.
                        </h2>
                    <?php } ?>

                    <?php
                        if( isset($pagination) ) {
                            print_r($pagination);
                        }
                    ?>
                </div><!--/.col-md-8-->

                <aside class="col-md-4">
                    <div class="widget search">
                        <form role="form" action="<?php echo site_url('article/search'); ?>" method="get">
                            <input type="text" name="q" class="form-control search_box" autocomplete="off" placeholder="Search Here">
                        </form>
                    </div><!--/.search-->

                    <div class="widget categories">
                        <h3>Most Viewed</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                    if( !empty($most_viewed) ) {
                                        foreach($most_viewed as $k => $item) {
                                            // detail url
                                            $url = site_url('article/post/'.$item['gid_article']);
                                ?>
                                    <div class="single_comments">
                                        <a href="<?php echo $url; ?>"><?php echo $item['title']; ?></a>
                                        <div class="entry-meta small muted">
                                            <span><i class="fa fa-eye"></i> <?php echo $item['count_views']; ?></span>
                                            <span><i class="fa fa-heart"></i> <?php echo $item['count_likes']; ?></span>
                                        </div>
                                    </div>
                                <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>                     
                    </div><!--/.most viewed-->
                    
                    <?php if( isset($category) ) { ?>
                        <div class="widget archieve">
                            <h3>Categories</h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="blog_archieve">
                                        <?php foreach($category as $k => $item) {  ?>
                                            <li>
                                                <a href="<?php echo site_url('article/category/'.$item['gid_category']); ?>">
                                                    <i class="fa fa-angle-double-right"></i> <?php echo $item['category_name']; ?> <span class="pull-right">(<?php echo $item['total'];?>)</span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>                     
                        </div>
                    <?php } ?>  

                    <?php if( isset($tag) ) { ?>
                        <div class="widget tags">
                            <h3>Related Tag</h3>
                            <ul class="tag-cloud">
                                <?php foreach($tag as $k => $item) { ?>
                                    <li><a class="btn btn-xs btn-primary" href="<?php echo site_url('article/tag/'.$item['gid_tag']); ?>"><?php echo $item['tag_name']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div><!--/.tags-->
                    <?php } ?>
                </aside>  
            </div><!--/.row-->
        </div><!--/.blog-->
    </section><!--/#blog-->

<?php echo call_footer('main'); ?>
<script>
    $(document).on('click', '._lk', function(e) {
        var id = $(this).attr('data-id');
        
        $.post('/article/set_article_count/like/', {'article_id':id}, function(res) {
            if( res == 'success' ) {
                var current_total = $('#total_'+id).text();
                var total = parseInt(current_total) + 1;
                $('#total_'+id).text(total);
                $('.fa.fa-heart').addClass('liked');
                alert('Anda menyukai artikel ini');
            } else {
                alert('Anda telah menyukai artikel ini');
            }
        });
    });
</script>