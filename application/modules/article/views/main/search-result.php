<?php echo call_header('main', 'article'); ?>

    <section id="blog" class="container">
        <div class="center">
            <h2>Artikel</h2>
        </div>

        <div class="blog blog-search">
            <div class="row">
                <div class="col-md-8">
                    <?php
                        if( is_array($query) ) {
                            $query = implode(' ', $query);
                        }
                    
                        if( isset($article) && !empty($article) ) {
                    ?>
                        <div class="blog-item">
                            <h2>Hasil pencarian untuk : "<?php echo $query; ?>"</h2>
                        </div>
                        <?php
                            foreach($article as $k => $item) {
                                // detail url
                                $detail_url = site_url('article/post/'.$item['gid_article']);
                                // title
//                                foreach($query as $key) {
//                                    if(strpos($item['title'], $key)) {
//                                        $item['title'] = str_replace($key, '<span style="text-decoration:underline">'.$key.'</span>', $item['title']);
//                                    }
//                                    if(strpos($item['title'], ucfirst($key))) {
//                                        $item['title'] = str_replace(ucfirst($key), '<span style="text-decoration:underline">'.ucfirst($key).'</span>', $item['title']);
//                                    }
//                                }
//                                if( preg_match('/('.implode('|', $query).'|'.ucfirst(implode('|', $query)).')/', $item['title']) ) {
//                                    $item['title'] = str
//                                } else {
//                                    echo 'kucing';
//                                }
                        ?>
                            <div class="blog-item">
                                <div class="row">
                                    <div class="col-xs-12 blog-content">
                                        <h2><a href="<?php echo $detail_url; ?>"><?php echo $item['title']; ?></a></h2>
                                        <h3>
                                            <?php echo $item['annotation']; ?>...
                                            <a class="readmore" style="text-decoration:underline;" href="<?php echo $detail_url; ?>">Read more</a>
                                        </h3>
                                        <p><?php echo format_datetime($item['date_start'], 'd', true)?></p>
                                    </div>
                                </div>
                            </div><!--/.blog-item-->
                    <?php } } else { ?>
                        <div class="blog-item">
                            <h2>Hasil untuk "<?php echo $query; ?>" tidak ditemukan.</h2>
                        </div>
                    <?php } ?>

                    <?php
                        if( isset($pagination) ) {
                            print_r($pagination);
                        }
                    ?>
                </div><!--/.col-md-8-->

                <aside class="col-md-4">
                    <div class="widget search">
                        <form role="form" action="<?php echo site_url('article/search'); ?>" method="get">
                            <input type="text" name="q" class="form-control search_box" autocomplete="off" placeholder="Search Here">
                        </form>
                    </div><!--/.search-->

                    <div class="widget categories">
                        <h3>Most Viewed</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                    if( !empty($most_viewed) ) {
                                        foreach($most_viewed as $k => $item) {
                                            // detail url
                                            $url = site_url('article/'.$item['gid_article']);
                                ?>
                                    <div class="single_comments">
                                        <a href="<?php echo $url; ?>"><?php echo $item['title']; ?></a>
                                        <div class="entry-meta small muted">
                                            <span><i class="fa fa-eye"></i> <?php echo $item['count_views']; ?></span>
                                            <span><i class="fa fa-heart"></i> <?php echo $item['count_likes']; ?></span>
                                        </div>
                                    </div>
                                <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>                     
                    </div><!--/.most viewed-->
                    
                    <?php if( isset($category) ) { ?>
                        <div class="widget archieve">
                            <h3>Categories</h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="blog_archieve">
                                        <?php foreach($category as $k => $item) {  ?>
                                            <li>
                                                <a href="<?php echo site_url('article/category/'.$item['gid_category']); ?>">
                                                    <i class="fa fa-angle-double-right"></i> <?php echo $item['category_name']; ?> <span class="pull-right">(<?php echo $item['total'];?>)</span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>                     
                        </div>
                    <?php } ?>  

                    <?php if( isset($tag) ) { ?>
                        <div class="widget tags">
                            <h3>Related Tag</h3>
                            <ul class="tag-cloud">
                                <?php foreach($tag as $k => $item) { ?>
                                    <li><a class="btn btn-xs btn-primary" href="<?php echo site_url('article/tag/'.$item['gid_tag']); ?>"><?php echo $item['tag_name']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div><!--/.tags-->
                    <?php } ?>
                </aside>  
            </div><!--/.row-->
        </div><!--/.blog-->
    </section><!--/#blog-->

<?php echo call_footer('main'); ?>
<script>
    $(document).on('click', '._lk', function(e) {
        var id = $(this).attr('data-id');
        
        $.post('/article/set_article_count/like/', {'article_id':id}, function(res) {
            if( res == 'success' ) {
                var current_total = $('#total_'+id).text();
                var total = parseInt(current_total) + 1;
                $('#total_'+id).text(total);
                $('.fa.fa-heart').addClass('liked');
                alert('Anda menyukai artikel ini');
            } else {
                alert('Anda telah menyukai artikel ini');
            }
        });
    });
</script>