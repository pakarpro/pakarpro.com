<?php echo call_header('main', 'article'); ?>

    <section id="blog" class="container">
        <div class="center">
            <!--<h2><?php //echo $article['title']; ?></h2>-->
            <h2>Artikel</h2>
            <!--<p class="lead">Pellentesque habitant morbi tristique senectus et netus et malesuada</p>-->
        </div>

        <div class="blog">
            <div class="row">
                <div class="col-md-8">
                    <?php
                        if( isset($article) ) {
                            // image
                            $date = explode(' ', $article['date_add']);
                            $date = str_replace('-','/',$date[0]).'/'.$article['article_id'].'/';
                            $img_url = base_url(IMG_PATH.'article/'.$date.'medium-'.$article['image_name']);
                    ?>
                        <div class="blog-item">
                            <img class="img-responsive img-blog img-thumbnail" src="<?php echo $img_url; ?>" width="100%" alt="<?php echo $article['title']; ?>">
                            <div class="row">  
                                <div class="col-xs-12 col-sm-2 text-center">
                                    <div class="entry-meta">
                                        <span id="publish_date"><?php echo format_datetime($article['date_start'], 'd', true); ?></span>
                                        <?php
                                            $liked = '';
                                            if( $article['already_like'] > 0 ) {
                                                $liked = 'liked';
                                            }
                                            $viewed = '';
                                            if( $article['already_view'] > 0 ) {
                                                $viewed = 'viewed';
                                            }
                                        ?>
                                        <span><i class="fa fa-eye<?php echo ' '.$viewed; ?>"></i> <a><?php echo $article['count_views']; ?> Views</a></span>
                                        <span class="_lk" data-id="<?php echo $article['article_id']; ?>">
                                            <i class="fa fa-heart<?php echo ' '.$liked; ?>"></i>
                                            <a><span id="total_<?php echo $article['article_id']; ?>"><?php echo $article['count_likes']; ?></span> Likes </a>
                                        </span>
                                        <?php /* ?>
                                        <span><i class="fa fa-user"></i> <a href="#"> John Doe</a></span>
                                        <span><i class="fa fa-comment"></i> <a href="blog-item.html#comments">2 Comments</a></span>
                                        <?php */ ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-10 blog-content">
                                    <h2><?php echo $article['title']; ?></h2>
                                    <div style="text-align: justify !important;"><?php echo $article['content']; ?></div>
                                </div>
                            </div>
                            
                            <?php if( isset($pager) ) { ?>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <ul class="pager">
                                            <?php
                                                foreach($pager as $k => $item) {
                                                    switch($k) {
                                                        case 'previous';
                                                            $text = '<i class="fa fa-long-arrow-left"></i> Previous';
                                                            break;
                                                        case 'next';
                                                            $text = 'Next <i class="fa fa-long-arrow-right"></i>';
                                                            break;
                                                    }
                                                    $url = site_url('article/post/'.$item['gid_article']);
                                            ?>
                                                <li class="<?php echo $k; ?>">
                                                    <a href="<?php echo $url; ?>" title="<?php echo $item['title']; ?>">
                                                        <?php echo $text; ?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </div><!--/.blog-item-->
                    <?php } else { ?>
                        <h2>
                            Artikel tidak ditemukan.
                            Kembali ke <a href="<?php echo site_url(); ?>">halaman utama</a>.
                        </h2>
                    <?php } ?>
                </div>

                <aside class="col-md-4">
                    <div class="widget search">
                        <form role="form" action="<?php echo site_url('article/search'); ?>" method="get">
                            <input type="text" name="q" class="form-control search_box" autocomplete="off" placeholder="Search Here">
                        </form>
                    </div>

                    <div class="widget categories">
                        <h3>Most Viewed</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                    if( !empty($most_viewed) ) {
                                        foreach($most_viewed as $k => $item) {
                                            // detail url
                                            $url = site_url('article/post/'.$item['gid_article']);
                                ?>
                                    <div class="single_comments">
                                        <a href="<?php echo $url; ?>"><?php echo $item['title']; ?></a>
                                        <div class="entry-meta small muted">
                                            <span><i class="fa fa-eye"></i> <?php echo $item['count_views']; ?></span>
                                            <span><i class="fa fa-heart"></i> <?php echo $item['count_likes']; ?></span>
                                        </div>
                                    </div>
                                <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>                     
                    </div><!--/.most viewed-->

                    <?php if( isset($category) ) { ?>
                        <div class="widget archieve">
                            <h3>Categories</h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <ul class="blog_archieve">
                                        <?php foreach($category as $k => $item) { ?>
                                            <li>
                                                <a href="<?php echo site_url('article/category/'.$item['gid_category']); ?>">
                                                    <i class="fa fa-angle-double-right"></i> <?php echo $item['category_name']; ?> <span class="pull-right">(<?php echo $item['total'];?>)</span>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>                     
                        </div>
                    <?php } ?>  

                    <?php if( isset($tag) ) { ?>
                        <div class="widget tags">
                            <h3>Related Tag</h3>
                            <ul class="tag-cloud">
                                <?php foreach($tag as $k => $item) { ?>
                                    <li><a class="btn btn-xs btn-primary" href="<?php echo site_url('article/tag/'.$item['gid_tag']); ?>"><?php echo $item['tag_name']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div><!--/.tags-->
                    <?php } ?>
                </aside>
            </div><!--/.row-->
         </div>
    </section>

<?php echo call_footer('main'); ?>
<script>
    $(document).on('click', '._lk', function(e) {
        var id = $(this).attr('data-id');
        
        $.post('/article/set_article_count/like/', {'article_id':id}, function(res) {
            if( res == 'success' ) {
                var current_total = $('#total_'+id).text();
                var total = parseInt(current_total) + 1;
                $('#total_'+id).text(total);
                $('.fa.fa-heart').addClass('liked');
                alert('Anda menyukai artikel ini');
            } else {
                alert('Anda telah menyukai artikel ini');
            }
        });
    });
</script>