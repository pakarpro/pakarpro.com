<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('list_area')){
    function list_area($data=''){
        $CI = &get_instance();
        $CI->load->model('area/Area_model');
        
        if( !empty($data) ) {
            $area_list = $CI->Area_model->list_area($data);
        } else {
            $area_list = $CI->Area_model->list_area();
        }
        
        return $area_list;
    }
}
if(!function_exists('count_list_area')){
    function count_list_area(){
        $CI = &get_instance();
        $CI->load->model('area/Area_model');
        
        $count_area_list = $CI->Area_model->count_area();
        return $count_area_list;
    }
}

if(!function_exists('get_area')){
    function get_area($type, $param=''){
        $CI = &get_instance();
        $CI->load->model('area/Area_model');
        $area = new Area_model();
        $data = $area->getArea($type, $param);
        
        return $data;
    }
}

if(!function_exists('get_area_by_id')){
    function get_area_by_id($type, $id){
        $CI = &get_instance();

        $param['where'] = array(
            'id' => $id
        );
        $CI->load->model('area/Area_model');
        $area = new Area_model();
        $data = $area->getArea($type, $param);
        if( !empty($data) ) {
            $data = array_shift($data);
            if( $type == 'city' && strpos($data['city_name'], ', Kota') ) {
                $data['city_name'] = str_replace(', Kota', '', $data['city_name']);
            }
        }
        
        return $data;
    }
}


if(!function_exists('get_area_by_gid')){
    function get_area_by_gid($type, $gid){
        $CI = &get_instance();

        $param['where'] = array(
            'gid_'.$type => $gid
        );
        $CI->load->model('area/Area_model');
        $area = new Area_model();
        $data = $area->getArea($type, $param);
        if( !empty($data) ) {
            $data = array_shift($data);
        }
        
        return $data;
    }
}

