<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

define('CITY', 'area_city');
define('PROVINCE', 'area_province');
define('DISTRICT', 'area_district');

class Area_model extends CI_model {
    
    private $data;
    private $return;
    private $province;
    private $city;
    private $district;
    
    public function __construct() {
        parent::__construct();
        
        $this->return = array();
        
        $this->province = array(
            'id AS province_id',
            'gid_province',
            'province_name',
            'capital'
        );

        $this->city = array(
            'id AS city_id',
            'gid_city',
            'province_id',
            'city_name',
            'phone_code'
        );

        $this->district = array(
            'id AS district_id',
            'gid_district',
            'city_id',
            'district_name',
        );

        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function saveArea($data) {
        $params['table'] = OFFICE;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function countArea() {
        $params['table'] = OFFICE;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getArea($area, $params=array()) {
        switch($area) {
            case 'province':
                $table = PROVINCE;
                $fields = $this->province;
                break;
            case 'city':
                $table = CITY;
                $fields = $this->city;
                break;
            case 'district':
                $table = DISTRICT;
                $fields = $this->district;
                break;
        }

        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = $table;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateArea($data, $where) {
        $params['table'] = OFFICE;
        $params['data'] = $data;
        $params['where'] = $where;

        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteArea($where) {
        $params['table'] = OFFICE;
        $params['where'] = $where;
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}