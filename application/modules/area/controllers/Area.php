<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends CI_Controller {
    
    public $CI;
    public $redirect = 'dashboard';
    public $user_data = array();

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->load->helper('general');
        
        if( $this->session->userdata('logged_in') ) {
            $this->user_data = $this->session->userdata('logged_in');
        }
    }
    
    public function ajax_get_city() {
        $return['data'] = array();
        $prov_id = $this->input->post('id');
        
        if( !empty($prov_id) && $prov_id != 0 ) {
            $params = array(
                'where' => array(
                    'province_id' => $prov_id
                ),
                'order_by' => array(
                    'city_name' => 'ASC'
                )
            );
            $this->load->helper('area');
            $data = get_area('city', $params);
            foreach($data as $k => $item) {
                $item['city_name'] = str_replace(', Kots', '', $item['city_name']);
                $data[$k] = $item;
            }
            $return['data'] = $data;
        }
        
        echo json_encode($return['data']);
    }

    public function ajax_get_district() {
        $return['data'] = array();
        $city_id = $this->input->post('id');
        
        if( !empty($city_id) && $city_id != 0 ) {
            $params = array(
                'where' => array(
                    'city_id' => $city_id
                ),
                'order_by' => array(
                    'district_name' => 'ASC'
                )
            );
            $this->load->helper('area');
            $data = get_area('district', $params);
            $return['data'] = $data;
        }
        
        echo json_encode($return['data']);
    }
}