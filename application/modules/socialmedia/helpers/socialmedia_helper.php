<?php

/* 
 * Name             : get_active_sosmed_account
 * Parameters       : -
 * Functionality    : get active account of social media
 */
if( !function_exists('get_active_sosmed_account') ){
    function get_active_sosmed_account(){
        $CI = &get_instance();
        
        $CI->load->model('socialmedia/Socialmedia_model');
        $socialmedia = new Socialmedia_model();

        $param = array(
            'fields' => array(
                SOSMED_ACCOUNT.'.name AS account_name',
                SOSMED.'.gid',
                SOSMED.'.url',
                SOSMED.'.name AS sosmed_name'
            ),
            'join' => array(
                SOSMED => SOSMED.'.id = '.SOSMED_ACCOUNT.'.id_sosmed'
            ),
            'where' => array(
                SOSMED_ACCOUNT.'.status' => 1
            ),
            'order_by' => array(
                SOSMED.'.gid' => 'ASC'
            )
        );
        $data = $socialmedia->getSocialmedia(SOSMED_ACCOUNT, $param);
        foreach($data as $k => $item) {
            $url = $item['url'].'/'.$item['account_name'];
            $item['url'] = $url;
            
            $data[$k] = $item;
        }

        return $data;
    }
}

/* 
 * Name             : get_active_sosmed
 * Parameters       : -
 * Functionality    : get active of social media type
 */
if( !function_exists('get_active_sosmed') ){
    function get_active_sosmed(){
        $CI = &get_instance();
        
        $CI->load->model('socialmedia/Socialmedia_model');
        $socialmedia = new Socialmedia_model();

        $param = array(
            'fields' => array(
                'id AS sosmed_id',
                'name AS sosmed_name',
            ),
            'where' => array(
                'status' => 1
            ),
            'order_by' => array(
                'name' => 'ASC'
            )
        );
        $data = $socialmedia->getSocialmedia(SOSMED, $param);

        return $data;
    }
}

// to be displayed in homepage
if( !function_exists('get_active_sosmed') ){
    function get_active_sosmed(){
        $CI = &get_instance();

        $param = array(
            'fields' => array(
                'id AS sosmed_id',
                'name AS sosmed_name',
            ),
            'where' => array(
                'status' => 1
            ),
            'order_by' => array(
                'name' => 'ASC'
            )
        );
        $data = get_social_media(SOSMED, $param);

        return $data;
    }
}

/* 
 * Name             : count_social_media
 * Parameters       : -
 * Functionality    : count all social media data
 */
if(!function_exists('count_social_media')){
    function count_social_media() {
        $CI = &get_instance();
        
        $CI->load->model('social_media/Social_media_model');
        $count = new Social_media_model();
        $data = $count->countSocialMedia();
        
        return $data;
    }
}
