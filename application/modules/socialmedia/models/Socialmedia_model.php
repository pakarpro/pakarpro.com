<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Socialmedia_model extends CI_model {
    
    public $data;
    public $return;
    private $sosmed;
    private $sosmed_account;
    private $sosmed_credentials = array(
        'credential_id',
        'client_id_key',
        'client_secret_key',
        'access_token'
    );
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
        $this->return = array();
        $this->sosmed = array(
            'id AS sosmed_id',
            'gid',
            'name AS sosmed_name',
            'url',
            'rules',
            'status AS sosmed_status'
        );
        $this->sosmed_account = array(
            'id AS account_id',
            'id_sosmed',
            'date_add',
            'date_update',
            'name AS account_name',
            'status AS account_status'
        );
    }
    
#=================================== CREATE ===================================#
    public function setNewSocialmedia($table, $data) {
        $params['table'] = $table;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getSocialmedia($table, $params=array()) {
        // fields
        switch($table) {
            case SOSMED: $fields = $this->sosmed; break;
            case SOSMED_ACCOUNT: $fields = $this->sosmed_account; break;
            case SOSMED_CREDENTIALS: $fields = $this->sosmed_credentials; break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getSocialmediaById($table, $id) {
        // fields
        switch($table) {
            case SOSMED: $fields = $this->sosmed; break;
            case SOSMED_ACCOUNT: $fields = $this->sosmed_account; break;
            case SOSMED_CREDENTIALS: $fields = $this->sosmed_credentials; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getSocialmediaByGid($table, $gid) {
        // fields
        switch($table) {
            case SOSMED: $fields = $this->sosmed; break;
            case SOSMED_ACCOUNT: $fields = $this->sosmed_account; break;
            case SOSMED_CREDENTIALS: $fields = $this->sosmed_credentials; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'gid' => $gid
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countSocialmedia($table, $params=array()) {
        $params['table'] = $table;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateSocialmedia($table, $data, $param) {
        $where = array('id' => $param);
        if(is_array($param) ) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteSocialmedia($table, $sosmedId) {
        $params['table'] = $table;
        $params['where'] = array(
            'id' => $sosmedId
        );
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}