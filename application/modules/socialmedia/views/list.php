<?php echo call_header('panel', 'Lists of Social Media Account'); ?>

<?php echo call_sidebar('socialmedia'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php echo $this->session->flashdata('notif'); ?>
                        
                        <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/socialmedia/add'); ?>">ADD NEW ACCOUNT</a>
                        <table class="table table-hover table-list">
                            <thead>
                                <tr>
                                    <th width="50">#</th>
                                    <th width="120">Sosmed Type</th>
                                    <th>Account Name</th>
                                    <th width="120"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( !empty($data) ) {
                                        foreach($data as $k => $item) {
                                            $checked = '';
                                            if( $item['account_status'] == 1 ) {
                                                $checked = 'checked';
                                            }
                                ?>
                                    <tr data-target="<?php echo site_url('panel/socialmedia/detail/'.$item['account_id']); ?>" data-id="<?php echo $item['account_id']; ?>">
                                        <td class="_l"><?php echo $k+1; ?></td>
                                        <td class="_l"><?php echo $item['sosmed_name']; ?></td>
                                        <td class="_l"><?php echo $item['account_name']; ?></td>
                                        <td>
                                            <div class="switch pull-left">
                                                <label><input type="checkbox" name="status" class="_cs" data-id="<?php echo $item['account_id']; ?>" <?php echo $checked; ?>><span class="lever"></span></label>
                                            </div>
                                            <a target="_blank" href="<?php echo $item['url'].'/'.$item['account_name']; ?>" class="waves-effect" title="<?php echo $item['account_name']; ?>">
                                                <i class="material-icons">launch</i>
                                            </a>
                                            <a href="<?php echo site_url('panel/socialmedia/delete/'.$item['account_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this account ?');">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="5">Data not found</td></tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <?php if( isset($pagination) ) { ?>
                        <!-- pagination -->
                        <div class="row clearfix">
                            <div class="col-xs-12 text-center">
                                <?php print_r($pagination); ?>
                            </div>
                        </div>
                        <!-- /pagination -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>

<script>
    updateStatus('socialmedia');
</script>