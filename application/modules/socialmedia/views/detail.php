<?php echo call_header('panel', 'Detail Account'); ?>

<?php echo call_sidebar('socialmedia'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <div class="form-group">
                                    <?php
                                        $back_url = site_url('panel/socialmedia');
                                        if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                            $back_url = $_SERVER['HTTP_REFERER'];
                                        }
                                    ?>
                                    <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect">BACK</a>
                                </div>

                                <?php if( isset($data) ) { ?>
                                    <form action="<?php echo site_url('panel/socialmedia/detail/'.$data['account_id']); ?>" method="post">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <div class="switch">
                                                <?php
                                                    $checked = '';
                                                    if( $status == 1 ) {
                                                        $checked = 'checked';
                                                    }
                                                ?>
                                                <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Social Media Type</label>
                                            <?php echo form_error('sosmed_type'); ?>
                                            <div class="form-line">
                                                <input type="hidden" name="regex_rules" id="regex_rules" value="<?php echo $data['regex_rules']; ?>">
                                                <select name="sosmed_type" class="form-control show-tick" id="sosmed_type" required="required">
                                                    <option value="">- Choose -</option>
                                                    <?php
                                                        foreach($sosmed_type as $k => $item) {
                                                            $selected = '';
                                                            if( $item['sosmed_id'] == $data['sosmed_type'] ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$item['sosmed_id'].'" '.$selected.'>'.$item['sosmed_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Account Name</label>
                                            <?php
                                                echo form_error('account_name');
                                                // additional error message
                                                if( isset($error_account_name) ) { echo $error_account_name; }
                                            ?>
                                            <div class="form-line">
                                                <input type="hidden" name="info_rules" id="info_rules" value="<?php echo $data['info_rules']; ?>">
                                                <input name="account_name" type="text" class="form-control" value="<?php echo $data['account_name']; ?>" autofocus required="required">
                                            </div>
                                            <p><small class="rules"><?php echo $data['info_rules']; ?></small></p>
                                        </div>
                                                                                
                                        <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 pull-right">UPDATE</button>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<script>
    var rules = [];
    var regex;
    var rule;
    
    $(document).on('change', '#sosmed_type', function() {
        var sosmed_id = $(this).val();

        clearRules();
        $.post('<?php echo site_url('socialmedia/get_sosmed_rules'); ?>/'+sosmed_id, function(data){
            data = JSON.parse(data);

            $('small.rules').text(data.info_rules);
            $('#info_rules').val(data.info_rules);
            $('#regex_rules').val(data.regex_rules);
            $('input[name="account_name"]').focus();
//            console.log(data);
        });
    });
    
    function clearRules() {
        rules = [];
    }
</script>