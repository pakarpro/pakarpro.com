<?php echo call_header('panel', 'Add New Account'); ?>

<?php echo call_sidebar('socialmedia'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-6 col-xs-12">
                                <form action="<?php echo site_url('socialmedia/add'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <?php
                                            $checked = '';
                                            if( $status == 1 ) {
                                                $checked = 'checked';
                                            }
                                        ?>
                                        <div class="switch">
                                            <label>INACTIVE<input type="checkbox" name="status" id="toggleStatus" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Social Media Type</label>
                                        <?php echo form_error('sosmed_type'); ?>
                                        <div class="form-line">
                                            <input type="hidden" name="regex_rules" id="regex_rules" value="<?php echo set_value('regex_rules'); ?>">
                                            <select name="sosmed_type" class="form-control show-tick" id="sosmed_type" required="required">
                                                <option value="">- Choose -</option>
                                                <?php
                                                    foreach($sosmed_type as $k => $item) {
                                                        $selected = '';
                                                        if( $item['sosmed_id'] == set_value('sosmed_type') ) {
                                                            $selected = 'selected';
                                                        }
                                                        echo '<option value="'.$item['sosmed_id'].'" '.$selected.'>'.$item['sosmed_name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Account Name</label>
                                        <?php
                                            echo form_error('account_name');
                                            // additional error message
                                            if( isset($error_account_name) ) { echo $error_account_name; }
                                        ?>
                                        <div class="form-line">
                                            <input type="hidden" name="info_rules" id="info_rules" value="<?php echo set_value('info_rules'); ?>">
                                            <input name="account_name" type="text" class="form-control" value="<?php echo set_value('account_name'); ?>" autofocus required="required">
                                        </div>
                                        <p><small class="rules"><?php echo set_value('info_rules'); ?></small></p>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <a href="<?php echo site_url('panel/socialmedia'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right m-r-10" value="SAVE">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<script>
    var rules = [];
    var regex;
    var rule;
    
    $(document).on('change', '#sosmed_type', function() {
        var sosmed_id = $(this).val();

        clearRules();
        $.post('<?php echo site_url('socialmedia/get_sosmed_rules'); ?>/'+sosmed_id, function(data){
            data = JSON.parse(data);

            $('small.rules').text(data.info_rules);
            $('#info_rules').val(data.info_rules);
            $('#regex_rules').val(data.regex_rules);
            $('input[name="account_name"]').focus();
//            console.log(data);
        });
    });
    
    function clearRules() {
        rules = [];
    }
</script>