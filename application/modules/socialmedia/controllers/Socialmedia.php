<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socialmedia extends CI_Controller {
    
    private $CI;
    private $access_token = '';
    private $account_status = 1;
    private $redirect = 'panel/login';
    private $img_path;
    private $temp_path;
    private $admin_data = array();
    private $validation_config = array(
        array(
            'field'   => 'sosmed_type',
            'label'   => 'Social media type',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'account_name',
            'label'   => 'Account name',
            'rules'   => 'trim|required'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'regex_match' => '%s does not follow approriate format'
    );
    private $instagram_credential = array(
        'client_id' => 'd86d7dc2ae0d4867912f63fbf7e466e5',
        'client_secret' => 'cec00df1da174168aa7522e1b478d4b4'
    );
    private $sosmed;

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('form');
        $this->load->model('Socialmedia_model');
        $this->sosmed = new Socialmedia_model();
        
        // image path
        $this->img_path = FCPATH.IMG_PATH.'activity/';
        $this->temp_path = FCPATH.IMG_PATH.'temp/activity/';

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
    
    private function _get_credential($type) {
        $additional_parameters['where'] = array(
            'source' => $type,
            'credential_status' => 1
        );
        $credential = $this->sosmed->getSocialmedia(SOSMED_CREDENTIALS, $additional_parameters);
        $credential = array_shift($credential);
        
        return $credential;
    }
    
    // for getting access token from instagram
    public function get_access_token($type='instagram') {
        $credential = $this->_get_credential($type);
        $credential_id = $credential['credential_id'];
        $client_id = $credential['client_id_key'];
        
        $redirect_url = strtolower(site_url(__CLASS__.'/'.__FUNCTION__));
        $authorization_url = 'https://api.instagram.com/oauth/authorize/?client_id='.$client_id.'&redirect_uri='.$redirect_url.'&response_type=code';
        $access_token_url = 'https://api.instagram.com/oauth/access_token';
        
        $code = $this->input->get('code');
        if( isset($code) && !empty($code) ) {
            $client_secret = $credential['client_secret_key'];
            $required_parameters = array(
                'client_id'     => $client_id,
                'client_secret' => $client_secret,
                'grant_type'    => 'authorization_code',
                'redirect_uri'  => $redirect_url,
                'code'          => $code
            );
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $access_token_url); // uri
            curl_setopt($ch, CURLOPT_POST, true); // POST
            curl_setopt($ch, CURLOPT_POSTFIELDS, $required_parameters); // POST DATA
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // RETURN RESULT true
            curl_setopt($ch, CURLOPT_HEADER, 0); // RETURN HEADER false
            curl_setopt($ch, CURLOPT_NOBODY, 0); // NO RETURN BODY false / we need the body to return
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); // VERIFY SSL HOST false
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // VERIFY SSL PEER false
            $result = json_decode(curl_exec($ch)); // execute curl
            
            // update credential by adding access token code
            if( isset($result->access_token) ) {
                $update_param = array(
                    'access_token' => $result->access_token
                );
                $where = array('credential_id' => $credential_id);
                $this->sosmed->updateSocialmedia(SOSMED_CREDENTIALS, $update_param, $where);
            }
        } else {
            redirect($authorization_url);
        }
    }
    
    public function get_media($type='instagram') {
        if( $type == 'instagram' ) {
            $credential = $this->_get_credential($type);
            $access_token = $credential['access_token'];
            $user_endpoints_url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token='.$access_token.'&count=20';

            $media = file_get_contents($user_endpoints_url);
            $media = json_decode($media, true, 512);
            $data = $media['data'];

            if( !empty($data) ) {
//                print_r($data); exit;
                $this->load->helper('uploads/uploads');
                foreach($data as $key => $item) {
                    $date_activity = gmdate('Y-m-d H:i:s', $item['created_time']);
                    $image_url = $item['images']['standard_resolution']['url'];
                    $image_name = pathinfo($image_url, PATHINFO_BASENAME);
                    
                    // check if activities already exist or no
                    $count_activity_param['where'] = array(
                        'date_activity' => $date_activity,
                        'activity_image' => $image_name
                    );
                    $count_activity = $this->sosmed->countSocialmedia(ACTIVITIES, $count_activity_param);
                    if( $count_activity == 0 ) {
                        $image_text = $item['caption']['text'];
                        if( !isset($image_text) ) {
                            $image_text = '';
                        }

                        $activity_parameter = array(
                            'date_add' => DATETIME,
                            'date_activity' => $date_activity,
                            'source' => 'instagram',
                            'activity_caption' => $image_text,
                            'activity_status' => 1
                        );
                        $save = $this->sosmed->setNewSocialmedia(ACTIVITIES, $activity_parameter);
                        if( isset($save['id']) ) {
                            $activity_id = $save['id'];
                            $temp_image = $this->temp_path.$image_name;

                            if( !file_exists($this->temp_path) ) {
                                mkdir($this->temp_path);
                                chmod($this->temp_path, 0777);
                            }
                            copy($image_url, $temp_image);

                            // create directory for image
                            $img_path = $this->img_path.$activity_id.'/';
                            if( !file_exists($img_path) ) {
                                mkdir($img_path);
                                chmod($img_path, 0777);
                            }

                            // get image config
                            $config = get_uploads_config('activity');
                            $thumb_where_param['where'] = array(
                                'config_id' => $config['config_id']
                            );
                            $thumb_config = get_thumb_config($thumb_where_param);
                            crop_image($config['min_width'], $config['min_height'], $temp_image, $img_path, $image_name);
                            if( !empty($thumb_config) ) {
                                foreach($thumb_config as $key => $val) {
                                    crop_image($val['width'], $val['height'], $temp_image, $img_path, $val['prefix'].$image_name);
                                }
                            }

                            // update image name in database
                            $update_param = array('activity_image' => $image_name);
                            $where_param = array('activity_id' => $activity_id);
                            $this->sosmed->updateSocialmedia(ACTIVITIES, $update_param, $where_param);

                            unlink($temp_image);
                        }
                    }
                }
            }
        } else {
            echo 'null';
        }
    }
    
    // check existing account based on sosmed type
//    private function _check_existing_account($param) {
//        $where['where'] = $param;
//        $count = $this->sosmed->countSocialmedia(SOSMED_ACCOUNT, $where);
//        echo $count; exit;
//    }
    
    // format account rules
    private function _format_account_rules($rules) {
        $rules = unserialize($rules);
        
        // regex
        $regex = implode("", $rules);
        $data['regex_rules'] = 'regex_match[/^['.$regex.']+$/]';
        
        // info rules
        foreach($rules as $rule) {
            switch($rule) {
                case "a-z": $rule = 'letters'; break;
                case "0-9": $rule = 'numbers'; break;
                case "_": $rule = 'underscore'; break;
                case ".": $rule = 'dot'; break;
            }

            $formatted_rules[] = ' '.$rule;
        }
        $data['info_rules'] = 'Must contain at least'.implode(',', $formatted_rules);
        
        return $data;
    }

    // get list of sosmed account
    private function _get_sosmed_account($param=array()) {
        $param['fields'] = array(
            SOSMED_ACCOUNT.'.id AS account_id',
            SOSMED_ACCOUNT.'.name AS account_name',
            SOSMED_ACCOUNT.'.status AS account_status',
            SOSMED.'.gid',
            SOSMED.'.url',
            SOSMED.'.name AS sosmed_name'
        );
        $param['join'] = array(
            SOSMED => SOSMED.'.id = '.SOSMED_ACCOUNT.'.id_sosmed'
        );
        $data = $this->sosmed->getSocialmedia(SOSMED_ACCOUNT, $param);
        
        return $data;
    }
    
    // updating status, for ajax use
    public function changestatus() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $status = $data['status'];
            
            $update_param = array(
                'date_update' => DATETIME,
                'status' => $status
            );
            $update = $this->sosmed->updateSocialmedia(SOSMED_ACCOUNT, $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    // get rules of sosmed account name
    public function get_sosmed_rules($sosmed_id) {
        $rules = array();
        if( !empty($sosmed_id) && is_numeric($sosmed_id) && $sosmed_id != 0 ) {
            $sosmed = $this->sosmed->getSocialmediaById(SOSMED, $sosmed_id);
            if( !empty($sosmed) ) {
                $rules = $sosmed['rules'];
                $rules = $this->_format_account_rules($rules);
            }
        }
        
        echo json_encode($rules);
    }
    
    public function index() {
        $this->lists();
    }
    
    // list all sosmed account
    public function lists($page=1) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        $count_sosmed_account = $this->sosmed->countSocialmedia(SOSMED_ACCOUNT);
        if( $count_sosmed_account > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            if( $count_sosmed_account > $limit_per_page ) {
                // additional where param
                $where_param['limit'] = array($limit_per_page, $start_row);
                
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/socialmedia'),
                    'total_rows'    =>  $count_sosmed_account,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            $where_param['order_by'] = array(
                SOSMED_ACCOUNT.'.id' => 'ASC'
            );
            $data['data'] = $this->_get_sosmed_account($where_param);
        }
        
        $this->CI->load->view('list', $data);
    }
    
    // save new socialmedia data
    public function add() {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;

        // get sosmed type
        $this->load->helper('socialmedia');
        $data['sosmed_type'] = get_active_sosmed();
        
        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();
            $this->account_status = format_status($post_data);
            $regex_rules = $post_data['regex_rules'];

            // validation
            $this->load->library('form_validation');
            foreach($this->validation_config as $k => $item) {
                if( $item['field'] == 'account_name' ) {
                    $new_rules = $item['rules'].'|'.$regex_rules;
                    $item['rules'] = $new_rules;
                }
                $this->validation_config[$k] = $item;
            }
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $id_sosmed = $post_data['sosmed_type'];
                $account_name = $post_data['account_name'];
                $status = $this->account_status;
                
                // checking existing account based on selected sosmed type
                $check_param['where'] = array(
                    'id_sosmed' => $id_sosmed,
                    'name' => $account_name
                );
                $check = $this->sosmed->countSocialmedia(SOSMED_ACCOUNT, $check_param);
                if( $check == 1 ) {
                    $error_account_name = 'Account name is already exist';
                    $data['error_account_name'] = error_msg_generator($error_account_name);
                } else {
                    // save parameter
                    $save_param = array(
                        'id_sosmed' => $id_sosmed,
                        'date_add' => DATETIME,
                        'name' => $account_name,
                        'status' => $status,
                    );
                    $save = $this->sosmed->setNewSocialmedia(SOSMED_ACCOUNT, $save_param);
                    if( isset($save['id']) ) {
                        $id = $save['id'];

                        // set notification
                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    New account has been added.
                                  </div>';
                        $this->session->set_flashdata('notif', $notif);

                        // redirect to list general info page
                        redirect('panel/socialmedia/detail/'.$id);
                    }
                }
            }
        }

        // view page (add courses)
        $data['status'] = $this->account_status;
        $this->load->view('add', $data);
    }
    
    // update socialmedia data, with image or not
    public function detail($id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $param = array(
                'fields' => array(
                    SOSMED_ACCOUNT.'.id AS account_id',
                    SOSMED_ACCOUNT.'.id_sosmed AS sosmed_type',
                    SOSMED_ACCOUNT.'.name AS account_name',
                    SOSMED_ACCOUNT.'.status AS account_status',
                    SOSMED.'.rules'
                ),
                'join' => array(
                    SOSMED => SOSMED.'.id = '.SOSMED_ACCOUNT.'.id_sosmed'
                ),
                'where' => array(
                    SOSMED_ACCOUNT.'.id' => $id
                )
            );
            $account = $this->sosmed->getSocialmedia(SOSMED_ACCOUNT, $param);
            if( !empty($account) ) {
                $account = array_shift($account);
                $this->account_status = $account['account_status'];
                $rules = $this->_format_account_rules($account['rules']);
                $account = array_merge($account, $rules);
                
                // get sosmed type
                $this->load->helper('socialmedia');
                $data['sosmed_type'] = get_active_sosmed();

                if(!empty($_POST)) {
                    $post_data = $this->input->post();
                    $this->account_status = format_status($post_data);
                    $regex_rules = $post_data['regex_rules'];
                    $post_data['account_id'] = $id;
                    $account = $post_data;

                    // validation
                    $this->load->library('form_validation');
                    foreach($this->validation_config as $k => $item) {
                        if( $item['field'] == 'account_name' ) {
                            $new_rules = $item['rules'].'|'.$regex_rules;
                            $item['rules'] = $new_rules;
                        }
                        $this->validation_config[$k] = $item;
                    }
                    $this->form_validation->set_rules($this->validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }

                    if ($this->form_validation->run() == TRUE ) {
                        $id_sosmed = $post_data['sosmed_type'];
                        $account_name = $post_data['account_name'];
                        $post_data['account_id'] = $id;
                
                        // checking existing account based on selected sosmed type
                        $check_param['where'] = array(
                            'id_sosmed' => $id_sosmed,
                            'name' => $account_name,
                            'id != ' => $id
                        );
                        $check = $this->sosmed->countSocialmedia(SOSMED_ACCOUNT, $check_param);
                        if( $check == 1 ) {
                            $error_account_name = 'Account name is already exist';
                            $data['error_account_name'] = error_msg_generator($error_account_name);
                        } else {
                            // update parameter
                            $update_param = array(
                                'id_sosmed' => $id_sosmed,
                                'date_update' => DATETIME,
                                'name' => $account_name,
                                'status' => $this->account_status,
                            );
                            $update = $this->sosmed->updateSocialmedia(SOSMED_ACCOUNT, $update_param, $id);
                            if( $update == 'SUCCESSFUL' ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            Account has been updated.
                                          </div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to list general info page
                                redirect('panel/socialmedia/detail/'.$id);
                            }
                        }
                    } # end of validation
                } # end of post
                
                $data['data'] = $account;
                $data['status'] = $this->account_status;
            }
        }
        
        $this->load->view('detail', $data);
    }
    
    // delete socialmedia data, include of image
    public function delete($id) {
        // check session login
//        $this->_check_session();
        $data['admin_data'] = $this->admin_data;
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting socialmedia. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && $id != 0 || is_numeric($id) ) {
            // check if given ID is existing in database
            $account = $this->sosmed->getSocialmediaById(SOSMED_ACCOUNT, $id);
            // if data is found
            if( !empty($account) ) {
                // get detail of sosmed type
                $id_sosmed = $account['id_sosmed'];
                $type = $this->sosmed->getSocialmediaById(SOSMED, $id_sosmed);

                // run delete action
                $delete = $this->sosmed->deleteSocialmedia(SOSMED_ACCOUNT, $id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    // provide success message
                    $msg = $type['sosmed_name'].' account with name <b>'.$account['account_name'].'</b> has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/socialmedia');
    }
}