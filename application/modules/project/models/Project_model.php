<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Project_model extends CI_model {
    
    public $data;
    public $return;
    private $project;
    private $project_contact;
    private $project_gallery;
    private $project_history;
    private $project_location;
    private $project_scale;
    private $project_status;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
        $this->return = array();
        $this->project = array(
            'id AS project_id',
            'gid AS gid_project',
            'date_add',
            'date_update',
            'date_start',
            'date_end',
            'date_published',
            'scale',
            'title',
            'annotation',
            'description',
            'image_name',
            'website'
        );
        $this->project_contact = array(
            'id AS project_contact_id',
            'name',
            'phone',
            'email'
        );
        $this->project_gallery = array(
            'id AS project_gallery_id',
            'date_add',
            'date_update',
            'image_name',
            'status AS gallery_status'
        );
        $this->project_history = array(
            'id AS project_history_id',
            'date_history',
            'status_id',
            'note'
        );
        $this->project_location = array(
            'id AS project_location_id',
            'building_name',
            'address',
            'province_id',
            'city_id',
            'district_id'
        );
        $this->project_scale = array(
            'id AS project_scale_id',
            'gid AS gid_scale',
            'scale_name'
        );
        $this->project_status = array(
            'id AS project_status_id',
            'gid AS gid_status',
            'status_name'
        );
    }
    
#=================================== CREATE ===================================#
    public function setNewProject($table, $data) {
        $params['table'] = $table;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getProject($table, $params=array()) {
        // fields
        switch($table) {
            case PROJECT: $fields = $this->project; break;
            case PROJECT_CONTACT: $fields = $this->project_contact; break;
            case PROJECT_GALLERY: $fields = $this->project_gallery; break;
            case PROJECT_HISTORY: $fields = $this->project_history; break;
            case PROJECT_LOCATION: $fields = $this->project_location; break;
            case PROJECT_SCALE: $fields = $this->project_scale; break;
            case PROJECT_STATUS: $fields = $this->project_status; break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getProjectById($table, $id) {
        // fields
        switch($table) {
            case PROJECT: $fields = $this->project; break;
            case PROJECT_CONTACT: $fields = $this->project_contact; break;
            case PROJECT_GALLERY: $fields = $this->project_gallery; break;
            case PROJECT_HISTORY: $fields = $this->project_history; break;
            case PROJECT_LOCATION: $fields = $this->project_location; break;
            case PROJECT_SCALE: $fields = $this->project_scale; break;
            case PROJECT_STATUS: $fields = $this->project_status; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getProjectByGid($table, $gid) {
        // fields
        switch($table) {
            case PROJECT: $fields = $this->project; break;
            case PROJECT_CONTACT: $fields = $this->project_contact; break;
            case PROJECT_GALLERY: $fields = $this->project_gallery; break;
            case PROJECT_HISTORY: $fields = $this->project_history; break;
            case PROJECT_LOCATION: $fields = $this->project_location; break;
            case PROJECT_SCALE: $fields = $this->project_scale; break;
            case PROJECT_STATUS: $fields = $this->project_status; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'gid' => $gid
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countProject($table, $params=array()) {
        $params['table'] = $table;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateProject($table, $data, $param) {
        $where = array('id' => $param);
        if(is_array($param)) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = $where;
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteProject($table, $param) {
        $where = array('id' => $param);
        if(is_array($param)) {
            $where = $param;
        }
        $params['table'] = $table;
        $params['where'] = $where;
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}