<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#NEWS SECTION#
if(!function_exists('list_project')){
    function list_project($data){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        
        $project_list = $CI->Project_model->list_project($data);
        return $project_list;
    }
}
if(!function_exists('count_list_project')){
    function count_list_project(){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        
        $count_project_list = $CI->Project_model->count_project();
        return $count_project_list;
    }
}

if(!function_exists('count_list_project_with_params')){
    function count_list_project_with_params($data){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        
        $count_project_list = $CI->Project_model->count_project_with_params($data);
        return $count_project_list;
    }
}

if(!function_exists('get_project')){
    function get_project($data){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        $project = $CI->Project_model->get_project($data);
        return $project;
    }
}
#END OF NEWS SECTION#

#NEWS TYPE SECTION#
if(!function_exists('list_project_type')){
    function list_project_type($data){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        
        $project_list = $CI->Project_model->list_project_type($data);
        return $project_list;
    }
}
if(!function_exists('count_list_project_type')){
    function count_list_project_type(){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        
        $count_project_list = $CI->Project_model->count_project_type();
        return $count_project_list;
    }
}
if(!function_exists('get_project_type')){
    function get_project_type($data){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        $project_type = $CI->Project_model->get_project_type($data);
        return $project_type;
    }
}

#END OF NEWS TYPE SECTION

#NEWS TYPE SECTION#
if(!function_exists('list_project_tags')){
    function list_project_tags($data){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        
        $project_tags_list = $CI->Project_model->list_project_tags($data);
        return $project_tags_list;
    }
}
if(!function_exists('count_list_project_tags')){
    function count_list_project_tags(){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        
        $count_project_tags_list = $CI->Project_model->count_project_tags();
        return $count_project_tags_list;
    }
}
if(!function_exists('get_project_tags')){
    function get_project_tags($data){
        $CI = &get_instance();
        $CI->load->model('project/Project_model');
        $project_tags = $CI->Project_model->get_project_tags($data);
        return $project_tags;
    }
}

#END OF NEWS TYPE SECTION


