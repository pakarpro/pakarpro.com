<?php echo call_header('main', 'project'); ?>

    <section id="project">
        <div  class="container">
            <div class="center">
                <h2>Our Projects</h2>
            </div>

            <div class="row">
                <div class="project-items">
                    <?php if( isset($project) && !empty($project) ) { ?>
                        <div class="row">
                            <?php
                                $no = 1;
                                foreach($project as $k => $item) {
                                    // detail url
                                    $detail_url = site_url('project/'.$item['gid_project']);
                                    // image
                                    $img_url = base_url(IMG_PATH.'project/'.$item['project_id'].'/'.$item['image_name']);
                            ?>
                            <div class="col-xs-12 col-md-4 col-sm-6" style="margin-bottom: 30px;">
                                <div class="recent-work-wrap">
                                    <img class="img-responsive img-thumbnail" src="<?php echo $img_url; ?>" alt="">
                                    <div class="overlay">
                                        <div class="recent-work-inner">
                                            <h3 class="text-bold"><a href="<?php echo $detail_url; ?>"><?php echo $item['title']; ?></a></h3>
                                            <p><?php echo $item['annotation']; ?></p>
                                            <a class="preview" href="<?php echo $detail_url; ?>" style="margin-right:15px;">
                                                <i class="fa fa-eye"></i> Read More
                                            </a>
                                            <a class="preview" href="<?php echo $item['website']; ?>" target="_blank">
                                                <i class="fa fa-external-link"></i> Visit Link
                                            </a>
                                        </div> 
                                    </div>
                                </div>
                            </div><!--/.project-item-->
                        <?php
                                    if( $no % 3 == 0 ) {
                                        echo '</div>';
                                        echo '<div class="row">';
                                    }
                                }
                        ?>
                    <?php } else { ?>
                        <h2>
                            Belum ada proyek terbaru.
                            Kembali ke <a href="<?php echo site_url(); ?>">halaman utama</a>.
                        </h2>
                    <?php } ?>

                    <?php
                        if( isset($pagination) ) {
                            print_r($pagination);
                        }
                    ?>
                </div><!--/.project-items-->
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#blog-->

<?php echo call_footer('main'); ?>