<?php echo call_header('main', 'project'); ?>

    <?php
        if( isset($project) ) {
            // image
            $img_url = base_url(IMG_PATH.'project/'.$project['project_id'].'/'.$project['image_name']);
    ?>
        <section id="blog">
            <div class="container">
                <div class="center">
                    <h2><?php echo $project['title']; ?></h2>
                </div>

                <div class="blog">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="blog-item">
                                <img class="img-responsive img-blog img-thumbnail" src="<?php echo $img_url; ?>" width="100%" alt="">
                                <div class="row">  
                                    <div class="col-sm-12 blog-content">
                                        <!--<h2><?php //echo $project['title']; ?></h2>-->
                                        <div style="text-align: justify !important;"><?php echo $project['description']; ?></div>
                                    </div>
                                </div>
                            </div><!--/.blog-item-->
                            
                            <?php if(!empty($project['gallery'])) { ?>
                            <div class="blog-item gallery">
                                <div class="row">
                                    <?php
                                        foreach($project['gallery'] as $k => $item) {
                                            $gallery_url = base_url(IMG_PATH.'project/'.$project['project_id'].'/gallery/'.$item['image_name']);
                                    ?>
                                        <div class="col-xs-12 col-md-4 col-sm-6">
                                            <div class="recent-work-wrap" style="margin-bottom: 30px;">
                                                <img class="img-responsive" src="<?php echo $gallery_url; ?>" alt="">
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div><!--gallery-->
                            <?php } ?>
                        </div>

                        <aside class="col-md-4 project-detail">
                            <?php
                                if( !empty($project['history']) ) {
                                    $history = $project['history'];
                            ?>
                                <div class="widget project-history">
                                    <h3>Project Status</h3>
                                    <ul class="blog_archieve">
                                        <li>
                                            <h4 class="text-bold"><?php echo $history['status_name']; ?></h4>
                                            <?php if( !empty($history['note']) ) { ?>
                                                <p><?php echo $history['note']; ?></p>
                                            <?php } ?>
                                        </li>
                                    </ul>
                                </div><!--/.project-history-->
                            <?php } ?>
                            
                            <?php if( !empty($project['location']) ) { ?>
                                <div class="widget project-location">
                                    <h3>Location</h3>
                                    <ul class="blog_archieve">
                                        <li><?php echo $project['location']; ?></li>
                                    </ul>
                                </div><!--/.project-location-->
                            <?php } ?>

                            <div class="widget project-scale">
                                <h3>Scale</h3>
                                <ul class="blog_archieve">
                                    <li><?php echo $project['scale']; ?></li>
                                </ul>
                                
                            </div><!--/.project-scale-->

                            <?php if(!empty($project['contact'])) { ?>
                                <div class="widget project-contact">
                                    <h3>Contact</h3>
                                    <ul class="blog_archieve">
                                    <?php foreach($project['contact'] as $k => $item) { ?>
                                        <li><i class="fa fa-angle-double-right"></i> <?php echo implode(' | ', $item); ?></li>
                                    <?php } ?>
                                    </ul>
                                </div><!--/.project-contact-->
                            <?php } ?>
                        </aside>
                    </div><!--/.row-->
                 </div>
            </div><!--/.container-->
        </section><!--/#blog-->
    <?php } ?>

<?php echo call_footer('main'); ?>