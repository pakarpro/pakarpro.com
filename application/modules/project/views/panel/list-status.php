<?php echo call_header('panel', 'List of Project Status'); ?>

<?php echo call_sidebar('project', 'status'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php echo $this->session->flashdata('notif'); ?>
                        
                        <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/project/status/add'); ?>">ADD NEW PROJECT STATUS</a>
                        <table class="table table-hover table-list">
                            <thead>
                                <tr>
                                    <th width="50">#</th>
                                    <th width="120">Keyword</th>
                                    <th>Status name</th>
                                    <th width="30"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( !empty($project) ) {
                                        foreach($project as $k => $item) {
                                ?>
                                    <tr data-target="<?php echo site_url('panel/project/status/detail/'.$item['project_status_id']); ?>" data-id="<?php echo $item['project_status_id']; ?>">
                                        <td class="_l"><?php echo $k+1; ?></td>
                                        <td class="_l"><?php echo $item['gid_status']; ?></td>
                                        <td class="_l"><?php echo $item['status_name']; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('panel/project/status/delete/'.$item['project_status_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this project status?');">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="4">Data not found</td></tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <?php if( isset($pagination) ) { ?>
                        <!-- pagination -->
                        <div class="row clearfix">
                            <div class="col-xs-12 text-center">
                                <?php print_r($pagination); ?>
                            </div>
                        </div>
                        <!-- /pagination -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>
<script>
    // updating project history
    updateProjectHistory();

    function updateProjectHistory() {
        $(document).on('click', '._cps', function() {
            var id = $(this).parent().parent().attr('data-id');
            var status = $('#project_status-'+id).val();

            var data = {
                "id": id,
                "status": status
            }

            $.post('changestatus', data, function(response) {
                // do something here, for custom
            });
        });
    }
</script>