<?php echo call_header('panel', 'Lists of Projects'); ?>

<?php echo call_sidebar('project', 'all'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body table-responsive">
                        <?php echo $this->session->flashdata('notif'); ?>
                        
                        <a class="btn waves-effect btn-primary m-b-15" href="<?php echo site_url('panel/project/all/add'); ?>">ADD NEW PROJECT</a>
                        <table class="table table-hover table-list">
                            <thead>
                                <tr>
                                    <th width="50">#</th>
                                    <th width="100">Date Start</th>
                                    <th>Item</th>
                                    <th width="180">Status</th>
                                    <th width="80">Scale</th>
                                    <th width="30"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if( !empty($project) ) {
                                        foreach($project as $k => $item) {
                                            $history = $item['history'];
                                ?>
                                    <tr data-target="<?php echo site_url('panel/project/all/detail/'.$item['project_id']); ?>" data-id="<?php echo $item['project_id']; ?>">
                                        <td class="_l"><?php echo $k+1; ?></td>
                                        <td class="_l"><?php echo format_datetime($item['date_start'], 'd'); ?></td>
                                        <td class="_l">
                                            <span class="text-bold"><?php echo $item['title']; ?></span> <br>
                                            <?php echo $item['annotation']; ?>
                                        </td>
                                        <td class="_l">
                                            <span class="text-bold"><?php echo $history['status_name']; ?></span> <br>
                                            <?php echo $history['note']; ?>
                                        </td>
                                        <td>
                                            <select id="project_scale-<?php echo $item['project_id']; ?>" class="show-tick form-control _cps">
                                                <option value="">- Choose -</option>
                                                <?php
                                                    foreach($project_scale as $key => $row) {
                                                        $selected = '';
                                                        if( $row['project_scale_id'] == $item['scale'] ) {
                                                            $selected = 'selected';
                                                        }
                                                        echo '<option value="'.$row['project_scale_id'].'" '.$selected.'>'.$row['scale_name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('panel/project/all/delete/'.$item['project_id']); ?>" class="waves-effect" title="Delete" onclick="return confirm('Delete this project?');">
                                                <i class="material-icons">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="6">Data not found</td></tr>';
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    
                    <?php if( isset($pagination) ) { ?>
                        <!-- pagination -->
                        <div class="row clearfix">
                            <div class="col-xs-12 text-center">
                                <?php print_r($pagination); ?>
                            </div>
                        </div>
                        <!-- /pagination -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<!-- Jquery DataTable CSS -->
<link href="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css'); ?>" rel="stylesheet">
<!-- Jquery Datatables JS -->
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url(PLG_PATH.'jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js'); ?>"></script>
<!-- Custom DataTable JS -->
<script src="<?php echo base_url(PANEL_PATH.'js/jquery-datatable.js'); ?>"></script>
<script src="<?php echo base_url(PANEL_PATH.'js/table-list.js'); ?>"></script>
<script>
    // updating project history
    updateProjectHistory();

    function updateProjectHistory() {
        $(document).on('click', '._cps', function() {
            var id = $(this).parent().parent().attr('data-id');
            var scale = $('#project_scale-'+id).val();

            var data = {
                "id": id,
                "scale": scale
            }

            $.post('changescale', data, function(response) {
                // do something here, for custom
            });
        });
    }
</script>