<?php echo call_header('panel', 'Add New Project'); ?>

<?php echo call_sidebar('project', 'all'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <form action="<?php echo site_url('panel/project/all/add'); ?>" method="post" enctype="multipart/form-data" novalidate>
                                    <!-- General Information -->
                                    <div class="clearfix general-information">
                                        <h3 class="m-t-0">General Information</h3>
                                        <div class="form-group">
                                            <label>Project name</label>
                                            <?php echo form_error('title'); ?>
                                            <div class="form-line">
                                                <input name="title" id="title" type="text" class="form-control" value="<?php echo set_value('title'); ?>" autofocus required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Keyword (for SEO)</label>
                                            <div class="form-line">
                                                <?php
                                                    $checked_gid = '';
                                                    $required_gid = '';
                                                    $disabled_gid = 'disabled';
                                                    $gid = '';
                                                    $gid_custom = '';
                                                    $gid_default = set_value('gid_project');
                                                    $gid_custom_error = '';
                                                    if(!empty(set_value('use_custom_gid'))) {
                                                        $checked_gid = 'checked';
                                                        $required_gid = 'required';
                                                        $disabled_gid = '';
                                                        $gid = $data['gid'];
                                                        $gid_custom = set_value('gid_custom');
                                                        $gid_default = set_value('gid_default');
                                                        $gid_custom_error = form_error('gid_custom');
                                                    }
                                                ?>
                                                <input name="gid" type="hidden" class="form-control" value="<?php echo $gid; ?>">
                                                <input name="gid_project" type="hidden" class="form-control" value="<?php echo set_value('gid_project'); ?>">
                                                <input name="gid_default" id="gid_default" type="hidden" class="gid form-control" value="<?php echo $gid_default; ?>">
                                                <p class="gid"><?php echo $gid_default; ?></p>
                                                <div class="clearfix">
                                                    <input name="use_custom_gid" type="checkbox" id="use_custom_gid" class="filled-in chk-col-red" <?php echo $checked_gid; ?>>
                                                    <label for="use_custom_gid">USE CUSTOM</label>
                                                    <?php echo $gid_custom_error; ?>
                                                    <input name="gid_custom" id="gid_custom" type="text" class="form-control" value="<?php echo $gid_custom; ?>" <?php echo $disabled_gid.' '.$required_gid; ?>>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6 col-xs-12">
                                                <label class="form-label">Scale</label>
                                                <?php echo form_error('scale'); ?>
                                                <select name="scale" class="form-control show-tick" required="required">
                                                    <option value="">- Choose -</option>
                                                    <?php
                                                        foreach($project_scale as $k => $item) {
                                                            $selected = '';
                                                            if( $item['project_scale_id'] == set_value('scale') ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$item['project_scale_id'].'" '.$selected.'>'.$item['scale_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-md-6 col-xs-12">
                                                <label>Web URL</label>
                                                <?php echo form_error('website'); ?>
                                                <div class="form-line">
                                                    <input name="website" type="text" class="form-control" placeholder="e.g. http://pakarpro.com/pcm" value="<?php echo set_value('website'); ?>">
                                                </div>
                                            </div>
                                        </div>

                                        <?php /*
                                        <div class="form-group row">
                                            <div class="col-sm-6 col-xs-12">
                                                <label>Date Start</label>
                                                <?php echo form_error('date_start'); ?>
                                                <div class="form-line">
                                                    <input type="text" id="date_start" name="date_start" class="form-control" placeholder="Beginning of project" value="<?php echo set_value('date_start'); ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <label>Date End</label>
                                                <?php echo form_error('date_end'); ?>
                                                <div class="form-line">
                                                    <input type="text" id="date_end" name="date_end" class="form-control" placeholder="End of project" value="<?php echo set_value('date_end'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                        */ ?>

                                        <div class="form-group">
                                            <label>Content</label>
                                            <?php echo form_error('description'); ?>
                                            <div class="form-line">
                                                <textarea id="description" name="description" class="form-control" required="required"><?php echo set_value('description'); ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Image</label>
                                            <?php if(isset($error_image)) echo $error_image; ?>
                                            <div class="form-line">
                                                <input type="file" name="image_name" id="imageInput" class="form-control">
                                            </div>
                                            <small class="rules">File size should be equal or less than 2 MB, with minimum size 600 x 350 px</small>
                                        </div>
                                    </div><!--/ General Information -->
                                    
                                    <!-- Project Status -->
                                    <div class="clearfix tracker">
                                        <h3>Project Status</h3>
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <label>Date Progress</label>
                                                <?php echo form_error('date_history'); ?>
                                                <div class="form-line">
                                                    <input type="text" id="date_history" name="date_history" class="form-control" placeholder="Progress of project" value="<?php echo set_value('date_history'); ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label class="form-label">Status</label>
                                                <?php echo form_error('project_status'); ?>
                                                <select name="project_status" class="form-control show-tick" required="required">
                                                    <option value="">- Choose -</option>
                                                    <?php
                                                        foreach($project_status as $k => $item) {
                                                            $selected = '';
                                                            if( $item['project_status_id'] == set_value('project_status') ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$item['project_status_id'].'" '.$selected.'>'.$item['status_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label>Note</label>
                                                <?php echo form_error('note'); ?>
                                                <div class="form-line">
                                                    <input type="text" name="note" class="form-control" placeholder="e.g. Peletakkan batu pertama" value="<?php echo set_value('note'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--/ Project Status -->
                                    
                                    <!-- Location -->
                                    <div class="clearfix location">
                                        <h3>Location</h3>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-sm-6 col-xs-12">
                                                    <label>Building Name</label>
                                                    <?php echo form_error('building_name'); ?>
                                                    <div class="form-line">
                                                        <input type="text" name="building_name" class="form-control" placeholder="e.g. Gedung CCTV Camera Indonesia" value="<?php echo set_value('building_name'); ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <label>Address</label>
                                                    <?php echo form_error('address'); ?>
                                                    <div class="form-line">
                                                        <input type="text" name="address" class="form-control" placeholder="e.g. Jl. Bungur Besar 17 No. 6" value="<?php echo set_value('address'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="form-label">Province</label>
                                                    <?php echo form_error('province'); ?>
                                                    <select id="province" name="province" class="form-control show-tick">
                                                        <option value="">- Choose -</option>
                                                        <?php
                                                            foreach($province as $k => $item) {
                                                                $selected = '';
                                                                if( $item['province_id'] == set_value('province') ) {
                                                                    $selected = 'selected';
                                                                }
                                                                echo '<option value="'.$item['province_id'].'" '.$selected.'>'.$item['province_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-label">City</label>
                                                    <?php echo form_error('city'); ?>
                                                    <select id="city" name="city" class="form-control show-tick">
                                                        <option value="">- Choose -</option>
                                                        <?php
                                                            foreach($city as $k => $item) {
                                                                $selected = '';
                                                                if( $item['city_id'] == set_value('city') ) {
                                                                    $selected = 'selected';
                                                                }
                                                                echo '<option value="'.$item['city_id'].'" '.$selected.'>'.$item['city_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="form-label">District</label>
                                                    <?php echo form_error('district'); ?>
                                                    <select id="district" name="district" class="form-control show-tick">
                                                        <option value="">- Choose -</option>
                                                        <?php
                                                            foreach($district as $k => $item) {
                                                                $selected = '';
                                                                if( $item['district_id'] == set_value('district') ) {
                                                                    $selected = 'selected';
                                                                }
                                                                echo '<option value="'.$item['district_id'].'" '.$selected.'>'.$item['district_name'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--/ Location -->
                                    
                                    <!-- Marketing Contact -->
                                    <div class="clearfix marketing-contact">
                                        <h3>Marketing Contact</h3>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">person</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <?php echo form_error('name'); ?>
                                                        <input name="name" type="text" class="form-control" value="<?php echo set_value('name'); ?>" placeholder="Name">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">phone</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <?php echo form_error('phone'); ?>
                                                        <input name="phone" type="number" class="form-control" placeholder="Phone" value="<?php echo set_value('phone'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="material-icons">email</i>
                                                    </span>
                                                    <div class="form-line">
                                                        <?php echo form_error('email'); ?>
                                                        <input name="email" type="email" class="form-control" placeholder="Email" value="<?php echo set_value('email'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!--/ Marketing Contact -->
                                    
                                    <?php /*
                                    <div class="form-group">
                                        <label>Project Images</label>
                                        <div id="projectImages" class="dropzone">
                                            <div class="dz-message">
                                                <div class="drag-icon-cph">
                                                    <i class="material-icons">touch_app</i>
                                                </div>
                                                <h3>Drop files here or click to upload.</h3>
                                            </div>
                                        </div>
                                    </div>
                          
                                    <div class="form-group">
                                        <label>Project References/Brochures</label>
                                        <div id="projectReferences" class="dropzone"></div>
                                    </div>
                                    */ ?>
                          
                                    <div class="form-group">
                                        <a href="<?php echo site_url('panel/project/all'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right m-r-10" value="SAVE">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<!-- Bootstrap Material Datetime Picker Css -->
<link href="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet">
<!-- Autosize Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'autosize/autosize.js'); ?>"></script>
<!-- Moment Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'momentjs/moment.js'); ?>"></script>
<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js'); ?>"></script>
<!-- TinyMCE -->
<script src="<?php echo base_url(PLG_PATH.'tinymce/tinymce.js'); ?>"></script>
<!-- Dropzone Css -->
<link href="<?php echo base_url(PLG_PATH.'dropzone/min/dropzone.min.css'); ?>" rel="stylesheet">
<!-- Dropzone Plugin Js -->
<script src="<?php echo base_url(PLG_PATH.'dropzone/min/dropzone.min.js'); ?>"></script>
<script>
    // ajax for location [province, city, district]
    // change city data
    $(document).on('change', '#province', function() {
        var i = 0;
        var id = $('#province').val();
        var data = {id: id}
        console.log(data);
        
        $.post('/area/ajax_get_city', data, function(response) {
            if( response != '' || response != '[]' ) {
                var obj = JSON.parse(response);
                var count = obj.length;

                $('#city').html('<option value="">- Choose -</option>');
                $('#district').html('<option value="">- Choose -</option>');
                for(i; i<count; i++) {
                    $('#city').append(
                        '<option value="'+ obj[i].city_id +'">'+ obj[i].city_name +'</option>'
                    );
                }
            }
        });
        $('#city').selectpicker('refresh');
        $('#district').selectpicker('refresh');
    });
    
    // change district data
    $(document).on('change', '#city', function() {
        var i = 0;
        var id = $('#city').val();
        var data = {id: id}
        
        $.post('/area/ajax_get_district', data, function(response) {
            if( response != '' || response != '[]' ) {
                var obj = JSON.parse(response);
                var count = obj.length;

                $('#district').html('<option value="">- Choose -</option>');
                for(i; i<count; i++) {
                    $('#district').append(
                        '<option value="'+ obj[i].district_id +'">'+ obj[i].district_name +'</option>'
                    );
                }
                $('#district').selectpicker('refresh');
            }
        });
    });

    // for editor, use tinymce for better
    tinymce.init({
        selector: "textarea#description",
        theme: "modern",
        height: 350,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu jbimages'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinymce.relative_urls = false;
    
    // gid
    $(document).on('change', '#title', function() {
        generate_gid('title');
    }).on('keyup', '#title', function() {
        generate_gid('title');
    }).on('blur', '#title', function() {
        generate_gid('title');
    });
    
    $(document).on('click', '#use_custom_gid', function() {
        if($(this).is(':checked')) {
            var default_gid = $('#gid_default').val();
            var gid = $('#gid_custom').val();
            if( gid == '' ) {
                gid = default_gid;
            }
            $('#gid_custom').removeAttr('disabled').focus().val(gid);
        } else {
            $('#gid_custom').attr('disabled', 'disabled');
        }
    });
    
    function generate_gid(element) {
        var title = $('#'+element).val();
        
        title = title.replace(/\s/g, "-").toLowerCase();
        $('.gid').val(title).text(title);
    }
    
    //Datetimepicker plugin
    $('#date_history').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        time: false,
        minDate: new Date(),
        clearButton: true,
        weekStart: 0
    });
    
    <?php /*
    //Datetimepicker plugin
    $('#date_end').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        time: false,
        clearButton: true,
        weekStart: 0
    });
    $('#date_start').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD',
        time: false,
        minDate: new Date(),
        clearButton: true,
        weekStart: 0
    }).on('change', function(e, date) {
        $('#date_end').bootstrapMaterialDatePicker('setMinDate', date);
    });
    */ ?>
        
    // dropzone for multiple upload images
    Dropzone.autoDiscover = false;
    
//    Dropzone.options.projectImages = {
//        url: "<?php echo site_url('panel/project/processImages'); ?>",
//        paramName: "project_image",
//        maxFilesize: 2,
//        parallelUploads: 15,
//        maxFiles: 15,
//        uploadMultiple: true,
//        addRemoveLinks: true,
//        removedfile: function(file) {
//            console.log(file);
//            var name = file.name;
//            $.ajax({
//                type: 'POST',
//                url: '<?php echo site_url('panel/project/deleteImage'); ?>',
//                data: "image_name="+name,
//                dataType: 'html',
//                success: function(response) {
//                    console.log(response);
//                }
//            });
//            var _ref;
//            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
//        },
//    };
    
    // for project images
    $("div#projectImages").dropzone({
        url: "<?php echo site_url('panel/project/processImages'); ?>",
        paramName: "project_image", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        parallelUploads: 15,
        maxFiles: 15,
        uploadMultiple: true,
        addRemoveLinks: true,
        removedfile: function(file) {
            console.log(file);
            var name = file.name;
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('panel/project/deleteImage'); ?>',
                data: "image_name="+name,
                dataType: 'html',
                success: function(response) {
                    console.log(response);
                }
            });
            var _ref;
            return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, done) {
            console.log(done);
        }
    });
    
//    Dropzone.options.projectImages = {
//        init: function() {
//            thisDropzone = this;
//            <!-- 4 -->
//            $.get('<?php echo site_url('panel/project/processImages') ?>', function(data) {
//alert(data);
//                <!-- 5 -->
//                $.each(data, function(key,value){
//
//                    var mockFile = { name: value.name, size: value.size };
//
//                    thisDropzone.options.addedfile.call(thisDropzone, mockFile);
//
//                    thisDropzone.options.thumbnail.call(thisDropzone, mockFile, "uploads/"+value.name);
//
//                });
//
//            });
//        }
//    };

    // for project reference images
    $("div#projectReferences").dropzone({
//        url: "add",
        url: "<?php echo site_url('panel/project/processImages'); ?>",
        paramName: "reference_image", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        addRemoveLinks: true,
        parallelUploads: 10,
        uploadMultiple: true,
        success: function(file, done) {
            console.logs(file);
        }
    });
</script>