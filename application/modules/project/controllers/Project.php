<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {
    
    private $CI;
    private $project;
    private $project_status = 1;
    private $page = 1;
    private $redirect = 'panel/login';
    private $all = 'project';
    private $status = 'status';
    private $img_path;
    private $temp_path;
    private $admin_data = array();
    private $project_validation_config = array(
        array(
            'field'   => 'gid',
            'label'   => 'Keyword',
            'rules'   => 'trim'
        ),
        array(
            'field'   => 'name',
            'label'   => 'Project name',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'description',
            'label'   => 'Description',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'url',
            'label'   => 'Project URL',
            'rules'   => 'trim|required|valid_url'
        ),
    );
    private $scale_validation_config = array(
        array(
            'field'   => 'scale_name',
            'label'   => 'Scale name',
            'rules'   => 'trim|required'
        )
    );
    private $status_validation_config = array(
        array(
            'field'   => 'status_name',
            'label'   => 'Status name',
            'rules'   => 'trim|required'
        )
    );
    private $gid_validation = array(
        'field'   => 'gid_custom',
        'label'   => 'Keyword',
        'rules'   => 'trim|required'
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'is_unique' => '%s is already taken',
        'edit_unique' => '%s is already taken'
    );

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();

        $this->load->helper('form');
        $this->load->model('Project_model');
        $this->project = new Project_model();
        
        // image path
        $this->img_path = FCPATH.IMG_PATH.'project/';
        $this->temp_path = FCPATH.IMG_PATH.'temp/project/';
         
        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // check session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
    
    // generate annotation
    private function _annotation($content) {
        $content = strip_tags($content);
        $content = preg_replace("/&#?[a-z0-9]+;/i","",$content);
        $annotation = summarize_sentence($content, 15);
        
        return $annotation;
    }
    
    // formatting input data
    private function _format_input_data($data) {
        // title
        if( isset($data['title']) ) {
            $data['title'] = ucwords($data['title']);
        }
        
//        // date start
//        if( empty($data['date_start']) ) {
//            $data['date_start'] = '0000-00-00';
//        }
//        
//        // date end
//        if( empty($data['date_end']) ) {
//            $data['date_end'] = '0000-00-00';
//        }
        
        // note
        if( isset($data['note']) ) {
            $data['note'] = ucfirst($data['note']);
        }
        
        // building_name
        if( isset($data['building_name']) ) {
            $data['building_name'] = ucwords($data['building_name']);
        }
        
        // name
        if( isset($data['name']) ) {
            $data['name'] = ucwords($data['name']);
        }
        
        // address
        if( isset($data['address']) ) {
            $data['address'] = ucwords($data['address']);
        }
        
        // email
        if( isset($data['email']) ) {
            $data['email'] = strtolower($data['email']);
        }
        
        // gid
        if( isset($data['gid_custom']) ) {
            $data['gid_custom'] = strtolower($data['gid_custom']);
        }
        $gid_project = $data['gid_default'];
        if( isset($data['use_custom_gid']) ) {
            $gid_project = $data['gid_custom'];
        }
        $data['gid'] = $gid_project;    // for validation use

        return $data;
    }
    
    // get project location
    private function _get_location_by_id($project_id) {
        $where['fields'] = array(
            PROJECT_LOCATION.'.building_name',
            PROJECT_LOCATION.'.address',
            AREA_DISTRICT.'.district_name',
            AREA_CITY.'.city_name',
            AREA_PROVINCE.'.province_name'
        );
        $where['where'] = array(
            PROJECT_LOCATION.'.project_id' => $project_id
        );
        $where['join'] = array(
            AREA_PROVINCE => AREA_PROVINCE.'.id = '.PROJECT_LOCATION.'.province_id',
            AREA_CITY => AREA_CITY.'.id = '.PROJECT_LOCATION.'.city_id',
            AREA_DISTRICT => AREA_DISTRICT.'.id = '.PROJECT_LOCATION.'.district_id',
        );
        $location = $this->project->getProject(PROJECT_LOCATION, $where);
        if( !empty($location) ) {
            $location = array_shift($location);
            if( isset($location['city_name']) ) {
                $location['city_name'] = str_replace(', Kota', '', $location['city_name']);
            }
        }
        
        return $location;
    }
    
    // get project contact
    private function _get_contact_by_id($project_id) {
        $where['where'] = array(
            'project_id' => $project_id
        );
        $contact = $this->project->getProject(PROJECT_CONTACT, $where);

        return $contact;
    }
    
    // get project history
    private function _get_history_by_id($project_id, $limit=FALSE) {
        if( $limit == TRUE ) {
            $where['limit'] = 1;
        }
        $where['fields'] = array(
            PROJECT_STATUS.'.status_name',
            PROJECT_HISTORY.'.note'
        );
        $where['where'] = array(
            PROJECT_HISTORY.'.project_id' => $project_id
        );
        $where['join'] = array(
            PROJECT_STATUS => PROJECT_STATUS.'.id = '.PROJECT_HISTORY.'.status_id'
        );
        $where['order_by'] = array(
            PROJECT_HISTORY.'.status_id' => 'DESC'
        );
        $history = $this->project->getProject(PROJECT_HISTORY, $where);
        if( !empty($history) && count($history) == 1 ) {
            $history = array_shift($history);
        }
        
        return $history;
    }
    
    // get all photos of related project
    private function _get_gallery_by_id($project_id) {
        $where['where'] = array(
            'project_id' => $project_id
        );
        $gallery = $this->project->getProject(PROJECT_GALLERY, $where);
        
        return $gallery;
    }
    
    // get list of project
    private function _get_project($param=array()) {
        $param['fields'] = array(
            'id AS project_id',
            'date_start',
            'scale',
            'title',
            'annotation'
        );
        $data = $this->project->getProject(PROJECT, $param);

        return $data;
    }
    
    // get list of project_scale
    private function _get_project_scale($param=array()) {
        $data = $this->project->getProject(PROJECT_SCALE, $param);

        return $data;
    }
    
    // get list of project_status
    private function _get_project_status($param=array()) {
        $data = $this->project->getProject(PROJECT_STATUS, $param);

        return $data;
    }
    
    // get history data
    private function _get_history($param=array()) {
        $param['fields'] = array(
            'id AS history_id',
            'project_id',
            'status_id',
            'note'
        );
        $data = $this->project->getProject(PROJECT_HISTORY, $param);
        if( count($data) == 1 ) {
            $data = array_shift($data);
        }
        
        return $data;
    }
    
    // updating project history, for ajax use
//    public function changehistory() {
//        $return['status'] = 'failed';
//        $data = $this->input->post();
//
//        if( !empty($data) ) {
//            $id = $data['id'];
//            $status = $data['status'];
//            
//            $update_param = array(
//                'date_update' => DATETIME,
//                'status_id' => $status
//            );
//            $update = $this->project->updateUser(PROJECT_HISTORY, $update_param, $id);
//            if( $update == 'SUCCESSFUL' ) {
//                $return['status'] = 'success';
//            }
//        }
//        
//        echo json_encode($return);
//    }
    
    // updating project scale, for ajax use
    public function changescale() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $scale = $data['scale'];
            
            $update_param = array(
                'date_update' => DATETIME,
                'scale' => $scale
            );
            $update = $this->project->updateProject(PROJECT, $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    // deleting uploaded image on HTML page and in directory
    public function deleteImage() {
        $image = $this->input->post('image_name');
        echo $image;
        
        unlink($this->temp_path.$image);
    }
    
    // getting uploaded image property from dropzone
    public function processImages() {
        $source = $_FILES;
        
        if( !empty($source) ) {
            $this->load->helper('uploads/uploads');
            $uploaded_data = upload_image_to_temp('project', $source, $this->temp_path);

            header('Content-type: text/json');              //3
            header('Content-type: application/json');
            echo json_encode($uploaded_data['uploaded_data']);
        } else {
            $result = array();
            $temp = glob($this->temp_path.'*');
            foreach( $temp as $file ) {
                $file_name = explode('/', $file);
                $file_name = end($file_name);
                
                $obj['filename'] = $file_name;
                $obj['size'] = filesize($file);
                $obj['type'] = 'image/'.pathinfo($file, PATHINFO_EXTENSION);
                $result[] = $obj;
            }

            header('Content-type: text/json');              //3
            header('Content-type: application/json');
            echo json_encode($result);
        }
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for main page
     * -------------------------------------------------------------------------
     */
    public function index($param='') {
        if( !empty($param) && !is_numeric($param) && !is_array($param) ) {
            $file = 'detail';
            $gid = $param;
            
            $project = $this->project->getProjectByGid(PROJECT, $gid);
            if( !empty($project) ) {
                $project_id = $project['project_id'];
                
                // get location
                $location = $this->_get_location_by_id($project_id);
                if( !empty($location) ) {
                    $location = array_filter($location);
                    $location = implode(', ', $location);
                }
                $project['location'] = $location;
                
                // get contact
                $contact = $this->_get_contact_by_id($project_id);
                if( !empty($contact) ) {
                    foreach($contact as $k => $item) {
                        unset($item['project_contact_id']);
                        unset($item['project_id']);
                        $item['name'] = '<span class="text-bold">'.$item['name'].'</span>';
                        $item['email'] = '<a href="mailto:'.$item['email'].'">'.$item['email'].'</a>';
                        
                        $contact[$k] = $item;
                    }
                }
                $project['contact'] = $contact;
                
                // get latest history
                $project['history'] = $this->_get_history_by_id($project_id, true);
                
                // get gallery
                $project['gallery'] = $this->_get_gallery_by_id($project_id);
                
                // get scale
                $scale_where_param['where'] = array(
                    'id' => $project['scale']
                );
                $scale = $this->project->getProject(PROJECT_SCALE, $scale_where_param);
                if( !empty($scale) ) {
                    $scale = array_shift($scale);
                }
                $project['scale'] = $scale['scale_name'];
                
                $data['project'] = $project;
            }
        } else {
            $file = 'list';
            $this->page = $param;
        
            $count_project = $this->project->countProject(PROJECT);
            if( $count_project > 0 ) {
                // set pagination
                $limit_per_page = 5;
                $start_row = ($this->page-1) * $limit_per_page;
                if( $count_project > $limit_per_page ) {
                    // additional where param
                    $project_param['limit'] = array($limit_per_page, $start_row);
                    $paging_config = array(
                        'base_url'      =>  site_url('project'),
                        'total_rows'    =>  $count_project,
                        'per_page'      =>  $limit_per_page
                    );
                    $config = paging_config($paging_config, false);
                    $this->load->library('pagination');
                    $this->pagination->initialize($config);
                    $data['pagination'] = $this->pagination->create_links();
                }
                
                // get project
                $project_param['order_by'] = array(
                    'date_add' => 'ASC'
                );
                $project = $this->project->getProject(PROJECT, $project_param);

                $data['project'] = $project;
            }
        }

        $this->load->view('main/'.$file, $data);
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for panel page
     * -------------------------------------------------------------------------
     */
    
    // list all project data
    public function lists($type='all', $page=1) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // set table
        $table = PROJECT;
        $file = 'list-project';
        $func = '_get_project';
        if( $type != 'all' ) {
            $file = 'list-'.$type;
            $func = '_get_project_'.$type;
            $table = PROJECT.'_'.$type;
        }
        
        $count_project = $this->project->countProject($table);
        if( $count_project > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            if( $count_project > $limit_per_page ) {
                // additional where param
                $where_param['limit'] = array($limit_per_page, $start_row);
                
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/project/'.$type),
                    'total_rows'    =>  $count_project,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            
            $where_param['order_by'] = array(
                $table.'.id' => 'ASC'
            );

            $project = $this->$func($where_param);
            if( $type == 'all' ) {
                foreach($project as $k => $item) {
                    $project_id = $item['project_id'];

                    // get latest history of project
                    $item['history'] = $this->_get_history_by_id($project_id, true);

                    $project[$k] = $item;
                }

                // get scale
                $data['project_scale'] = $this->project->getProject(PROJECT_SCALE);

                // get status
                $data['project_status'] = $this->project->getProject(PROJECT_STATUS);
            }
            $data['project'] = $project;
        }
        
        $this->load->view('panel/'.$file, $data);
    }
    
    // save new project data
    public function add($type='all') {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;

        // set table
        $table = PROJECT;
        $subject = 'project';
        $file = 'add-project';
        if( $type != 'all' ) {
            $table = PROJECT.'_'.$type;
            $subject = 'project '.$type;
            $file = 'add-'.$type;
        }
        
        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();
            $post_data = $this->_format_input_data($post_data);

            // validation
#---------------------------------------------------------------------------------------------------------------------#
            $this->load->library('form_validation');
            if( $type == 'project' ) {
                $validation_config = $this->project_validation_config;
            } elseif( $type == 'scale' ) {
                $validation_config = $this->scale_validation_config;
            } elseif( $type == 'status' ) {
                $validation_config = $this->status_validation_config;
            }
            if( isset($post_data['use_custom_gid']) ) {                 // if custom gid is chosen
                foreach($this->gid_validation as $key => $val) {        // add new rules to check existing gid
                    if( $key == 'rules' ) {                             // in database, must be unique
                        $val = $val.'|is_unique['.$table.'.gid]';
                    }
                    $this->gid_validation[$key] = $val;
                }

                $validation_config[] = $this->gid_validation;
            }

            $this->form_validation->set_rules($validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }
#---------------------------------------------------------------------------------------------------------------------#

            if ($this->form_validation->run() == TRUE ) {
                switch($type) {
                    case 'all':
                        // save general info data of project
                        $gid = $post_data['gid'];
//                        $date_start = $post_data['date_start'];
//                        $date_end = $post_data['date_end'];
                        $scale = $post_data['scale'];
                        $title = $post_data['title'];
                        $description = $post_data['description'];
                        $annotation = $this->_annotation($description);
                        $website = $post_data['website'];
                        
                        $save_param = array(
                            'gid' => $gid,
                            'date_add' => DATETIME,
//                            'date_start' => $date_start,
//                            'date_end' => $date_end,
                            'scale' => $scale,
                            'title' => $title,
                            'annotation' => $annotation,
                            'description' => $description,
                            'website' => $website
                        );
//                        if( !empty($date_start) ) {
//                            $save_param['date_start'] = $date_start;
//                        }
//                        if( !empty($date_end) ) {
//                            $save_param['date_end'] = $date_end;
//                        }

                        if( !empty($_FILES['image_name']['tmp_name']) ) {
                            $this->load->helper('uploads/uploads');

                            $source = $_FILES;
                            $uploaded_data = upload_image_to_temp('project', $source, $this->temp_path);
                        }

                        // if error occured
                        if( isset($uploaded_data) ) {
                            foreach($uploaded_data as $k => $value) {
                                switch($k) {
                                    case 'uploaded_data':
                                        $save = $this->project->setNewProject($table, $save_param);
                                        if( isset($save['id']) ) {
                                            $id = $save['id'];

                                            // create directory
                                            $img_path = $this->img_path.$id.'/';
                                            if( !file_exists($img_path) ) {
                                                mkdir($img_path);
                                                chmod($img_path, 0777);
                                            }

                                            // crop image, store in directory, update image_name in database
                                            $image_name = cropping_image('project', $value, $img_path, $title);
                                            $update_param = $image_name;

                                            $update = $this->project->updateProject($table, $update_param, $id);
                                            if( $update == 'SUCCESSFUL' ) {
                                                // save the contact
                                                $name = $post_data['name'];
                                                $phone = $post_data['phone'];
                                                $email = $post_data['email'];

                                                $contact_param = array(
                                                    'project_id' => $id,
                                                    'name' => $name,
                                                    'phone' => $phone,
                                                    'email' => $email
                                                );
                                                $this->project->setNewProject(PROJECT_CONTACT, $contact_param);

                                                // save the status being history
                                                $status = $post_data['project_status'];
                                                $note = $post_data['note'];
                                                $date_history = $post_data['date_history'];
                                                $history_param = array(
                                                    'date_history' => $date_history.' '.TIME,
                                                    'project_id' => $id,
                                                    'status_id' => $status,
                                                    'note' => $note
                                                );
                                                $this->project->setNewProject(PROJECT_HISTORY, $history_param);

                                                // save the location
                                                $building_name = $post_data['building_name'];
                                                $address = $post_data['address'];
                                                $province = $post_data['province'];
                                                $city = $post_data['city'];
                                                $district = $post_data['district'];
                                                
                                                $location_param = array(
                                                    'project_id' => $id,
                                                    'building_name' => $building_name,
                                                    'address' => $address,
                                                    'province_id' => $province,
                                                    'city_id' => $city,
                                                    'district_id' => $district
                                                );
                                                $this->project->setNewProject(PROJECT_LOCATION, $location_param);
                                                
                                                // set notification
                                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                            New project has been added with image.'.
                                                          '</div>';
                                                $this->session->set_flashdata('notif', $notif);

                                                // redirect to list general info page
                                                redirect('panel/project/all/detail/'.$id);
                                            }
                                        }
                                    break;
                                    case 'error_image':
                                        $data[$k] = array_shift($value);
                                        break;
                                }
                            }
                        } else {
                            $save = $this->project->setNewProject($table, $save_param);
                            if( isset($save['id']) ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            New project has been added without image.
                                          </div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to list general info page
                                redirect('panel/project/all/detail/'.$id);
                            }
                        } # end of checking uploaded data
                        
                        $save_param = $this->project->setNewProject($table, $save_param);
                        if( isset($save_param['id']) ) {
                            $id = $save_param['id'];
                        }
                        break;
                    case 'scale':
                    case 'status':
                        $gid = $post_data['gid'];
                        $name = ucfirst($post_data[$type.'_name']);

                        // save parameter
                        $save_param = array(
                            'gid' => $gid,
                            $type.'_name' => $name
                        );
                        $save = $this->project->setNewProject($table, $save_param);
                        if( isset($save['id']) ) {
                            $id = $save['id'];
                            
                            // set notification
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        New '.$type.' has been added.
                                      </div>';
                            $this->session->set_flashdata('notif', $notif);

                            // redirect to list general info page
                            redirect('panel/project/'.$type.'/detail/'.$id);
                        }
                        break;
                }
                
                $save = $this->project->setNewProject($save_param);
                if( isset($save['id']) ) {
                    $id = $save['id'];

                    // set notification
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                New project has been added.
                              </div>';
                    $this->session->set_flashdata('notif', $notif);

                    // redirect to list general info page
                    redirect('panel/project/'.$type.'/detail/'.$id);
                } # end of save id
            } # end of validation
        } # end of post

        if( $type == 'all' ) {
            // get project status list
            $data['project_status'] = $this->project->getProject(PROJECT_STATUS);

            // get project scale list
            $data['project_scale'] = $this->project->getProject(PROJECT_SCALE);

            $this->load->helper('area/area');
            // get province list
            $province_param['order_by'] = array('province_name' => 'ASC');
            $data['province'] = get_area('province', $province_param);
            
            // get city list
            $city_param['where'] = array('province_id' => 6);
            $city_param['order_by'] = array('city_name' => 'ASC');
            $data['city'] = get_area('city', $city_param);
            
            // get district list
            $district_param['where'] = array('city_id' => 58);
            $district_param['order_by'] = array('district_name' => 'ASC');
            $data['district'] = get_area('district', $district_param);
        }
//        print_r($data); exit;
        $this->load->view('panel/'.$file, $data);
    }

    // update user data, operated by admin
    public function detail($type='all', $id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // set table
        $table = PROJECT;
        $subject = 'project';
        $file = 'detail-project';
        if( $type != 'all' ) {
            $table = PROJECT.'_'.$type;
            $subject = 'project '.$type;
            $file = 'detail-'.$type;
        }
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $project = $this->project->getProjectById($table, $id);

            if( !empty($project) ) {
                if( $type == 'all' ) {
                    // date start and date end
//                    $date_start = '';
//                    if( $project['date_start'] != DEFAULT_DATETIME ) {
//                        $date_start = substr($project['date_start'], 0, -9);
//                    }
//                    $project['date_start'] = $date_start;
//                    $date_end = '';
//                    if( $project['date_end'] != DEFAULT_DATETIME ) {
//                        $date_end = substr($project['date_end'], 0, -9);
//                    }
//                    $project['date_end'] = $date_end;

                    // where param to get related data
                    $where_param['where'] = array(
                        'project_id' => $id
                    );

                    // get contact
                    $contact = $this->project->getProject(PROJECT_CONTACT, $where_param);
                    if( !empty($contact) ) {
                        $contact = array_shift($contact);
                    }
                    $project['contact'] = $contact;

                    // get latest history
                    $history_param['where'] = $where_param['where'];
                    $history_param['order_by'] = array('id' => 'DESC');
                    $history = $this->project->getProject(PROJECT_HISTORY, $history_param);
                    if( !empty($history) ) {
                        foreach ($history as $k => $item) {
                            // get status name
                            $status = $this->project->getProjectById(PROJECT_STATUS, $item['status_id']);
                            $item['status_name'] = $status['status_name'];
//                            if( $item['date_history'] != DEFAULT_DATETIME ) {
//                                $item['date_history'] = substr($item['date_history'], 0, -9);
//                            }
                            
                            $history[$k] = $item;
                        }
                    }
                    $project['history'] = $history;
                    
                    // get project location
                    $location = $this->project->getProject(PROJECT_LOCATION, $where_param);
                    if( !empty($location) ) {
                        $location = array_shift($location);
                    }
                    $project['location'] = $location;

                    // get gallery
                    $gallery = $this->project->getProject(PROJECT_GALLERY, $where_param);
                    if( !empty($gallery) ) {
                        $gallery = array_shift($gallery);
                    }
                    $project['gallery'] = $gallery;

                    // get project status list
                    $data['project_status'] = $this->project->getProject(PROJECT_STATUS);

                    // get project scale list
                    $data['project_scale'] = $this->project->getProject(PROJECT_SCALE);
                }// elseif($type == 'scale') {
//                    print_r($project); exit;
                //}

                if(!empty($_POST)) {
                    // get post data
                    $post_data = $this->input->post();
                    $post_data = $this->_format_input_data($post_data);
                    
                    if( $type == 'all' ) {
                        // for area
                        $project['location']['province_id'] = $post_data['province'];
                        $project['location']['city_id'] = $post_data['city'];
                    }

                    // validation
#-------------------------------------------------------------------------------------------------------------------------#
                    $this->load->library('form_validation');
                    if( $type == 'project' ) {
                        $validation_config = $this->project_validation_config;
                    } elseif( $type == 'scale' ) {
                        $validation_config = $this->scale_validation_config;
                    } else {
                        $validation_config = $this->status_validation_config;
                    }

                    if( isset($post_data['use_custom_gid']) ) {                     // if custom gid is chosen
                        foreach($this->gid_validation as $key => $val) {        // add new rules to check existing gid
                            if( $key == 'rules' ) {                             // in database, must be unique
                                $val = $val.'|edit_unique['.$table.'.gid.'.$post_data['gid'].']|is_unique['.$table.'.gid]';
                            }
                            $this->gid_validation[$key] = $val;
                        }

                        $validation_config[] = $this->gid_validation;
                    }

                    $this->form_validation->set_rules($validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }
#-------------------------------------------------------------------------------------------------------------------------#
                    
                    if ($this->form_validation->run() == TRUE ) {
                        switch($type) {
                            case 'all':
                                // general info data of project
                                $gid = $post_data['gid'];
                                $date_start = $post_data['date_start'];
                                $date_end = $post_data['date_end'];
                                $scale = $post_data['scale'];
                                $title = $post_data['title'];
                                $description = $post_data['description'];
                                $annotation = $this->_annotation($description);
                                $website = $post_data['website'];

                                $update_param = array(
                                    'gid' => $gid,
                                    'date_update' => DATETIME,
                                    'scale' => $scale,
                                    'title' => $title,
                                    'annotation' => $annotation,
                                    'description' => $description,
                                    'website' => $website
                                );
//                                if( !empty($date_start) ) {
//                                    $update_param['date_start'] = $date_start;
//                                }
//                                if( !empty($date_end) ) {
//                                    $update_param['date_end'] = $date_end;
//                                }

                                if( !empty($_FILES['image_name']['tmp_name']) ) {
                                    $this->load->helper('uploads/uploads');

                                    $source = $_FILES;
                                    $uploaded_data = upload_image_to_temp('project', $source, $this->temp_path);
                                }

                                // if error occured
                                if( isset($uploaded_data) ) {
                                    foreach($uploaded_data as $k => $value) {
                                        switch($k) {
                                            case 'uploaded_data':
                                                $save = $this->project->setNewProject($table, $save_param);
                                                if( isset($save['id']) ) {
                                                    $id = $save['id'];

                                                    // create directory
                                                    $img_path = $this->img_path.$id.'/';
                                                    if( !file_exists($img_path) ) {
                                                        mkdir($img_path);
                                                        chmod($img_path, 0777);
                                                    }

                                                    // crop image, store in directory, update image_name in database
                                                    $image_name = cropping_image('project', $value, $img_path, $title);
                                                    $update_param = $image_name;

                                                    $update = $this->project->updateProject($table, $update_param, $id);
                                                    if( $update == 'SUCCESSFUL' ) {
                                                        // update the contact
                                                        $name = $post_data['name'];
                                                        $phone = $post_data['phone'];
                                                        $email = $post_data['email'];

                                                        $contact_param = array(
                                                            'name' => $name,
                                                            'phone' => $phone,
                                                            'email' => $email
                                                        );
                                                        $this->project->updateProject(PROJECT_CONTACT, $contact_param, $where_param['where']);

                                                        // update the status being history
                                                        $status = $post_data['project_status'];
                                                        if( !empty($status) ) {
                                                            $note = $post_data['note'];
                                                            $date_history = $post_data['date_history'];
                                                            $history_param = array(
                                                                'date_history' => $date_history.' '.TIME,
                                                                'project_id' => $id,
                                                                'status_id' => $status,
                                                                'note' => $note
                                                            );
                                                            $this->project->setNewProject(PROJECT_HISTORY, $history_param);
                                                        }

                                                        // update the location
                                                        $building_name = $post_data['building_name'];
                                                        $address = $post_data['address'];
                                                        $province = $post_data['province'];
                                                        $city = $post_data['city'];
                                                        $district = $post_data['district'];

                                                        $location_param = array(
                                                            'building_name' => $building_name,
                                                            'address' => $address,
                                                            'province_id' => $province,
                                                            'city_id' => $city,
                                                            'district_id' => $district
                                                        );
                                                        $this->project->updateProject(PROJECT_LOCATION, $location_param, $where_param['where']);

                                                        // set notification
                                                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                    New project has been added with image.'.
                                                                  '</div>';
                                                        $this->session->set_flashdata('notif', $notif);

                                                        // redirect to list general info page
                                                        redirect('panel/project/all/detail/'.$id);
                                                    }
                                                }
                                            break;
                                            case 'error_image':
                                                $data[$k] = array_shift($value);
                                                break;
                                        }
                                    }
                                } else {
                                    $update = $this->project->updateProject($table, $update_param, $id);
                                    if( $update == 'SUCCESSFUL' ) {
                                        // update the contact
                                        $name = $post_data['name'];
                                        $phone = $post_data['phone'];
                                        $email = $post_data['email'];

                                        $contact_param = array(
                                            'name' => $name,
                                            'phone' => $phone,
                                            'email' => $email
                                        );
                                        $this->project->updateProject(PROJECT_CONTACT, $contact_param, $where_param['where']);

                                        // update the status being history
                                        $status = $post_data['project_status'];
                                        if( !empty($status) ) {
                                            $note = $post_data['note'];
                                            $date_history = $post_data['date_history'];
                                            $history_param = array(
                                                'date_history' => $date_history.' '.TIME,
                                                'project_id' => $id,
                                                'status_id' => $status,
                                                'note' => $note
                                            );
                                            $this->project->setNewProject(PROJECT_HISTORY, $history_param);
                                        }

                                        // update the location
                                        $building_name = $post_data['building_name'];
                                        $address = $post_data['address'];
                                        $province = $post_data['province'];
                                        $city = $post_data['city'];
                                        $district = $post_data['district'];

                                        $location_param = array(
                                            'building_name' => $building_name,
                                            'address' => $address,
                                            'province_id' => $province,
                                            'city_id' => $city,
                                            'district_id' => $district
                                        );
                                        $this->project->updateProject(PROJECT_LOCATION, $location_param, $where_param['where']);

                                        // set notification
                                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    Project has been updated.
                                                  </div>';
                                        $this->session->set_flashdata('notif', $notif);

                                        // redirect to list general info page
                                        redirect('panel/project/all/detail/'.$id);
                                    }
                                } # end of checking uploaded data
                                break;
                            case 'scale':
                            case 'status':
                                $gid = $post_data['gid'];
                                $name = ucfirst($post_data[$type.'_name']);

                                // update parameter
                                $update_param = array(
                                    'gid' => $gid,
                                    $type.'_name' => $name
                                );
                                $update = $this->project->updateProject($table, $update_param, $id);
                                if( $update == 'SUCCESSFUL' ) {
                                    // set notification
                                    $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                '.ucfirst($type).' with name <b>'.$post_data['gid_'.$type].'</b> has been updated.'.
                                              '</div>';
                                    $this->session->set_flashdata('notif', $notif);

                                    // redirect to list general info page
                                    redirect('panel/project/'.$type.'/detail/'.$id);
                                }
                                break;
                        } # end of switch type
                    } # end of validation
                } # end of post
                
                // get another data
                if( $type == 'all' ) {
                    $province_id = $project['location']['province_id'];
                    $city_id = $project['location']['city_id'];

                    $this->load->helper('area/area');
                    // get province list
                    $province_param['order_by'] = array('province_name' => 'ASC');
                    $data['province'] = get_area('province', $province_param);

                    // get city list
                    $city_param['where'] = array('province_id' => $province_id);
                    $city_param['order_by'] = array('city_name' => 'ASC');
                    $data['city'] = get_area('city', $city_param);

                    // get district list
                    $district_param['where'] = array('city_id' => $city_id);
                    $district_param['order_by'] = array('district_name' => 'ASC');
                    $data['district'] = get_area('district', $district_param);
                }
                
                $data['data'] = $project;
            }
        }
//        print_r($data['data']); exit;

        $this->load->view('panel/'.$file, $data);
    }

    // delete project data, include of image
    public function delete($type='all', $id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;

        // set table
        $table = PROJECT;
        $subject = 'project';
        $name = 'title';
        if( $type != 'all' ) {
            $table = PROJECT.'_'.$type;
            $subject = 'project '.$type;
            $name = $type.'_name';
        }
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting project. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && $id != 0 && is_numeric($id) ) {
            // check if given ID is existing in database
            $project = $this->project->getProjectById($table, $id);
            // if data is found
            if( !empty($project) ) {
                // run delete action
                $delete = $this->project->deleteProject($table, $id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    if( $type == 'all' ) {
                        $img_name = $project['image_name'];
                        $where_param = array(
                            'project_id' => $id
                        );
                        
                        $img_path = $this->img_path.$id.'/';
                        $gallery_path = $img_path.'/gallery/';
                        $glob = glob($img_path.'*'.$img_name);
                        $glob_gallery = glob($gallery_path.'*');
                        
                        if( !empty($glob_gallery) ) {                   // remove gallery
                            foreach($glob_gallery as $k => $item) {
                                unlink($item);                          // delete images
                            }
                            rmdir($gallery_path);                       // delete directory
                        }
                        if( !empty($glob) ) {                           // remove main image
                            foreach($glob as $k => $item) {
                                unlink($item);                          // delete images
                            }
                            rmdir($img_path);                           // delete directory
                        }
                        
                        $this->project->deleteProject(PROJECT_GALLERY, $where_param);   // delete data from gallery table in db
                        $this->project->deleteProject(PROJECT_HISTORY, $where_param);   // delete history
                        $this->project->deleteProject(PROJECT_LOCATION, $where_param);  // delete location
                        $this->project->deleteProject(PROJECT_CONTACT, $where_param);   // delete contact
                    }
                    
                    // provide success message
                    $msg = ucfirst($subject).' with name <b>'.$project[$name].'</b> has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/project/'.$type);
    }
    
    // check datetime format
    public function checkDateFormat($datetime) {
        $data = explode(' ', $datetime);
//        $data = explode('%20', $datetime);
        $match_date = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$data[0]);
        $match_time = preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])/", $data[1]);
        if( $match_date && $match_time ) {
            return true;
//            echo 'true';
        } else {
            return false;
//            echo 'false';
        }
    }
}
