<?php echo call_header('panel', 'Detail Activity'); ?>

<?php echo call_sidebar('activity'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-8">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <div class="form-group">
                                    <a href="<?php echo site_url('panel/activity'); ?>" class="btn btn-default waves-effect">BACK</a>
                                </div>

                                <?php if( isset($data) ) { ?>
                                    <form action="<?php echo site_url('panel/activity/detail/'.$data['activity_id']); ?>" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <div class="switch">
                                                <?php
                                                    $checked = '';
                                                    if( $activity_status == 1 ) {
                                                        $checked = 'checked';
                                                    }
                                                ?>
                                                <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Source</label>
                                            <?php echo form_error('source'); ?>
                                            <div class="form-line">
                                                <select class="form-control" name="source">
                                                    <option value="">- Choose -</option>
                                                    <?php
                                                        foreach($source as $key => $item) {
                                                            $selected = '';
                                                            if( $item['source_gid'] == $data['source'] ) {
                                                                $selected = 'selected';
                                                            }
                                                            
                                                            echo '<option value="'.$item['source_gid'].'" '.$selected.'>'.$item['source_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Caption</label>
                                            <?php echo form_error('activity_caption'); ?>
                                            <div class="form-line">
                                                <textarea rows="8" class="form-control no-resize" name="activity_caption"><?php echo $data['activity_caption']; ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Image</label>
                                            <?php if(isset($error_image)) echo $error_image; ?>
                                            <div class="form-line">
                                                <?php
                                                    $img_name = $data['activity_image'];
                                                    $image = base_url(IMG_PATH.'activity/'.$data['activity_id'].'/'.$img_name);
                                                ?>
                                                <input type="file" name="activity_image" id="imageInput" class="form-control">
                                                <input type="hidden" name="activity_image" value="<?php echo $img_name; ?>">
                                            </div>
                                            <small class="rules">File size should be equal or less than 2 MB, with minimum size 640 x 640 px</small>
                                            <br><br>
                                            <img class="img-responsive img-thumbnail" src="<?php echo $image; ?>" width="200">
                                        </div>

                                        <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 pull-right">UPDATE</button>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
