<?php echo call_header('panel', 'Add New Activity'); ?>

<?php echo call_sidebar('activity'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-8 col-xs-12">
                                <form action="<?php echo site_url('panel/activity/add'); ?>" method="post" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <div class="switch">
                                            <?php
                                                $checked = '';
                                                if( $activity_status == 1 ) {
                                                    $checked = 'checked';
                                                }
                                            ?>
                                            <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Source</label>
                                        <?php echo form_error('source'); ?>
                                        <div class="form-line">
                                            <select class="form-control" name="source">
                                                <option value="">- Choose -</option>
                                                <?php
                                                    $selected = '';
                                                    foreach($source as $key => $item) {
                                                        if( $item['source_gid'] == set_value('source') ) {
                                                            $selected = 'selected';
                                                        }

                                                        echo '<option value="'.$item['source_gid'].'" '.$selected.'>'.$item['source_name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Caption</label>
                                        <?php echo form_error('activity_caption'); ?>
                                        <div class="form-line">
                                            <textarea rows="8" class="form-control no-resize" name="activity_caption"><?php echo set_value('activity_caption'); ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Image</label>
                                        <?php if(isset($error_image)) echo $error_image; ?>
                                        <div class="form-line">
                                            <input type="file" name="activity_image" id="imageInput" class="form-control">
                                        </div>
                                        <small class="rules">File size should be equal or less than 2 MB, with minimum size 640 x 640 px</small>
                                    </div>

                                    <div class="form-group">                                        
                                        <a href="<?php echo site_url('panel/activity'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right m-r-10" value="SAVE">
                                    </div>
                                </form>                                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<script>
    $(document).on('click', '#toggleStatus', function() {
        if( $(this).is(':checked') ) {
            $('#status').val('1');
        } else {
            $('#status').val('0');
        }
    });
</script>