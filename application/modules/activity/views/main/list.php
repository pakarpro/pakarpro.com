<?php echo call_header('main', 'activity'); ?>

    <section id="activity">
        <div  class="container">
            <div class="center">
                <h2>Activities / Updates</h2>
            </div>

            <div class="row">
                <div class="activity-items">
                    <?php if( isset($activity) && !empty($activity) ) { ?>
                        <div class="row">
                            <?php
                                $no = 1;
                                foreach($activity as $k => $item) {
                                    $id = $item['activity_id'];
                                    // image
                                    $img_url = base_url(IMG_PATH.'activity/'.$id.'/small-'.$item['activity_image']);
                            ?>
                                <div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                                    <div class="feature-wrap">
                                        <a class="_da" data-id="<?php echo $id; ?>" href="#" data-toggle="modal" data-target="#detailActivity">
                                            <img class="img-responsive img-thumbnail" src="<?php echo $img_url; ?>" width="100%">
                                        </a>
                                        <h3><?php echo $item['activity_caption']; ?></h3>
                                    </div>
                                </div><!--/.col-md-4-->
                            <?php
                                    if( $no % 3 == 0 ) {
                                        echo '</div>';
                                        echo '<div class="row">';
                                    }

                                    $no++;
                                }
                            ?>
                        </div>
                        <div class="more-data"></div>
                    <?php } else { ?>
                        <h2>
                            Belum ada aktivitas terbaru.
                            Kembali ke <a href="<?php echo site_url(); ?>">halaman utama</a>.
                        </h2>
                    <?php } ?>

                    <?php if( $total > 12 ) { ?>
                        <div class="row">
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-primary btn-load-more">Load More</button>
                            </div>
                        </div>
                    <?php } ?>
                </div><!--/.activity-items-->
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#blog-->

    <!-- Modal for activities -->
    <div class="modal fade" id="detailActivity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Activity</h5>
                </div>
                <div class="modal-body activity-caption"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-close-modal btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
<?php echo call_footer('main'); ?>
<script>
    // for modal purpose
    $(document).on('click', '._da', function(){
        var image = $(this).children('img').attr('src');
        var caption = $(this).next('h3').text();

        $('#detailActivity').on('shown.bs.modal', function() {
            $('.activity-caption').html(
                '<img class="img-responsive img-thumbnail" src="'+image.replace('small-','')+'">'+
                '<h3 style="word-break: break-all;">'+caption+'</h3>'
            );
        })
        
        $(document).on('click', '.btn-close-modal', function(){
            $('.activity-caption').html('');
        });
    });
    
    // for load more purpose
    var i = 0;
    var current_page = 2;
    var fetch_lock = false;
    var fetch_page = function() {
        if(fetch_lock) return;
        fetch_lock = true;
        $.get('/activity/load', {page_index: current_page}, function(data) {
            var obj = JSON.parse(data);
            var count = obj.length;
            
            $('.more-data').append('<div class="row">');
                for(i; i < count; i++) {
                    $('.more-data').append(
                        '<div class="col-md-4 col-sm-6 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">'+
                            '<div class="feature-wrap">'+
                                '<a class="_da" data-id="'+obj[i].activity_id+'" href="#" data-toggle="modal" data-target="#detailActivity">'+
                                    '<img class="img-responsive img-thumbnail" src="'+obj[i].activity_image+'" width="100%">'+
                                '</a>'+
                                '<h3>'+obj[i].activity_caption+'</h3>'+
                            '</div>'+
                        '</div>'
                    );
                    
                    if( parseInt(i+1) % 3 == 0 ) {
                        $('.more-data').append(
                            '<div class="row">'+
                            '</div>'
                        );
                    }
                }
            $('.more-data').append('</div>');

            current_page += 1;
            if(count == 0) {
                // hide the `more` tag, show that there are no more data.
                // do not disable the lock in this case.
                $('.btn-load-more').parent().parent().remove();
            } else {
                fetch_lock = false;
            }
        });
    }
    
//    fetch_page();
    $('.btn-load-more').click(fetch_page);
</script>