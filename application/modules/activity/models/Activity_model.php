<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Activity_model extends CI_model {
    
    public $data;
    public $return;
    private $activities;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
        $this->return = array();
        $this->activities = array(
            'activity_id',
            'date_activity',
            'source',
            'activity_image',
            'activity_caption',
            'activity_status'
        );
    }
    
#=================================== CREATE ===================================#
    public function setNewActivity($data) {
        $params['table'] = ACTIVITIES;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getActivity($params=array()) {
        // fields
        $fields = $this->activities;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        
        $params['fields'] = $fields;
        $params['table'] = ACTIVITIES;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getActivityById($id) {
        $params['fields'] = $this->activities;
        $params['table'] = ACTIVITIES;
        $params['where'] = array(
            'activity_id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countActivity() {
        $params['table'] = ACTIVITIES;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateActivity($data, $activityId) {
        $params['table'] = ACTIVITIES;
        $params['data'] = $data;
        $params['where'] = array(
            'activity_id' => $activityId
        );
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteActivity($activityId) {
        $params['table'] = ACTIVITIES;
        $params['where'] = array(
            'activity_id' => $activityId
        );
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}