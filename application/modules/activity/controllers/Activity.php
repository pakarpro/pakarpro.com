<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activity extends CI_Controller {
    
    private $CI;
    private $activity;
    private $activity_status = 1;
    private $page = 1;
    private $redirect = 'panel/login';
    private $img_path;
    private $temp_path;
    private $admin_data = array();
    private $validation_config = array(
        array(
            'field'   => 'source',
            'label'   => 'Source',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'activity_caption', 
            'label'   => 'Caption', 
            'rules'   => 'trim|required'
        )
    );
    private $validation_msg = array(
        'required' => '%s is required'
    );

    public function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('form');
        $this->load->model('Activity_model');
        $this->activity = new Activity_model();
        
        // image path
        $this->img_path = FCPATH.IMG_PATH.'activity/';
        $this->temp_path = FCPATH.IMG_PATH.'temp/activity/';

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
    
    // updating status, for ajax use
    public function changestatus() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $status = $data['status'];
            
            $update_param = array(
                'date_update' => DATETIME,
                'activity_status' => $status
            );
            $update = $this->activity->updateActivity($update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for main page
     * -------------------------------------------------------------------------
     */
    public function index($param='') {
        if( !empty($param) ) {
            $this->page = $param;
        }

        $count_activity = $this->activity->countActivity();
        if( $count_activity > 0 ) {
            // set pagination
            $limit_per_page = 12;
            $start_row = ($this->page-1) * $limit_per_page;
            
            // where param
            $activity_param['limit'] = array($limit_per_page, $start_row);
            $activity_param['order_by'] = array(
                'date_add' => 'ASC'
            );
            $activity = $this->activity->getActivity($activity_param);
            foreach($activity as $k => $item) {
                $annotation = strip_tags($item['activity_caption']);

                $activity[$k] = $item;
            }

            $data['activity'] = $activity;
            $data['total'] = $count_activity;
        }

        $this->load->view('main/list', $data);
    }
    
    // for ajax use
    public function load() {
        // set pagination
        $limit_per_page = 12;
        $this->page = intval($this->input->get('page_index'));
        
        $start_row = ($this->page-1) * $limit_per_page;

        // where param
        $activity_param['limit'] = array($limit_per_page, $start_row);
        $activity_param['order_by'] = array(
            'date_add' => 'ASC'
        );
        $activity = $this->activity->getActivity($activity_param);
        foreach($activity as $k => $item) {
            $img_url = base_url(IMG_PATH.'activity/'.$item['activity_id'].'/small-'.$item['activity_image']);
            $item['activity_image'] = $img_url;
            $activity[$k] = $item;
        }
        
        echo json_encode($activity);
    }
    
    /*
     * -------------------------------------------------------------------------
     * This section is for panel page
     * -------------------------------------------------------------------------
     */
    
    // list all activities data
    public function lists($page=1) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        $this->load->helper('activity');
        $count_activity = count_activity('instagram');
        if( $count_activity > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            if( $count_activity > $limit_per_page ) {
                // additional where param
                $where_param['limit'] = array($limit_per_page, $start_row);
                
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/activity'),
                    'total_rows'    =>  $count_activity,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            $where_param['order_by'] = array(
                'date_activity' => 'DESC'
            );
            $data['data'] = $this->activity->getActivity($where_param);
        }
        
        $this->load->view('panel/list', $data);
    }
    
    // save new activity data
    public function add() {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;

        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();
            $this->activity_status = format_status($post_data);

            // validation
            $this->load->library('form_validation');
            $this->form_validation->set_rules($this->validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $source = $post_data['source'];
                $caption = ucfirst($post_data['activity_caption']);
                
                // save parameter
                $save_param = array(
                    'date_add' => DATETIME,
                    'date_activity' => DATETIME,
                    'source' => $source,
                    'activity_caption' => $caption,
                    'activity_status' => $this->activity_status,
                );

                // if image uploaded
                if( !empty($_FILES['activity_image']['tmp_name']) ) {
                    $this->load->helper('uploads/uploads');
                    
                    $source = $_FILES;
                    $uploaded_data = upload_image_to_temp('activity', $source, $this->temp_path);
                }

                // if error occured
                if( isset($uploaded_data) ) {
                    foreach($uploaded_data as $k => $value) {
                        switch($k) {
                            case 'uploaded_data':
                                $save = $this->activity->setNewActivity($save_param);
                                if( isset($save['id']) ) {
                                    $id = $save['id'];
                                    
                                    // create directory
                                    $img_path = $this->img_path.$id.'/';
                                    if( !file_exists($img_path) ) {
                                        mkdir($img_path);
                                        chmod($img_path, 0777);
                                    }

                                    // crop image, store in directory, update image_name in database
                                    $image_name = cropping_image('activity', $value, $img_path, time());
                                    
                                    // update image name in database
                                    $update_param = $image_name;
                                    $update = $this->activity->updateActivity($update_param, $id);
                                    if( $update == 'SUCCESSFUL' ) {
                                        // set notification
                                        $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                    New activity has been added.
                                                  </div>';
                                        $this->session->set_flashdata('notif', $notif);

                                        // redirect to list general info page
                                        redirect('panel/activity/detail/'.$id);
                                    }
                                }
                                break;
                            case 'error_image':
                                $data[$k] = array_shift($value);
                                break;
                        }
                    }
                }
            }
        }

        $data['activity_status'] = $this->activity_status;
        $data['source'] = array(
            array(
                'source_gid' => 'internal',
                'source_name' => 'Internal',
            ),
            array(
                'source_gid' => 'instagram',
                'source_name' => 'Instagram',
            )
        );
        $this->load->view('panel/add', $data);
    }
    
    // update activity data, with image or not
    public function detail($id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            // check ID in database
            $detail_activity = $this->activity->getActivityById($id);
            if( !empty($detail_activity) ) {
                $this->activity_status = $detail_activity['activity_status'];
                if(!empty($_POST)) {
                    $post_data = $this->input->post();
                    $this->activity_status = format_status($post_data);
                    $detail_activity = array_merge($detail_activity, $post_data);

                    $this->load->library('form_validation');
                    $this->form_validation->set_rules($this->validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }

                    if ($this->form_validation->run() == TRUE ) {
                        $source = strtolower($post_data['source']);
                        $caption = ucfirst($post_data['activity_caption']);
                        $status = $this->activity_status;
                        $image_name = $post_data['activity_image'];

                        // update parameter
                        $update_param = array(
                            'date_update' => DATETIME,
                            'source' => $source,
                            'activity_caption' => $caption,
                            'activity_status' => $status
                        );

                        if( !empty($_FILES['activity_image']['tmp_name']) ) {
                            $this->load->helper('uploads/uploads');

                            $source = $_FILES;
                            $uploaded_data = upload_image_to_temp('activity', $source, $this->temp_path);
                        }

                        // if error occured
                        if( isset($uploaded_data) ) {
                            foreach($uploaded_data as $k => $value) {
                                switch($k) {
                                    case 'uploaded_data':
                                        // create directory
                                        $img_path = $this->img_path.$id.'/';
                                        if( !file_exists($img_path) ) {
                                            mkdir($img_path);
                                            chmod($img_path, 0777);
                                        }
                                        
                                        // unlink old photo
                                        $glob = glob($this->img_path.$id.'/*'.$image_name);
                                        if( !empty($glob) ) {
                                            foreach($glob as $item) {
                                                unlink($item);
                                            }
                                        }

                                        // crop image, store in directory, update image_name in database
                                        $image_name = cropping_image('activity', $value, $img_path, time());
                                        $update_param = array_merge($update_param, $image_name);

                                        $update = $this->activity->updateActivity($update_param, $id);
                                        if( $update == 'SUCCESSFUL' ) {
                                            // set notification
                                            $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                                        Activity has been updated with image.
                                                      </div>';
                                            $this->session->set_flashdata('notif', $notif);
                                        }

                                        // redirect to
                                        redirect('panel/activity/detail/'.$id);
                                    break;
                                    case 'error_image':
                                        $data[$k] = array_shift($value);
                                        break;
                                }
                            }
                        } else {
                            $update = $this->activity->updateActivity($update_param, $id);

                            if( $update == 'SUCCESSFUL' ) {
                                // set notification
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                            Activity has been updated without image.
                                          </div>';
                                $this->session->set_flashdata('notif', $notif);

                                // redirect to list general info page
                                redirect('panel/activity/detail/'.$id);
                            }
                        }
                    } # end of validation
                } # end of post

                $data['data'] = $detail_activity;
                $data['activity_status'] = $this->activity_status;
                $data['source'] = array(
                    array(
                        'source_gid' => 'internal',
                        'source_name' => 'Internal',
                    ),
                    array(
                        'source_gid' => 'instagram',
                        'source_name' => 'Instagram',
                    )
                );
            } # end of empty data
        }

        $this->load->view('panel/detail', $data);
    }
    
    // delete activity data, include of image
    public function delete($id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting activity. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            // check if given ID is existing in database
            $activity = $this->activity->getActivityById($id);
            // if data is found
            if( !empty($activity) ) {
                // set image name
                $img_name = $activity['activity_image'];
                // full path
                $path = $this->img_path.$id.'/';
                // delete the image
                $glob = glob($path.'*'.$img_name);
                foreach($glob as $file) {
                    unlink($file);
                }
                // delete the directory
                rmdir($path);

                // run delete action
                $delete = $this->activity->deleteActivity($id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    // provide success message
                    $msg = 'Activity with ID <b>#'.$activity['activity_id'].'</b> has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/activity');
    }
}