<?php

// to be displayed in homepage
if( !function_exists('get_active_activity') ){
    function get_active_activity(){
        $CI = &get_instance();

        $CI->load->model('activity/Activity_model');
        $activity = new Activity_model();
        
        $param = array(
            'fields' => array(
                'id AS activity_id',
                'title',
                'caption',
                'image_name'
            ),
            'where' => array(
                'status' => 1
            ),
            'order_by' => array(
                'date_add' => 'DESC'
            )
        );
        $data = $activity->getActivity($param);

        return $data;
    }
}

// count activity
if(!function_exists('count_activity')){
    function count_activity($type='all', $active=false){
        $CI = &get_instance();
        $CI->load->model('activity/Activity_model');
        
        $param = array();
        if( $type != 'all' ) {
            $param['where'] = array('source' => $type);
        }
        if( $active == true ) {
            $param['where'] = array('status' => 1);
        }
        
        $activity = new Activity_model();
        $data = $activity->countActivity($param);

        return $data;
    }
}
