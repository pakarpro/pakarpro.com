<?php

/* 
 * Name             : get_office
 * Parameters       : int or string $param, default empty
 * Functionality    : get office data
 */
if(!function_exists('get_office')){
    function get_office($param='') {
        $CI = &get_instance();
        
        $CI->load->model('office/Office_model');
        $office = new Office_model();
        
        $func = 'getOffice';
        if( !empty($param) ) {
            if( is_numeric($param) ) {
                $func = 'getOfficeById';     // id
            } elseif( !is_array($param) ) {
                $func = 'getOfficeByGid';    // gid
            }
        }
        
        $data = $office->$func($param);
        
        return $data;
    }
}

/* 
 * Name             : get_active_office
 * Parameters       : int or string $param, default empty
 * Functionality    : to be displayed in contact page
 */
if( !function_exists('get_active_office') ){
    function get_active_office(){
        $CI = &get_instance();

        $CI->load->model('office/Office_model');
        $office = new Office_model();
        
        $param = array(
            'fields' => array(
                'id AS office_id',
                'type',
                'building_name',
                'address',
                'province_id as province',
                'city_id as city',
                'district_id as district',
                'phone',
                'email'
            ),
            'where' => array(
                'status' => 1
            )
        );
        $data = $office->getOffice($param);

        return $data;
    }
}
