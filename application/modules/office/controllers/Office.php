<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Office extends CI_Controller {
    
    public $CI;
    
    public function __construct() {
        parent::__construct();
        
        $this->CI = &get_instance();
        
        $this->load->helper('office/office');
    }
}
