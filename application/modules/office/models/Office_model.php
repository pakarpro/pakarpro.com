<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Office_model extends CI_model {
    
    private $data;
    private $return;
    private $office;
    
    public function __construct() {
        parent::__construct();
        
        $this->return = array();
        
        $this->office = array(
            'id AS office_id',
            'date_add',
            'date_update',
            'id AS office_id',
            'type',
            'building_name',
            'address',
            'province_id as province',
            'city_id as city',
            'district_id as district',
            'phone',
            'email',
            'status AS office_status'
        );

        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
    public function saveOffice($data) {
        $params['table'] = OFFICE;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function countOffice() {
        $params['table'] = OFFICE;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getOffice($params=array()) {
        // fields
        $fields = $this->office;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['fields'] = $fields;
        $params['table'] = OFFICE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getOfficeById($id) {
        $fields = $this->office;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['table'] = OFFICE;
        $params['where'] = array(
            'id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getOfficeByType($type) {
        $fields = $this->office;
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        $params['table'] = OFFICE;
        $params['where'] = array(
            'type' => $type
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateOffice($data, $where) {
        $params['table'] = OFFICE;
        $params['data'] = $data;
        $params['where'] = $where;

        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteOffice($where) {
        $params['table'] = OFFICE;
        $params['where'] = $where;
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}