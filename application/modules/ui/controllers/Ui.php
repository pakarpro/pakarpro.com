<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ui extends CI_Controller {
    
    public $CI;
    public $redirect = '';
    public $user_data = array();

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        $this->load->helper('general');
        $this->load->helper('cookie');
        $this->load->model('Ui_model');
         
        if( $this->session->userdata('logged_in') ) {
            $this->user_data = $this->session->userdata('logged_in');
        }
    }
    
    public function change_language($lang){
        $expire = time() + (60*60*24*30*12);
        setcookie('tp_lang', $lang, $expire, '/');
        $rdr = $_GET['rdr'];
        if(!empty($rdr))
        {
            redirect(site_url($rdr));
        }
        else
        {
            redirect(site_url());
        }
        
    }   
    
    public function update_lang($id)
    {
        // check session login
        if( empty($this->user_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('admin'.$this->redirect);
        }
        $data['user_data'] = $this->user_data;
        
        if(!empty($_POST['ajax_post_data']))
        {
            $ajax_post_data = array();
            parse_str($_POST['ajax_post_data'], $ajax_post_data);
            $_POST = $ajax_post_data;
        }
        if(!empty($_POST))
        {
            $this->load->library('form_validation');
            $config = array(                
                array(
                    'field'   => 'lang_1', 
                    'label'   => 'Language 1', 
                    'rules'   => 'trim|required'
                ),
                array(
                    'field'   => 'lang_2', 
                    'label'   => 'Language 2', 
                    'rules'   => 'trim|required'
                )
            );
            $this->form_validation->set_rules($config);                
            $this->form_validation->set_message('required', '<font color="red"><br/>%s is required!</font>'); 
            if ($this->form_validation->run() == TRUE)
            {                    
                $data['parameters'] = array(
                    'id'        => $id,
                    'lang_1'    => $_POST['lang_1'],
                    'lang_2'    => $_POST['lang_2'],
                );
                $data['where'] = array(
                    'id'    => $id
                );
                $query = $this->Ui_model->update_lang($data);
                
                $json_array = json_encode($data['parameters']);
                print_r($json_array);
            }
            else 
            {
                print_r(2);
            }
        }
    }
}
?>