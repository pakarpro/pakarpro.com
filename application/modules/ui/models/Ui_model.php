<?php


if (!defined('BASEPATH')) exit('No direct script access allowed');


class Ui_model extends CI_model
{
    //lang section
    public function add_lang($data)
    {
        $this->db->insert('tp_lang', $data['parameters']);
        $insert_id['id'] = $this->db->insert_id();

        return $insert_id;
    }
    
    public function count_lang()
    {
        $this->db->select('*');
        $this->db->from('tp_general_info');

        $query = $this->db->get();

        if($query->num_rows()!=0)
        {
          return $query->num_rows();
        }
        else
        {
          return 0;
        }	 
    }
    
    public function get_lang($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_lang');
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k, $v);
            }
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $fields => $value) {
                $this->db->join($fields, $value);
            }
        }
        
        if(isset($data['like']))
        {
            foreach($data['like'] AS $k=>$v)
            {
                $this->db->like($k, $v);
            }
        }
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function list_lang($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_lang');
        
        // limit
        if(isset($data['limit']))
        {
            $this->db->limit($data['limit']['per_page'], $data['limit']['start']);
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $k => $val) {
                $this->db->join($k, $val);
            }
        }
        // like
        if(isset($data['like'])) {
            foreach($data['like'] as $k => $val) {
                $this->db->like($k, $val);
            }
        }
        // where
        if(isset($data['where'])) {
            foreach($data['where'] as $k => $val) {
                if(!is_array($val))
                {
                    $this->db->where($k, $val);
                }
                else
                {
                    foreach($val as $i => $j) {
                        $this->db->where($i, $j);
                    }
                }
            }
        }
        
        //or where
        if(isset($data['or_where'])) {
            foreach($data['or_where'] as $k => $val) {
                if(!is_array($val))
                {
                    $this->db->or_where($k, $val);
                }
                else
                {
                    foreach($val as $i => $j) {
                        $this->db->or_where($i, $j);
                    }
                }
            }
        }
        
        
        // order_by
        if(isset($data['order_by'])) {
            foreach($data['order_by'] as $k => $val) {
                $this->db->order_by($k, $val);
            }
        }
        
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function update_lang($data)
    {
        if(isset($data['parameters']))
        {
            foreach($data['parameters'] AS $k=>$v)
            {
                $this->db->set($k,$v);
            }
        }
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        $this->db->update('tp_lang');
    }

    public function delete_lang($data)
    {
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        $this->db->delete('tp_lang');
    }
    
    //menu section
    public function add_menu($data)
    {
        $this->db->insert('tp_menu', $data['parameters']);
        $insert_id['id'] = $this->db->insert_id();

        return $insert_id;
    }
    
    public function count_menu()
    {
        $this->db->select('*');
        $this->db->from('tp_menu');

        $query = $this->db->get();

        if($query->num_rows()!=0)
        {
          return $query->num_rows();
        }
        else
        {
          return 0;
        }	 
    }
    
    public function get_menu($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_menu');
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k, $v);
            }
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $fields => $value) {
                $this->db->join($fields, $value);
            }
        }
        
        if(isset($data['like']))
        {
            foreach($data['like'] AS $k=>$v)
            {
                $this->db->like($k, $v);
            }
        }
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function list_menu($data){
        // fields
        $fields = '*';
        if( isset($data['fields']) ) {
            $fields = implode(',', $data['fields']);
        }
        $this->db->select($fields);
        
        $this->db->from('tp_menu');
        
        // limit
        if(isset($data['limit']))
        {
            $this->db->limit($data['limit']['per_page'], $data['limit']['start']);
        }
        
        // join
        if(isset($data['join'])) {
            foreach($data['join'] as $k => $val) {
                $this->db->join($k, $val);
            }
        }
        // like
        if(isset($data['like'])) {
            foreach($data['like'] as $k => $val) {
                $this->db->like($k, $val);
            }
        }
        // where
        if(isset($data['where'])) {
            foreach($data['where'] as $k => $val) {
                if(!is_array($val))
                {
                    $this->db->where($k, $val);
                }
                else
                {
                    foreach($val as $i => $j) {
                        $this->db->where($i, $j);
                    }
                }
            }
        }
        
        //or where
        if(isset($data['or_where'])) {
            foreach($data['or_where'] as $k => $val) {
                if(!is_array($val))
                {
                    $this->db->or_where($k, $val);
                }
                else
                {
                    foreach($val as $i => $j) {
                        $this->db->or_where($i, $j);
                    }
                }
            }
        }
        
        
        // order_by
        if(isset($data['order_by'])) {
            foreach($data['order_by'] as $k => $val) {
                $this->db->order_by($k, $val);
            }
        }
        
        $query = $this->db->get();
        if($query->num_rows()>= 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
    
    public function update_menu($data)
    {
        if(isset($data['parameters']))
        {
            foreach($data['parameters'] AS $k=>$v)
            {
                $this->db->set($k,$v);
            }
        }
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        $this->db->update('tp_menu');
    }

    public function delete_menu($data)
    {
        if(isset($data['where']))
        {
            foreach($data['where'] AS $k=>$v)
            {
                $this->db->where($k,$v);
            }
        }
        $this->db->delete('tp_menu');
    }
}