<?php

//lang sections
if(!function_exists('list_lang')){
    function list_lang($data){
        $CI = &get_instance();
        $CI->load->model('ui/Ui_model');
        
        $lang_list = $CI->Ui_model->list_lang($data);
        return $lang_list;
    }
}
if(!function_exists('count_list_lang')){
    function count_list_lang($data){
        $CI = &get_instance();
        $CI->load->model('ui/Ui_model');
        
        $count_list_lang = $CI->Ui_model->count_lang($data);
        return $count_list_lang;
    }
}
if(!function_exists('get_lang')){
    function get_lang($data){
        $CI = &get_instance();
        $CI->load->model('ui/Ui_model');
        $get_lang = $CI->Ui_model->get_lang($data);
        return $get_lang;
    }
}

//lang sections
if(!function_exists('list_menu')){
    function list_menu($data){
        $CI = &get_instance();
        $CI->load->model('ui/Ui_model');
        
        $list_menu = $CI->Ui_model->list_menu($data);
        return $list_menu;
    }
}
if(!function_exists('count_list_menu')){
    function count_list_menu(){
        $CI = &get_instance();
        $CI->load->model('ui/Ui_model');
        
        $count_list_menu = $CI->Ui_model->count_list_menu();
        return $count_list_menu;
    }
}
if(!function_exists('get_menu')){
    function get_menu($data){
        $CI = &get_instance();
        $CI->load->model('ui/Ui_model');
        $get_menu = $CI->Ui_model->get_menu($data);
        return $get_menu;
    }
}
