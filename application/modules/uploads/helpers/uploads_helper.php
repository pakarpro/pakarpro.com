<?php

// for uploading image to the temporary pth
if( !function_exists('upload_image') ) {
    function upload_image_to_temp($module, $source, $destination) {
        $CI = &get_instance();
        
        // get upload config
        $config = get_uploads_config($module);
        $config['file_formats'] = unserialize($config['file_formats']);

        // set temporary path
        if (!file_exists($destination)) {
            mkdir($destination);
            chmod($destination, 0777);
        }
        
        // upload process to the temp directory
        $CI->load->library('upload');
        foreach($source as $k => $val) {
//            echo $k; exit;
            if( !empty($val['tmp_name']) ) {
                if( is_array($val['tmp_name']) ) {
//                    print_r($val); exit;
                    $count = count($val['tmp_name']);
//                    echo $count; exit;
                    for($i = 0; $i < $count; $i++){
                        $_FILES['image_name']['name'] = $val['name'][$i];
                        $_FILES['image_name']['type'] = $val['type'][$i];
                        $_FILES['image_name']['tmp_name'] = $val['tmp_name'][$i];
                        $_FILES['image_name']['size'] = $val['size'][$i];
                        $_FILES['image_name']['error'] = $val['error'][$i];
                        
//                        $name = strtolower($val['name'][$i]);
//                        $name = str_replace(array(' ','_','\''),'-',$name);

                        $ext = str_replace('image/','',$_FILES['image_name']['type']);
                        if( $ext == 'jpeg' ) {
                            $ext = 'jpg';
                        }

                        // image upload
                        $temp_config['upload_path']   = $destination;
                        $temp_config['allowed_types'] = implode('|', $config['file_formats']);
                        $temp_config['max_size']      = $config['max_size'];
                        $temp_config['min_width']     = $config['min_width'];
                        $temp_config['min_height']    = $config['min_height'];
                        $temp_config['overwrite']     = TRUE;
                        $temp_config['file_name']     = $_FILES['image_name']['name'];
                        $CI->upload->initialize($temp_config);
                        if ( !$CI->upload->do_upload('image_name') ) {
//                            print_r($CI->upload->display_errors());
                            $error_image = strip_tags($CI->upload->display_errors());
                            $data['error_image'][$k][$i] = '<p class="text-danger">'.$error_image.'</p>';
                        } else {
//                            print_r($CI->upload->data());
                            $data['uploaded_data'][$k][$i] = $CI->upload->data();
                            chmod($data['uploaded_data'][$k][$i]['full_path'], 0777);
                        }
                    }
                } else {
                    $ext = str_replace('image/','',$val['type']);
                    if( $ext == 'jpeg' ) {
                        $ext = 'jpg';
                    }

                    // image upload
                    $temp_config['upload_path']   = $destination;
                    $temp_config['allowed_types'] = implode('|', $config['file_formats']);
                    $temp_config['max_size']      = $config['max_size'];
                    $temp_config['min_width']     = $config['min_width'];
                    $temp_config['min_height']    = $config['min_height'];
                    $temp_config['overwrite']     = TRUE;
                    $temp_config['file_name']     = $val['name'];
                    $CI->upload->initialize($temp_config);
                    if ( !$CI->upload->do_upload($k) ) {
                        $error_image = strip_tags($CI->upload->display_errors());
                        $data['error_image'][$k] = '<p class="text-danger">'.$error_image.'</p>';
                    } else {
                        $data['uploaded_data'][$k] = $CI->upload->data();
                        chmod($data['uploaded_data'][$k]['full_path'], 0777);
                    }
                }
            }
        }

        return $data;
    }
}

// for cropping image
if( !function_exists('cropping_image') ) {
    function cropping_image($module, $source, $destination, $img_name) {
        $CI = &get_instance();

        // get upload config
        $config = get_uploads_config($module);
//        print_r($config); exit;
        foreach($source as $k => $row) {
            $new_name = preg_replace('/[\_\+\'\s]/','-',$img_name.$row['file_ext']);
//            $new_name = $k.'-'.strtolower($new_name);
            $new_name = strtolower($new_name);
//            $data[$k] = $new_name;
            $data[$k] = $new_name;                  // to be saved in database
            
//            print_r($row);
            // get thumb config
            $whereThumb['where'] = array(
                'config_id' => $config['config_id']
            );
            $thumb = get_thumb_config($whereThumb);
//            print_r($thumb); exit;
            
            // crop image from master upload config
            crop_image($config['min_width'], $config['min_height'], $row['full_path'], $destination, $new_name);
            // crop image based on thumb config
            if( !empty($thumb) ) {
                foreach($thumb as $i => $j) {
//                    if( $j['prefix'] == $k ) {
    //                    echo $j['prefix']; echo '<hr>';
                        crop_image($j['width'], $j['height'], $row['full_path'], $destination, $j['prefix'].$new_name);
//                    }
                }
            }

            // delete temporary image
            unlink($row['full_path']);
        }
//exit;
        return $data;
    }
}

// get upload config based on module
if( !function_exists('get_uploads_config') ) {
    function get_uploads_config($parameter='') {
        $CI = &get_instance();
        
        $CI->load->model('uploads/Uploads_model');
        $upload = new Uploads_model();
        
        $func = 'getUploads';
        if( !empty($parameter) ) {
            if( is_numeric($parameter) ) {
                $func = 'getUploadsById';     // id
            } elseif( !is_array($parameter) ) {
                $func = 'getUploadsByGid';    // gid
            }
        }
        
        $data = $upload->$func('uploads', $parameter);
        
        return $data;
    }
}

// get thumbnail size based on config type
if( !function_exists('get_thumb_config') ) {
    function get_thumb_config($parameter=array()) {
        $CI = &get_instance();
        
        $CI->load->model('uploads/Uploads_model');
        $upload = new Uploads_model();
        
        $func = 'getUploads';
        if( !empty($parameter) ) {
            if( is_numeric($parameter) ) {
                $func = 'getUploadsById';     // id
            } elseif( !is_array($parameter) ) {
                $func = 'getUploadsByGid';    // gid
            }
        }
        
        $data = $upload->$func('thumb', $parameter);
        
        return $data;
    }
}

// crop image
if( !function_exists('crop_image')) {
    function crop_image($max_width, $max_height, $source_file, $destination_path, $new_image) {
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        
        switch($imgsize['mime']){
            case 'image/png':
                $quality = 6;
                break;
            case 'image/jpeg':
                $quality = 50;
                break;
        }
        $image_create = str_replace('/','createfrom',$imgsize['mime']);
        $image = str_replace('/','',$imgsize['mime']);

        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);

        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;

        # if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width) {
            # cut point by height
            $h_point = (($height - $height_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        } else {
            # cut point by width
            $w_point = (($width - $width_new) / 2);
            imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }

        if($imgsize['mime'] == 'image/gif') {
            $image($dst_img, $destination_path.$new_image);
        } else {
            $image($dst_img, $destination_path.$new_image, $quality);
        }

        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
        
        chmod($destination_path.$new_image, 0777);
    }
}

// for compressing image
if( !function_exists('compress_image') ) {
    function compress_image($source, $destination, $quality) {
        $info = getimagesize($source);
        
        $imagecreatefrom = str_replace('/','createfrom', $info['mime']);
        $imagetype = str_replace('/', '', $info['mime']);
        $source = $imagecreatefrom($source);

        $imagetype($source, $destination, $quality);

        return $destination;
    }
}

if( !function_exists('thumbnail_box') ) {
    function thumbnail_box($img, $box_w, $box_h) {
        //create the image, of the required size
        $new = imagecreatetruecolor($box_w, $box_h);
        if($new === false) {
            //creation failed -- probably not enough memory
            return null;
        }
    
        //Fill the image with a light grey color
        //(this will be visible in the padding around the image,
        //if the aspect ratios of the image and the thumbnail do not match)
        //Replace this with any color you want, or comment it out for black.
        //I used grey for testing =)
        $fill = imagecolorallocate($new, 255, 255, 255);
        imagefill($new, 0, 0, $fill);
    
        //compute resize ratio
        $hratio = $box_h / imagesy($img);
        $wratio = $box_w / imagesx($img);
        $ratio = min($hratio, $wratio);
    
        //if the source is smaller than the thumbnail size, 
        //don't resize -- add a margin instead
        //(that is, dont magnify images)
        if($ratio > 1.0)
            $ratio = 1.0;
    
        //compute sizes
        $sy = floor(imagesy($img) * $ratio);
        $sx = floor(imagesx($img) * $ratio);
    
        //compute margins
        //Using these margins centers the image in the thumbnail.
        //If you always want the image to the top left, 
        //set both of these to 0
        $m_y = floor(($box_h - $sy) / 2);
        $m_x = floor(($box_w - $sx) / 2);
    
        //Copy the image data, and resample
        //
        //If you want a fast and ugly thumbnail,
        //replace imagecopyresampled with imagecopyresized
        if(!imagecopyresampled($new, $img,
            $m_x, $m_y, //dest x, y (margins)
            0, 0, //src x, y (0,0 means top left)
            $sx, $sy,//dest w, h (resample to this size (computed above)
            imagesx($img), imagesy($img)) //src w, h (the full size of the original)
        ) {
            //copy failed
            imagedestroy($new);
            return null;
        }
        //copy successful
        return $new;
    }
}
            