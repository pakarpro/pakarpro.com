<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Uploads_model extends CI_model {
    private $data;
    private $return;
    private $upload;
    private $upload_thumb;
    
    public function __construct() {
        parent::__construct();
        
        $this->return = array();
        
        $this->uploads = array(
            'id AS config_id',
            'gid AS gid_config',
            'name',
            'min_width',
            'min_height',
            'max_size',
            'file_formats'
        );
        
        $this->uploads_thumb = array(
            'id AS thumb_id',
            'config_id',
            'prefix',
            'width',
            'height',
            'default_img'
        );
        
        $this->load->library('Crud');
        $this->data = new Crud();
    }
    
#=================================== CREATE ===================================#
//    public function setNewUploads($data) {
//        $params['table'] = UPLOADS;
//        $params['data'] = $data;
//        
//        $this->return = $this->data->set($params);
//        
//        return $this->return;
//    }
    
#==================================== READ ====================================#
    public function countUploads($type) {
        switch($type) {
            case 'thumb':
                $field = $this->uploads_thumb;
                $table = UPLOADS_THUMB;
                break;
            default:
                $field = $this->uploads;
                $table = UPLOADS;
        }
        $params['fields'] = $field;
        $params['table'] = $table;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getUploads($type, $params=array()) {
        switch($type) {
            case 'thumb':
                $field = $this->uploads_thumb;
                $table = UPLOADS_THUMB;
                break;
            default:
                $field = $this->uploads;
                $table = UPLOADS;
        }
        $params['fields'] = $field;
        $params['table'] = $table;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getUploadsById($type, $uploadId) {
        switch($type) {
            case 'thumb':
                $field = $this->uploads_thumb;
                $table = UPLOADS_THUMB;
                break;
            default:
                $field = $this->uploads;
                $table = UPLOADS;
        }
        $params['fields'] = $field;
        $params['table'] = $table;
        $params['where'] = array(
            'id' => $uploadId
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getUploadsByGid($type, $uploadGid) {
        switch($type) {
            case 'thumb':
                $field = $this->uploads_thumb;
                $table = UPLOADS_THUMB;
                break;
            default:
                $field = $this->uploads;
                $table = UPLOADS;
        }
        $params['fields'] = $field;
        $params['table'] = $table;
        $params['where'] = array(
            'gid' => $uploadGid
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
//    public function updateUploads($data, $uploadId) {
//        $params['table'] = UPLOADS;
//        $params['data'] = $data;
//        $params['where'] = array(
//            'id' => $uploadId
//        );
//        
//        $this->return = $this->data->set($params, 'update');
//        
//        return $this->return;
//    }
    
#=================================== DELETE ===================================#
//    public function deleteUploads($uploadId) {
//        $params['table'] = UPLOADS;
//        $params['where'] = array(
//            'id' => $uploadId
//        );
//        
//        $this->return = $this->data->delete($params);
//        
//        return $this->return;
//    }
}