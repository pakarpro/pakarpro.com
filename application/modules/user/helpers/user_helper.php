<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if(!function_exists('list_user')){
    function list_user($data=''){
        $CI = &get_instance();
        $CI->load->model('user/User_model');
        
        if( !empty($data) ) {
            $user_list = $CI->User_model->list_user($data);
        } else {
            $user_list = $CI->User_model->list_user();
        }
        
        return $user_list;
    }
}
if(!function_exists('count_list_user')){
    function count_list_user(){
        $CI = &get_instance();
        $CI->load->model('user/User_model');
        
        $count_user_list = $CI->User_model->count_user();
        return $count_user_list;
    }
}
if(!function_exists('get_user_role')){
    function get_user_role(){
        $CI = &get_instance();
        $CI->load->model('user/User_model');
        
        $user = new User_model();
        $data = $user->getUser(USER_ROLE);
        
        return $data;
    }
}

/*
 * Function name    :   enc_password
 * Usage            :   formatting password to be encrypted
 * Type             :   string
 * Value            :   
 */
if( !function_exists('enc_password') ) {
    function enc_password($password) {
        $CI = &get_instance();

        $key = read_key(256);
        $key = pack('H*', $key);
        # create a random IV to use with CBC encoding
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $iv = read_iv();

        $new_password = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $password, MCRYPT_MODE_CBC, $iv);
        $new_password = $iv . $new_password;
        $new_password = base64_encode($new_password);
        
        return $new_password;
    }
}

if( !function_exists('read_key') ){
    function read_key($length, $pos=0) {
        $return = '';
        
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/key.txt";
        $fp = fopen($file, 'rb');
        if(!$fp) {
            // show something
        }
        $fseek = fseek($fp, $pos, SEEK_SET);
        if($fseek == -1) {
            // show something
        }
        $data = fread($fp, $length);
        $data = unpack("h*", $data);
        $arr = str_split(current($data), 2);
        foreach($arr as $val) {
            $return .= chr(hexdec($val));
        }
        fclose($fp);
        
        return $return;
    }
}

if( !function_exists('read_iv') ){
    function read_iv($pos=0) {
        $return = '';
        
        $lenght = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $file = $_SERVER['DOCUMENT_ROOT']."/etc/iv.txt";
        $fp = fopen($file, 'rb');
        if(!$fp) {
            // show something
        }
        $fseek = fseek($fp, $pos, SEEK_SET);
        if($fseek == -1) {
            // show something
        }
        $data = fread($fp, $lenght);
        fclose($fp);
        
        return $data;    
    }
}
