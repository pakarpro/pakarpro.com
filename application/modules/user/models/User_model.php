<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_model {
    
    public $data;
    public $return;
    private $user;
    private $user_role;
    
    public function __construct() {
        parent::__construct();
        
        $this->data = new Crud();
        $this->return = array();
        $this->user = array(
            'id AS user_id',
            'date_add',
            'date_update',
            'role_id',
            'name',
            'email',
            'username',
            'password',
            'status AS user_status'
        );
        $this->user_role = array(
            'id AS role_id',
            'gid AS gid_role',
            'role_name',
        );
    }
    
#=================================== CREATE ===================================#
    public function setNewUser($table, $data) {
        $params['table'] = $table;
        $params['data'] = $data;
        
        $this->return = $this->data->set($params);
        
        return $this->return;
    }
    
#==================================== READ ====================================#
    public function getUser($table, $params=array()) {
        // fields
        switch($table) {
            case USER: $fields = $this->user; break;
            case USER_ROLE: $fields = $this->user_role; break;
        }
        if( isset($params['fields']) ) {
            $fields = $params['fields'];
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
    public function getUserById($table, $id) {
        // fields
        switch($table) {
            case USER: $fields = $this->user; break;
            case USER_ROLE: $fields = $this->user_role; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'id' => $id
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function getUserByGid($table, $gid) {
        // fields
        switch($table) {
            case USER: $fields = $this->user; break;
            case USER_ROLE: $fields = $this->user_role; break;
        }
        
        $params['fields'] = $fields;
        $params['table'] = $table;
        $params['where'] = array(
            'gid' => $gid
        );

        $this->return = $this->data->get($params);
        if( !empty($this->return) ) {
            $this->return = array_shift($this->return);
        }
        
        return $this->return;
    }
    
    public function countUser($table, $params=array()) {
        $params['table'] = $table;
        $params['count'] = TRUE;

        $this->return = $this->data->get($params);
        
        return $this->return;
    }
    
#=================================== UPDATE ===================================#
    public function updateUser($table, $data, $userId) {
        $params['table'] = $table;
        $params['data'] = $data;
        $params['where'] = array(
            'id' => $userId
        );
        
        $this->return = $this->data->set($params, 'update');
        
        return $this->return;
    }
    
#=================================== DELETE ===================================#
    public function deleteUser($table, $userId) {
        $params['table'] = $table;
        $params['where'] = array(
            'id' => $userId
        );
        
        $this->return = $this->data->delete($params);
        
        return $this->return;
    }
}