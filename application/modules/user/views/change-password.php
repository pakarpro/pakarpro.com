<?php echo call_header('panel', 'Edit Password'); ?>

<?php echo call_sidebar(); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>

                                <form action="<?php echo site_url('panel/changepassword'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Old Password</label>
                                        <?php echo form_error('old_password'); ?>
                                        <div class="form-line">
                                            <input name="old_password" type="password" class="form-control" value="<?php echo set_value('old_password'); ?>" autofocus required>
                                        </div>
                                        <p><small class="rules">Minimum 8 characters</small></p>
                                    </div>

                                    <div class="form-group">
                                        <label>New Password</label>
                                        <?php echo form_error('password'); ?>
                                        <div class="form-line">
                                            <input name="password" type="password" class="form-control" value="<?php echo set_value('password'); ?>" required>
                                        </div>
                                        <p><small class="rules">Minimum 8 characters</small></p>
                                    </div>

                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <?php echo form_error('c_password'); ?>
                                        <div class="form-line">
                                            <input name="c_password" type="password" class="form-control" value="<?php echo set_value('c_password'); ?>" required>
                                        </div>
                                        <p><small class="rules">Minimum 8 characters</small></p>
                                    </div>

                                    <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 pull-right">CHANGE PASSWORD</button>
                                </form>                                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>

<script>
    var rules = [];
    var regex;
    var rule;
    
    $(document).on('click', '#trigger_change_pass', function() {
        $('input[type=password]').removeAttr('disabled');
//        alert(user_id);
//        return false;
//
//        clearRules();
//        $.post('<?php echo site_url('user/get_user_type'); ?>/'+user_id, function(data){
//            data = JSON.parse(data);
//
//            $('small.rules').text(data.info_rules);
//            $('#info_rules').val(data.info_rules);
//            $('#regex_rules').val(data.regex_rules);
//            $('input[name="username"]').focus();
////            console.log(data);
//        });
    });
    
//    function clearRules() {
//        rules = [];
//    }
</script>