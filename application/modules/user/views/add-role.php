<?php echo call_header('panel', 'Add New Role'); ?>

<?php echo call_sidebar('user', 'role'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <form action="<?php echo site_url('panel/user/role/add'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Role name</label>
                                        <?php echo form_error('role_name'); ?>
                                        <div class="form-line">
                                            <input name="role_name" id="role_name" type="text" class="form-control" value="<?php echo set_value('role_name'); ?>" autofocus required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Keyword</label>
                                        <?php echo form_error('gid_role'); ?>
                                        <div class="form-line">
                                            <input name="gid_role" type="text" class="form-control" value="<?php echo set_value('gid_role'); ?>">
                                        </div>
                                    </div>

                                    <div class="form-group">                                        
                                        <a href="<?php echo site_url('panel/user/role'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right m-r-10" value="SAVE">
                                    </div>
                                </form>                                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
