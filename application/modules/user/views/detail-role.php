<?php echo call_header('panel', 'Detail User Role'); ?>

<?php echo call_sidebar('user', 'role'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <div class="form-group">
                                    <?php
                                        $back_url = site_url('panel/user/role');
                                        if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                            $back_url = $_SERVER['HTTP_REFERER'];
                                        }
                                    ?>
                                    <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect">BACK</a>
                                </div>

                                <?php if( isset($data) ) { ?>
                                    <form action="<?php echo site_url('panel/user/role/detail/'.$data['role_id']); ?>" method="post">
                                        <div class="form-group">
                                            <label>Role name</label>
                                            <?php echo form_error('role_name'); ?>
                                            <div class="form-line">
                                                <input name="role_name" id="role_name" type="text" class="form-control" value="<?php echo $data['role_name']; ?>" autofocus required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Keyword</label>
                                            <?php echo form_error('gid_role'); ?>
                                            <div class="form-line">
                                                <input name="gid_role" type="text" class="form-control" value="<?php echo $data['gid_role']; ?>">
                                            </div>
                                        </div>

                                        <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 pull-right">UPDATE</button>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
