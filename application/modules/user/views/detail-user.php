<?php echo call_header('panel', 'Detail User'); ?>

<?php echo call_sidebar('user', 'all'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-12 col-md-6">
                                <?php echo $this->session->flashdata('notif'); ?>
                                
                                <div class="form-group">
                                    <?php
                                        $back_url = site_url('panel/user');
                                        if(isset($_SERVER['HTTP_REFERER']) && site_url(uri_string()) != $_SERVER['HTTP_REFERER']) {
                                            $back_url = $_SERVER['HTTP_REFERER'];
                                        }
                                    ?>
                                    <a href="<?php echo $back_url; ?>" class="btn btn-default waves-effect">BACK</a>
                                </div>

                                <?php if( isset($data) ) { ?>
                                    <form action="<?php echo site_url('panel/user/all/detail/'.$data['user_id']); ?>" method="post">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <?php
                                                $checked = '';
                                                if( $status == 1 ) {
                                                    $checked = 'checked';
                                                }
                                            ?>
                                            <div class="switch">
                                                <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Role</label>
                                            <?php echo form_error('user_role'); ?>
                                            <div class="form-line">
                                                <select name="user_role" class="form-control show-tick" id="user_role" required="required">
                                                    <option value="">- Choose -</option>
                                                    <?php
                                                        foreach($user_role as $k => $item) {
                                                            $selected = '';
                                                            if( $item['role_id'] == $data['role_id'] ) {
                                                                $selected = 'selected';
                                                            }
                                                            echo '<option value="'.$item['role_id'].'" '.$selected.'>'.$item['role_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label>Name</label>
                                            <?php echo form_error('name'); ?>
                                            <div class="form-line">
                                                <input name="name" type="text" class="form-control" value="<?php echo $data['name']; ?>" autofocus required="required">
                                            </div>
                                            <p><small class="rules">Must contain only standard letters, space, dot, or comma.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <?php echo form_error('email'); ?>
                                            <div class="form-line">
                                                <input name="email" type="email" class="form-control" value="<?php echo $data['email']; ?>" required="required">
                                            </div>
                                            <p><small class="rules">For recovery reason, email must be provide in order to recover your account easily.</small></p>
                                        </div>

                                        <div class="form-group">
                                            <label>Username</label>
                                            <?php echo form_error('username'); ?>
                                            <div class="form-line">
                                                <input name="username" type="text" class="form-control" value="<?php echo $data['username']; ?>" required="required">
                                            </div>
                                            <p><small class="rules">Must contain only letters, underscore, or dot.</small></p>
                                        </div>

                                        <div class="clearfix">
                                            <?php
                                                $chk = '';
                                                $required = '';
                                                $disabled = 'disabled';
                                                if( isset($trigger) ) {
                                                    $chk = 'checked';
                                                    $required = 'required';
                                                    $disabled = '';
                                                }
                                            ?>
                                            <input type="checkbox" name="trigger_change_pass" id="trigger_change_pass" class="filled-in" <?php echo $chk; ?>>
                                            <label for="trigger_change_pass">Click to change password</label>
                                            <br>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <?php echo form_error('password'); ?>
                                                <div class="form-line">
                                                    <input name="password" id="pass" type="password" class="form-control" value="<?php echo set_value('password'); ?>" <?php echo $disabled.' '.$required; ?>>
                                                </div>
                                                <p><small class="rules">Minimum 8 characters</small></p>
                                            </div>

                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <?php echo form_error('c_password'); ?>
                                                <div class="form-line">
                                                    <input name="c_password" type="password" class="form-control" value="<?php echo set_value('c_password'); ?>" <?php echo $disabled.' '.$required; ?>>
                                                </div>
                                                <p><small class="rules">Minimum 8 characters</small></p>
                                            </div>
                                        </div>
                                        
                                                                                
                                        <button type="submit" id="btnTriggerUpdate" class="btn btn-primary m-t-15 pull-right">UPDATE</button>
                                    </form>                                            
                                <?php
                                    } else {
                                        echo 'Data not found';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>
<script>
    $(document).on('click', '#trigger_change_pass', function() {
        $('input[type=password]').removeAttr('disabled');
        $('#pass').focus();
    });
</script>