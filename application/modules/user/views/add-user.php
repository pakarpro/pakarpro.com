<?php echo call_header('panel', 'Add New User'); ?>

<?php echo call_sidebar('user', 'all'); ?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-md-6 col-xs-12">
                                <form action="<?php echo site_url('panel/user/all/add'); ?>" method="post">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <?php
                                            $checked = '';
                                            if( $status == 1 ) {
                                                $checked = 'checked';
                                            }
                                        ?>
                                        <div class="switch">
                                            <label>INACTIVE<input type="checkbox" name="status" <?php echo $checked; ?>><span class="lever"></span>ACTIVE</label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Role</label>
                                        <?php echo form_error('user_role'); ?>
                                        <div class="form-line">
                                            <select name="user_role" class="form-control show-tick" id="user_role" required="required">
                                                <option value="">- Choose -</option>
                                                <?php
                                                    foreach($user_role as $k => $item) {
                                                        $selected = '';
                                                        if( $item['role_id'] == set_value('user_role') ) {
                                                            $selected = 'selected';
                                                        }
                                                        echo '<option value="'.$item['role_id'].'" '.$selected.'>'.$item['role_name'].'</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Name</label>
                                        <?php echo form_error('name'); ?>
                                        <div class="form-line">
                                            <input name="name" type="text" class="form-control" value="<?php echo set_value('name'); ?>" autofocus required="required">
                                        </div>
                                        <p><small class="rules">Must contain only standard letters, space, dot, or comma.</small></p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Email</label>
                                        <?php echo form_error('email'); ?>
                                        <div class="form-line">
                                            <input name="email" type="email" class="form-control" value="<?php echo set_value('email'); ?>" required="required">
                                        </div>
                                        <p><small class="rules">For recovery reason, email must be provide in order to recover your account easily.</small></p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Username</label>
                                        <?php echo form_error('username'); ?>
                                        <div class="form-line">
                                            <input name="username" type="text" class="form-control" value="<?php echo set_value('username'); ?>" required="required">
                                        </div>
                                        <p><small class="rules">Must contain only letters, underscore, or dot.</small></p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Password</label>
                                        <?php echo form_error('password'); ?>
                                        <div class="form-line">
                                            <input name="password" type="password" class="form-control" value="<?php echo set_value('password'); ?>" required="required">
                                        </div>
                                        <p><small class="rules">Minimum 8 characters</small></p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <?php echo form_error('c_password'); ?>
                                        <div class="form-line">
                                            <input name="c_password" type="password" class="form-control" value="<?php echo set_value('c_password'); ?>" required="required">
                                        </div>
                                        <p><small class="rules">Minimum 8 characters</small></p>
                                    </div>
                                    
                                    <div class="form-group">                                        
                                        <a href="<?php echo site_url('panel/user/all'); ?>" class="btn btn-default m-t-15 waves-effect pull-right">BACK</a>
                                        <input type="submit" class="btn btn-primary m-t-15 waves-effect pull-right m-r-10" value="SAVE">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php echo call_footer('panel'); ?>