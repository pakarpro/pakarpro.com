<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    private $CI;
    private $user_status = 1;
    private $redirect = 'panel/login';
//    private $users = 'user';
//    private $users_role = 'role';
    private $admin_data = array();
    private $user_validation_config = array(
        array(
            'field'   => 'user_role',
            'label'   => 'User role',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'name',
            'label'   => 'Name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z0-9\s\.\,]+$/]'
        ),
        array(
            'field'   => 'email',
            'label'   => 'Email',
            'rules'   => 'trim|required|valid_email'
        ),
        array(
            'field'   => 'username',
            'label'   => 'Username',
            'rules'   => 'trim|required|min_length[5]|max_length[30]'
        )
    );
    private $role_validation_config = array(
        array(
            'field'   => 'gid_role',
            'label'   => 'Keyword',
            'rules'   => 'trim|required'
        ),
        array(
            'field'   => 'role_name',
            'label'   => 'Name',
            'rules'   => 'trim|required|regex_match[/^[A-Za-z\s]+$/]'
        )
    );
    private $password_validation = array(
        array(
            'field'   => 'password',
            'label'   => 'Password',
            'rules'   => 'trim|required|min_length[8]|max_length[30]'
        ),
        array(
            'field'   => 'c_password',
            'label'   => 'Confirm password',
            'rules'   => 'trim|required|min_length[8]|max_length[30]|matches[password]'
        )
    );
    private $old_password_rules = array(
        'field'   => 'old_password',
        'label'   => 'Old password',
        'rules'   => 'trim|required|min_length[8]|max_length[30]'
    );
    private $validation_msg = array(
        'required' => '%s is required',
        'min_length' => '%s at least %d characters',
        'max_length' => '%s maximum length is %d characters',
        'regex_match' => '%s does not follow approriate format',
        'matches' => '%s does not match',
        'password_match' => '%s does not match with current password',
        'is_unique' => '%s is already used',
        'edit_unique' => '%s is already used',
        'valid_email' => '%s is invalid'
    );
    private $user;

    function __construct() {
        parent::__construct();
        $this->CI =& get_instance();
        
        $this->load->helper('form');
        $this->load->model('User_model');
        $this->user = new User_model();

        if( $this->session->userdata('is_login') ) {
            $this->admin_data = $this->session->userdata('is_login');
        }
    }
    
    // for checking session login
    private function _check_login_session() {
        if( empty($this->admin_data) ) {
            $this->redirect = '?rdr='.urlencode($_SERVER['REQUEST_URI']);
            redirect('panel/login'.$this->redirect);
        }
    }
   
    // get list of user
    private function _get_user($param=array()) {
        $param['fields'] = array(
            USER.'.id AS user_id',
            USER.'.role_id',
            USER.'.name',
            USER.'.username',
            USER.'.status AS user_status'
        );
        $data = $this->user->getUser(USER, $param);

        return $data;
    }
    
    // get list of user role
    private function _get_user_role($param=array()) {
        $param['fields'] = array(
            'id AS role_id',
            'gid AS gid_role',
            'role_name'
        );
        $data = $this->user->getUser(USER_ROLE, $param);

        return $data;
    }
    
    // updating status, for ajax use
    public function changestatus() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $status = $data['status'];
            
            $update_param = array(
                'date_update' => DATETIME,
                'status' => $status
            );
            $update = $this->user->updateUser(USER, $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    // updating user_type, for ajax use
    public function changerole() {
        $return['status'] = 'failed';
        $data = $this->input->post();

        if( !empty($data) ) {
            $id = $data['id'];
            $user_type = $data['user_role'];
            
            $update_param = array(
                'date_update' => DATETIME,
                'role_id' => $user_type
            );
            $update = $this->user->updateUser(USER, $update_param, $id);
            if( $update == 'SUCCESSFUL' ) {
                $return['status'] = 'success';
            }
        }
        
        echo json_encode($return);
    }
    
    public function index() {
        $this->lists();
    }
    
    // list all user data
    public function lists($type='all', $page=1) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // set table
        $table = USER;
        $file = 'list-user';
        $func = '_get_user';
        if( $type != 'all' ) {
            $file = 'list-'.$type;
            $func = '_get_user_'.$type;
            $table = USER.'_'.$type;
        }
        
        $count_user = $this->user->countUser($table);
        if( $count_user > 0 ) {
            $limit_per_page = 10;
            $start_row = ($page-1) * $limit_per_page;

            if( $count_user > $limit_per_page ) {
                // additional where param
                $where_param['limit'] = array($limit_per_page, $start_row);
                
                $this->load->library('pagination');
                $paging_config = array(
                    'base_url'      =>  site_url('panel/user/'.$type),
                    'total_rows'    =>  $count_user,
                    'per_page'      =>  $limit_per_page
                );
                $config = paging_config($paging_config, false);
                $this->pagination->initialize($config);
                $data['pagination'] = $this->pagination->create_links();
            }
            $where_param['order_by'] = array(
                $table.'.id' => 'ASC'
            );
            $data['data'] = $this->$func($where_param);
            
            if( $type == 'all' ) {
                // get user type
                $this->load->helper('user');
                $data['user_role'] = get_user_role();
            }
        }
        
        $this->load->view($file, $data);
    }
    
    // save new user data
    public function add($type='all') {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;

        // set table
        $table = USER;
        $file = 'add-user';
        if( $type != 'all' ) {
            $table = USER.'_'.$type;
            $file = 'add-'.$type;
        }
        
        $this->load->helper('user');
        if(!empty($_POST)) {
            // get post data
            $post_data = $this->input->post();
            if( $type == 'all' ) {
                $this->user_status = format_status($post_data);
            }
            $user = $post_data;

            // validation
            $this->load->library('form_validation');
            if( $type == 'all' ) {
                $validation_config = $this->user_validation_config;
                $validation_config = array_merge($validation_config, $this->password_validation);
            } else {
                $validation_config = $this->role_validation_config;
            } 

            foreach($validation_config as $k => $item) {
                switch($item['field']) {
                    case 'gid_role':
                    case 'email':
                    case 'username':
                        $field = $item['field'];
                        if( $item['field'] == 'gid_role' ) {
                            $field = 'gid';
                        }
                        $item['rules'] = $item['rules'].'|is_unique['.$table.'.'.$field.']';
                        break;
                }

                $validation_config[$k] = $item;
            }
            
            $this->form_validation->set_rules($validation_config);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                foreach($post_data as $field => $value) {
                    switch($field) {
                        case 'name':
                            $value = ucwords($value);
                            break;
                        case 'email':
                        case 'username':
                        case 'gid_role':
                            $save_param['gid'] = strtolower($value);
                            break;
                        case 'password':
                            $value = enc_password($value);
                            break;
                        case 'role_name':
                            $value = ucwords($value);
                            break;;
                    }
                    $save_param[$field] = $value;
                }
                if( $type == 'all' ) {
                    $save_param['date_add'] = DATETIME;
                    $save_param['status'] = $this->user_status;
                }
                unset($save_param['gid_role']);
                $save = $this->user->setNewUser($table, $save_param);
                if( isset($save['id']) ) {
                    $id = $save['id'];

                    // set notification
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                New data has been added.
                              </div>';
                    $this->session->set_flashdata('notif', $notif);

                    // redirect to list general info page
                    redirect('panel/user/'.$type.'/detail/'.$id);
                }
            }
        }

        if( $type == 'all' ) {
            // get user type
            $data['user_role'] = get_user_role();
            $data['status'] = $this->user_status;
        }
        
        // view page (add user or role)
        $this->load->view($file, $data);
    }
    
    // update user data, operated by admin
    public function detail($type='all', $id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // set table
        $table = USER;
        $file = 'detail-user';
        if( $type != 'all' ) {
            $file = 'detail-'.$type;
            $table = USER.'_'.$type;
        }
        
        //if given ID is not empty and not 0
        if( !empty($id) && is_numeric($id) && $id != 0 ) {
            $user = $this->user->getUserById($table, $id);

            if( !empty($user) ) {
                if( $type == 'all' ) {
                    $this->user_status = $user['user_status'];

                    // get user type
                    $this->load->helper('user');
                    $data['user_role'] = get_user_role();
                }

                if(!empty($_POST)) {
                    $post_data = $this->input->post();
                    if( $type == 'all' ) {
                        $post_data['user_id'] = $id;
                        $this->user_status = format_status($post_data);
                    } else {
                        $post_data['role_id'] = $id;
                    }
                    $user = $post_data;
                    
                    // validation
                    $this->load->library('form_validation');
                    if( $type == 'all' ) {
                        $validation_config = $this->user_validation_config;
                    } elseif( $type == 'role' ) {
                        $validation_config = $this->role_validation_config;
                    }
                    foreach($validation_config as $k => $item) {
                        switch($item['field']) {
                            case 'gid_role':
                            case 'email':
                            case 'username':
                                $field = $item['field'];
                                if( $item['field'] == 'gid_role' ) {
                                    $field = 'gid';
                                }
                                $item['rules'] = $item['rules'].'|edit_unique['.$table.'.'.$field.'.'.$id.']';
                                break;
                        }

                        $validation_config[$k] = $item;
                    }
                    if( isset($post_data['trigger_change_pass']) ) {
                        $data['trigger'] = $post_data['trigger_change_pass'];
                        $validation_config = array_merge($validation_config, $this->password_validation);
                    }
//                    print_r($validation_config); exit;
                    $this->form_validation->set_rules($validation_config);
                    foreach($this->validation_msg as $key => $msg) {
                        $this->form_validation->set_message($key, $msg);
                    }

                    if ($this->form_validation->run() == TRUE ) {
//                        echo 'ok'; exit;
                        foreach($post_data as $field => $value) {
                            switch($field) {
                                case 'role_id':
                                case 'user_id':
                                    $update_param['id'] = $value;
                                    break;
                                case 'name':
                                    $value = ucwords($value);
                                    break;
                                case 'email':
                                case 'username':
                                case 'gid_role':
                                    $update_param['gid'] = strtolower($value);
                                    break;
                                case 'user_role':
                                    $update_param['role_id'] = $value;
                                    break;
                                case 'c_password':
                                    $value = enc_password($value);
                                    break;
                                case 'role_name':
                                    $value = ucwords($value);
                                    break;;
                            }
                            $update_param[$field] = $value;
                        }
                        if( $type == 'all' ) {                              // user
                            $update_param['date_update'] = DATETIME;
                            $update_param['status'] = $this->user_status;
                            
                            unset($update_param['user_id']);
                            unset($update_param['id']);
                            unset($update_param['gid']);
                            unset($update_param['user_role']);
                        } else {                                            // role
                            unset($update_param['role_id']);
                            unset($update_param['gid_role']);
                        }
//                        print_r($update_param); exit;
                        $update = $this->user->updateUser($table, $update_param, $id);
                        if( $update == 'SUCCESSFUL' ) {
                            $msg = '';
                            if( isset($post_data['password']) ) {
                                $msg = ' with password changed into <b>'.$post_data['c_password'].'</b>';
                            }
                            // set notification
                            $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        Data has been updated.'.$msg.
                                      '</div>';
                            $this->session->set_flashdata('notif', $notif);

                            // redirect to list general info page
                            redirect('panel/user/'.$type.'/detail/'.$id);
                        }
                    } # end of validation
                } # end of post
                
                $data['data'] = $user;
                
                if( $type == 'all' ) {
                    // status
                    $data['status'] = $this->user_status;
                }
            }
        }

        $this->load->view($file, $data);
    }
    
    // update profile data
    public function changepassword() {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        $id = $this->admin_data;

        if(!empty($_POST)) {
            $post_data = $this->input->post();

            // validation
            $this->load->library('form_validation');
            $this->old_password_rules['rules'] = $this->old_password_rules['rules'].'|password_match[users.password.'.$id.']';
            $this->password_validation[] = $this->old_password_rules;
            $this->form_validation->set_rules($this->password_validation);
            foreach($this->validation_msg as $key => $msg) {
                $this->form_validation->set_message($key, $msg);
            }

            if ($this->form_validation->run() == TRUE ) {
                $password = enc_password($post_data['c_password']);

                // update parameter
                $update_param = array(
                    'date_update' => DATETIME,
                    'password' => $password
                );
                $update = $this->user->updateUser(USER, $update_param, $id);
                if( $update == 'SUCCESSFUL' ) {
                    // set notification
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                Password has been changed with <b>'.$post_data['c_password'].'</b>
                              </div>';
                    $this->session->set_flashdata('notif', $notif);

                    // redirect to list general info page
                    redirect('panel/changepassword');
                }
            } # end of validation
        } # end of post
        
        $this->load->view('change-password', $data);
    }
    
    // delete user data, include of image
    public function delete($type='all', $id) {
        // check session login
        $this->_check_login_session();
        $data['admin_data'] = $this->admin_data;
        
        // set table
        $table = USER;
        $subject = 'user';
        if( $type != 'all' ) {
            $table = USER.'_'.$type;
            $subject = 'user role';
        }
        
        // provide error message when given ID is empty or 0
        $msg = 'An error occured while deleting '.$subject.'. Please try again later.';
        $alertClass = 'alert-warning';
        if( !empty($id) && $id != 0 || is_numeric($id) ) {
            // check if given ID is existing in database
            $user = $this->user->getUserById($table, $id);
            // if data is found
            if( !empty($user) ) {
                $name = $user['name'];
                if( $type != 'all' ) {
                    $name = $user['role_name'];
                }
                
                // run delete action
                $delete = $this->user->deleteUser($table, $id);
                // if delete was succeed
                if( $delete == 'SUCCESSFUL' ) {
                    // provide success message
                    $msg = ucfirst($subject).' with name <b>'.$name.'</b> has been deleted.';
                    $alertClass = 'alert-success';
                }
            }
        }

        // set notification in HTML code
        $notif = '<div class="alert '.$alertClass.'" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    '.$msg.'
                 </div>';
        // store in temporary session called flashdata
        $this->session->set_flashdata('notif', $notif);
        // redirect to list page
        redirect('panel/user/'.$type);
    }
}