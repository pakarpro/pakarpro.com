<?php
ob_start();
include('ci_index.php');
ob_end_clean();
$CI =& get_instance();
#session_start();
include_once("config.php");
include_once("inc/twitteroauth.php");

if (isset($_REQUEST['oauth_token']) && $_SESSION['token']  !== $_REQUEST['oauth_token']) {
	// if token is old, distroy any session and redirect user to index.php
	session_destroy();
	header('Location: ../index.php');
	
}elseif(isset($_REQUEST['oauth_token']) && $_SESSION['token'] == $_REQUEST['oauth_token']) {
	// everything looks good, request access token
	//successful response returns oauth_token, oauth_token_secret, user_id, and screen_name
	//define token
	define('TOKEN', $_SESSION['token']);
	define('TOKEN_SECRET', $_SESSION['token_secret']);

	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['token'], $_SESSION['token_secret']);
	
	$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);
	$user = $connection->get('account/verify_credentials');
	$following = $connection->get('friendships/show', array('source_screen_name' => 'inproperti', 'target_screen_name' => $user->screen_name));
	//print_r($following->relationship->target->following);exit;
	
	if($connection->http_code=='200')
	{
		//redirect user to twitter
		#$_SESSION['status_twitter'] = 'verified';
		#$_SESSION['twitter'] = $access_token;
		#$_SESSION['twitter']['user_data'] = $user;
		#$_SESSION['twitter']['follow_status'] = $following->relationship->target->following;
                $set_userdata['status_twitter'] = 'verified';
		$set_userdata['twitter'] = $access_token;
		$set_userdata['twitter']['user_data'] = $user;
		$set_userdata['twitter']['follow_status'] = $following->relationship->target->following;
                $CI->session->set_userdata($set_userdata); 
		// unset no longer needed request tokens
		unset($_SESSION['token']);
		unset($_SESSION['token_secret']);
		header("Location: http://rumahmurah.com/users/login_social_media");
	}else{
		die("error, try again later!");
	}
		
}else{

	if(isset($_GET["denied"]))
	{
		header('Location: ../../index.php');
		die();
	}
	//fresh authentication
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);
	$request_token = $connection->getRequestToken(OAUTH_CALLBACK);
	//received token info from twitter
	$_SESSION['token']              = $request_token['oauth_token'];
	$_SESSION['token_secret'] 	= $request_token['oauth_token_secret'];
        #print_r($_SESSION);
	// any value other than 200 is failure, so continue only if http code is 200
	if($connection->http_code=='200')
	{
		//redirect user to twitter
		$twitter_url = $connection->getAuthorizeURL($request_token['oauth_token']);
		header('Location: ' . $twitter_url); 
	}else{
		die("error connecting to twitter! try again later!");
	}
}
?>

