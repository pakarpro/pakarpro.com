jQuery(function($) {'use strict',

    //#main-slider
    $(function(){
        $('#main-slider.carousel').carousel({
            interval: 8000
        });
    });

    // accordian
    $('.accordion-toggle').on('click', function(){
        $(this).closest('.panel-group').children().each(function(){
            $(this).find('>.panel-heading').removeClass('active');
        });

        $(this).closest('.panel-heading').toggleClass('active');
    });

    //Initiat WOW JS
    new WOW().init();

    // portfolio filter
    $(window).load(function(){'use strict';
        var $portfolio_selectors = $('.portfolio-filter >li>a');
        var $portfolio = $('.portfolio-items');
        $portfolio.isotope({
            itemSelector : '.portfolio-item',
            layoutMode : 'fitRows'
        });

        $portfolio_selectors.on('click', function(){
            $portfolio_selectors.removeClass('active');
            $(this).addClass('active');
            var selector = $(this).attr('data-filter');
            $portfolio.isotope({ filter: selector });
            return false;
        });
    });

    // Contact form
    var form = $('#main-contact-form');
    form.submit(function(event){ 
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: form.serialize(),
            success: function(data) {
                var msg = JSON.parse(data);

//                form.prepend( ('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
                switch( msg.status ) {
                    case 'success':
                        $('p.text-danger').remove();
                        $('.form-control').val('');
                        $('div.alert').show().html(
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'+
                            'Pesan berhasil dikirim'
                        ).delay(5000).fadeOut();
                        break;
                    case 'failed':
                        $('p.text-danger').remove();
                        
                        var error = msg.message;
                        $(error.name).insertAfter('input#c_name');
                        $(error.email).insertAfter('input#c_email');
                        $(error.phone).insertAfter('input#c_phone');
                        $(error.company_name).insertAfter('input#c_company_name');
                        $(error.subject).insertAfter('input#c_subject');
                        $(error.message).insertAfter('textarea#c_message');
                        break;
                }
            }
        });
        event.preventDefault();
    });

    //.scroll-to-top
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 1000);
        return false;
    });

    //Pretty Photo
    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false
    });	
});