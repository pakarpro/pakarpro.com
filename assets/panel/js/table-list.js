$(document).on('click', '._l', function() {
    var url = $(this).parent().attr('data-target');

    window.location = url;
});

function updateStatus(page) {
    $(document).on('click', '._cs', function() {
        var url = page+'/changestatus';
        var status;
        var id = $(this).attr('data-id');

        if( $(this).is(':checked') ) {
            status = 1;
        } else {
            status = 0;
        }

        var data = {
            "id": id,
            "status": status
        }
        
        $.post(url, data, function(response) {
            console.log(response);
            // do something here, for custom
        });
    });
}

